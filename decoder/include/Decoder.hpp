/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file Decoder.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-03

#ifndef DECODER__H
#define DECODER__H

#include "TransitionMachine.hpp"
#include "Config.hpp"

/// @brief A simple object capable of using a trained TransitionMachine to process a given BD.
class Decoder
{
  private :

  /// @brief The trained TransitionMachine
  TransitionMachine & tm;
  /// @brief The current configuration of the TransitionMachine
  Config & config;
  std::map< std::string, std::map<std::string, int> > nbActionsPerClassifier;

  private :

  /// @brief Fill bd using tm, with beam serach.
  void decodeBeam();
  /// @brief Fill bd using tm, without beam search.
  void decodeNoBeam();
  /// @brief Print for each classifier, the detail of predicted actions.
  void printActionsPerClassifier(FILE * output);

  public :

  /// @brief Use tm to fill bd.
  ///
  /// At the start of the function, bd must contain the input.\n
  /// At the end of the function, bd will be terminal.
  /// @param tm The trained TransitionMachine
  /// @param config The current configuration of the TransitionMachine
  Decoder(TransitionMachine & tm, Config & config);
  /// @brief Fill bd using tm.
  void decode();
};

#endif
