/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "ProgramOutput.hpp"
#include "ProgramParameters.hpp"
#include "util.hpp"

ProgramOutput ProgramOutput::instance;

void ProgramOutput::print(FILE * output)
{
  if (!ProgramParameters::delayedOutput)
    return;
  for (auto & line : matrix)
    for (unsigned int i = 0; i < line.size(); i++)
      fprintf(output, "%s%s%s", ProgramParameters::printOutputEntropy ? ("<"+util::float2str(line[i].second,"%f")+">").c_str() : "", line[i].first.c_str(), i == line.size()-1 ? "\n" : "\t");
}

void ProgramOutput::addLine(FILE * output, const std::vector< std::pair<std::string, float> > & line, unsigned int index)
{
  if (!ProgramParameters::delayedOutput)
  {
    for (unsigned int i = 0; i < line.size(); i++)
      fprintf(output, "%s%s", line[i].first.c_str(), i == line.size()-1 ? "\n" : "\t");

    return;
  }

  while (matrix.size() < index+1)
    matrix.emplace_back();

  for (unsigned int i = 0; i < line.size(); i++)
  {
    if (matrix[index].size() <= i)
      matrix[index].emplace_back(line[i]);
    else if (matrix[index][i].second > line[i].second)
    {
      matrix[index][i] = line[i];
    }
  }
}

