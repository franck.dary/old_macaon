/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "Dict.hpp"
#include "File.hpp"
#include "util.hpp"
#include "ProgramParameters.hpp"
#include <dynet/io.h>

std::string Dict::currentClassifierName;
std::string Dict::nullValueStr = "_nullVALUEstr_";
std::string Dict::unknownValueStr = "_unknownVALUEstr_";
std::map< std::string, std::unique_ptr<Dict> > Dict::str2dict;

Dict::Mode Dict::str2mode(const std::string & s)
{ 
  if(s == "OneHot")
    return Mode::OneHot;
  else if (s == "Embeddings")
    return Mode::Embeddings;

  fprintf(stderr, "ERROR (%s) : %s is not a valid mode. Aborting.\n", ERRINFO, s.c_str());
  exit(1);

  return Mode::OneHot;
}

Dict::Policy Dict::str2policy(const std::string & s)
{ 
  if(s == "Final")
    return Policy::Final;
  else if (s == "Modifiable")
    return Policy::Modifiable;
  else if (s == "FromZero")
    return Policy::FromZero;

  fprintf(stderr, "ERROR (%s) : %s is not a valid policy. Aborting.\n", ERRINFO, s.c_str());
  exit(1);

  return Policy::Final;
}

const char * Dict::mode2str(Mode mode)
{
  if(mode == Mode::OneHot)
    return "OneHot";

  return "Embeddings";
}

Dict::Dict(const std::string & name, int dimension, Mode mode, int dictCapacity)
{
  this->isInit = false;
  this->isTrained = false;
  this->policy = Policy::FromZero;
  this->filename = name;
  this->name = name;
  this->oneHotIndex = 0;
  this->dictCapacity = dictCapacity;

  this->mode = mode;
  this->dimension = dimension;
}

Dict::Dict(const std::string & name, Policy policy, const std::string & filename, int dictCapacity)
{
  this->isInit = false;
  this->isTrained = true;
  this->policy = policy;
  this->filename = filename;
  this->oneHotIndex = 0;
  this->name = name;
  this->dictCapacity = dictCapacity;
}

void Dict::init(dynet::ParameterCollection & pc)
{
  if (isInit)
  {
    fprintf(stderr, "ERROR (%s) : Dict is already initialized. Aborting.\n", ERRINFO);
    exit(1);
  }

  isInit = true;
  this->lookupParameter = pc.add_lookup_parameters(dictCapacity, {(unsigned int)dimension});
  addEntry(nullValueStr);
  addEntry(unknownValueStr);
}

void Dict::initFromFile(dynet::ParameterCollection & pc)
{
  auto badFormatAndAbort = [&](std::string errInfo)
  {
    fprintf(stderr, "Error (%s) : file %s bad format. Aborting.\n", errInfo.c_str(), filename.c_str());
    exit(1);
  };

  if (isInit)
  {
    fprintf(stderr, "ERROR (%s) : Dict is already initialized. Aborting.\n", ERRINFO);
    exit(1);
  }

  File * file = new File(filename, "r");
  FILE * fd = file->getDescriptor();

  char b1[100000];
  char b2[100000];

  if(fscanf(fd, "%s\n%d\n%s\n", b1, &dimension, b2) != 3)
    badFormatAndAbort(ERRINFO);

  mode = str2mode(b2);

  isInit = true;

  // If a fasttext pretrained embedding file is specified
  if(fscanf(fd, "Fasttext : %s\n", b1) == 1)
  {
    static_assert(std::is_same<float, fasttext::real>::value, "ERROR : fasttext::real is not float on this machine, it needs to be. Aborting.\n");

    ftEmbeddings.reset(new fasttext::FastText);
    try
    {
      ftEmbeddings->loadModel(b1);
    } catch (std::exception &)
    {
      fprintf(stderr, "ERROR (%s) : Could not open file \'%s\'. Aborting.\n", ERRINFO, b1);
      exit(1);
    }
    ftFilename = b1;

    if(ftEmbeddings->getDimension() != dimension)
    {
      fprintf(stderr, "ERROR (%s) : tried to load fasttext embeddings of dimension %d into dict \'%s\' of dimension %d. Aborting.\n", ERRINFO, ftEmbeddings->getDimension(), name.c_str(), dimension);
      exit(1);
    }

    ftVector.reset(new fasttext::Vector(dimension));
    this->lookupParameter = pc.add_lookup_parameters(dictCapacity, {(unsigned int)dimension});
  }

  // If policy is FromZero, we don't need to read the current entries
  if(this->policy == Policy::FromZero)
    return;

  int readIndex = -1;
  while (fscanf(fd, "\1%[^\1]\1%d\n", b1, &readIndex) == 2)
    str2index[b1] = readIndex;

  delete file;

  if (readIndex == -1) // No parameters to read
  {
    this->lookupParameter = pc.add_lookup_parameters(dictCapacity, {(unsigned int)dimension});
    addEntry(nullValueStr);
    addEntry(unknownValueStr);
    return;
  }

  dynet::TextFileLoader loader(filename);
  lookupParameter = loader.load_lookup_param(pc, "lookup");
}

void Dict::saveDicts(const std::string & directory, const std::string & namePrefix)
{
  for (auto & it : str2dict)
  {
    if(!strncmp(it.first.c_str(), namePrefix.c_str(), namePrefix.size()))
    {
      if (!(it.second->isTrained && it.second->policy != Policy::Modifiable))
      {
        it.second->filename = directory + it.second->name + ".dict";
        it.second->save();
      }
    }
  }
}

void Dict::createFiles(const std::string & directory, const std::string & namePrefix)
{
  for (auto & it : str2dict)
  {
    if(!strncmp(it.first.c_str(), namePrefix.c_str(), namePrefix.size()))
    {
      if (!it.second->isTrained)
      {
        it.second->filename = directory + it.second->name + ".dict";
        it.second->createFile();
      }
    }
  }
}

void Dict::initDicts(dynet::ParameterCollection & pc, const std::string & namePrefix)
{
  for (auto & it : str2dict)
  {
    if(!strncmp(it.first.c_str(), namePrefix.c_str(), namePrefix.size()))
    {
      if (!it.second->isTrained)
        it.second->init(pc);
      else
        it.second->initFromFile(pc);
    }
  }
}

void Dict::createFile()
{
  // If policy is Final, we didn't change any entry so no need to rewrite the file
  if (policy == Policy::Final)
    return;

  File * file = new File(filename, "w");
  FILE * fd = file->getDescriptor();

  fprintf(fd, "%s\n%d\n%s\n", name.c_str(), dimension, mode2str(mode));

  if(ftEmbeddings.get())
    fprintf(fd, "Fasttext : %s\n", ftFilename.c_str());

  delete file;
}

void Dict::save()
{
  // If policy is Final, we didn't change any entry so no need to rewrite the file
  if (policy == Policy::Final)
    return;

  File * file = new File(filename, "w");
  FILE * fd = file->getDescriptor();

  fprintf(fd, "%s\n%d\n%s\n", name.c_str(), dimension, mode2str(mode));

  if(ftEmbeddings.get())
    fprintf(fd, "Fasttext : %s\n", ftFilename.c_str());

  for (auto & it : str2index)
    fprintf(fd, "\1%s\1%u\n", it.first.c_str(), it.second);

  delete file;

  dynet::TextFileSaver s(filename, true);
  s.save(lookupParameter, "lookup");
}

unsigned int Dict::getValue(const std::string & s)
{
  auto it = str2index.find(s);
  if(it != str2index.end())
    return it->second;

  if(policy == Policy::Final && !ftEmbeddings.get())
    return getUnknownValue();

  return addEntry(s);
}

unsigned int Dict::getValueFasttext(const std::string &)
{
  unsigned int index = str2index.size();


  return index;
}

const std::string * Dict::getStr(const std::string & s)
{
  auto it = str2index.find(s);
  if(it != str2index.end())
    return &(it->first);

  if(policy == Policy::Final)
  {
    if(ftEmbeddings.get())
      return getStrFasttext(s);
    return &unknownValueStr;
  }

  addEntry(s);

  it = str2index.find(s);

  return &(it->first);
}

const std::string * Dict::getStrFasttext(const std::string & s)
{
  auto it = ftVocab.find(s);
  if(it != ftVocab.end())
    return &(it->first);

  if(s.empty())
  {
    fprintf(stderr, "ERROR (%s) : dict \'%s\' was asked to store an empty entry. Aborting.\n", ERRINFO, name.c_str());
    exit(1);
  }

  ftVocab.emplace(s, std::vector<float>(dimension, 0.0));
  auto & vec = ftVocab[s];
  ftEmbeddings->getWordVector(*ftVector.get(), s);
  memcpy(vec.data(), ftVector.get()->data(), dimension * sizeof vec[0]);

  it = ftVocab.find(s);
  return &(it->first);
}

void Dict::initParameterAsOneHot(const std::string &, unsigned int index)
{
  std::vector<float> vec(dimension, 0.0);
  vec[index] = 1.0;
  lookupParameter.initialize(index, vec);
}

void Dict::initParameterAsEmbedding(const std::string & s, unsigned int index)
{
  if (ProgramParameters::randomEmbeddings)
    return initEmbeddingRandom(index);
  if(ftEmbeddings.get())
    return initEmbeddingFromFasttext(s, index);

  initEmbeddingZero(index);
}

void Dict::initParameterAsValue(unsigned int index, const std::vector<float> & value)
{
  lookupParameter.initialize(index, value);
}

void Dict::initEmbeddingZero(unsigned int index)
{
  lookupParameter.initialize(index, std::vector<float>(dimension, 0.0));
}

void Dict::initEmbeddingRandom(unsigned int index)
{
  std::vector<float> vec(dimension, 0.0);
  int range = 1;

  for (auto & val : vec)
    val = util::getRandomValueInRange(range);

  lookupParameter.initialize(index, vec);
}

void Dict::initEmbeddingFromFasttext(const std::string & s, unsigned int index)
{
  ftEmbeddings->getWordVector(*ftVector.get(), s);
  lookupParameter.initialize(index, *(std::vector<float>*)ftVector.get());
}

unsigned int Dict::getUnknownValue()
{
  return str2index[unknownValueStr];
}

unsigned int Dict::getNullValue()
{
  return str2index[nullValueStr];
}

unsigned int Dict::addEntry(const std::string & s)
{
  if (!isInit)
  {
    fprintf(stderr, "ERROR (%s) : dict \'%s\' is not initialized. Aborting.\n", ERRINFO, name.c_str());
    exit(1);
  }

  if(s.empty())
  {
    fprintf(stderr, "ERROR (%s) : dict \'%s\' was asked to store an empty entry. Aborting.\n", ERRINFO, name.c_str());
    exit(1);
  }

  auto index = str2index.size();
  str2index.emplace(s, index);

  if ((int)str2index.size() >= dictCapacity)
  {
    fprintf(stderr, "ERROR (%s) : Dict %s of maximal capacity %d is full. Saving dict then aborting.\n", ERRINFO, name.c_str(), dictCapacity);
    save();
    exit(1);
  }

  if(mode == Mode::OneHot)
  {
    if(oneHotIndex >= dimension)
      fprintf(stderr, "WARNING (%s) : Dict %s of dimension %d is asked to store %d elements in one-hot.\n", ERRINFO, name.c_str(), dimension, oneHotIndex+1);
    else
    {
      initParameterAsOneHot(s, index);
      oneHotIndex++;
    }
  }
  else
    initParameterAsEmbedding(s, index);

  return index;
}

unsigned int Dict::addEntry(const std::string & s, const std::vector<float> & embedding)
{
  if (!isInit)
  {
    fprintf(stderr, "ERROR (%s) : dict \'%s\' is not initialized. Aborting.\n", ERRINFO, name.c_str());
    exit(1);
  }

  if(s.empty())
  {
    fprintf(stderr, "ERROR (%s) : dict \'%s\' was asked to store an empty entry. Aborting.\n", ERRINFO, name.c_str());
    exit(1);
  }

  auto index = str2index.size();
  str2index.emplace(s, index);

  if ((int)str2index.size() >= dictCapacity)
  {
    fprintf(stderr, "ERROR (%s) : Dict %s of maximal capacity %d is full. Aborting.\n", ERRINFO, name.c_str(), dictCapacity);
    exit(1);
  }

  if(mode == Mode::OneHot)
  {
    if(oneHotIndex >= dimension)
      fprintf(stderr, "WARNING (%s) : Dict %s of dimension %d is asked to store %d elements in one-hot.\n", ERRINFO, name.c_str(), dimension, oneHotIndex+1);
    else
    {
      initParameterAsOneHot(s, index);
      oneHotIndex++;
    }
  }
  else
    initParameterAsValue(index, embedding);

  return index;
}

Dict * Dict::getDict(Policy policy, const std::string & filename)
{
  auto it = str2dict.find(filename);
  if(it != str2dict.end())
    return it->second.get();

  Dict * dict = new Dict(util::removeSuffix(util::getFilenameFromPath(filename), ".dict"),policy, filename, ProgramParameters::dictCapacity);

  str2dict.insert(std::make_pair(dict->name, std::unique_ptr<Dict>(dict)));

  return str2dict[dict->name].get();
}

Dict * Dict::getDict(const std::string & name)
{
  std::string relativeName = currentClassifierName + "_" + name;
  auto it = str2dict.find(relativeName);
  if(it != str2dict.end())
    return it->second.get();

  it = str2dict.find(name);
  if(it != str2dict.end())
    return it->second.get();

  fprintf(stderr, "ERROR (%s) : dictionary \'%s\' does not exists. Aborting.\n", ERRINFO, relativeName.c_str());
  exit(1);

  return nullptr;
}

void Dict::readDicts(const std::string & directory, const std::string & filename, bool trainMode)
{
  char buffer[1024];
  char name[1024];
  char modeStr[1024];
  char pretrained[1024];
  int dim;
  int dictCapacity;

  File file(filename, "r");
  FILE * fd = file.getDescriptor();

  while(fscanf(fd, "%[^\n]\n", buffer) == 1)
  {
    if(buffer[0] == '#')
      continue;

    if(trainMode)
    {
      if(sscanf(buffer, "%s %d %s %s %d\n", name, &dim, modeStr, pretrained, &dictCapacity) != 5)
      {
        if(sscanf(buffer, "%s %d %s %s\n", name, &dim, modeStr, pretrained) != 4)
        {
          if(sscanf(buffer, "%s %d %s\n", name, &dim, modeStr) != 3)
          {
            fprintf(stderr, "ERROR (%s) : line \'%s\' do not describe a dictionary. Aborting.\n", ERRINFO, buffer);
            exit(1);
          }
          sprintf(pretrained, "_");
        }

        dictCapacity = ProgramParameters::dictCapacity;
      }

      auto it = str2dict.find(name);
      if(it != str2dict.end())
      {
        fprintf(stderr, "ERROR (%s) : dictionary \'%s\' already exists. Aborting.\n", ERRINFO, name);
        exit(1);
      }

      if (std::string(pretrained) == "_")
      {
        if (ProgramParameters::newTemplatePath == ProgramParameters::expPath)
        {
          std::string probableFilename = ProgramParameters::expPath + name + std::string(".dict");
          str2dict.insert(std::make_pair(name, std::unique_ptr<Dict>(new Dict(name, Policy::Modifiable, probableFilename, dictCapacity))));
        }
        else
        {
          str2dict.insert(std::make_pair(name, std::unique_ptr<Dict>(new Dict(name, dim, str2mode(modeStr), dictCapacity))));
        }
      }
      else
      {
        str2dict.insert(std::make_pair(name, std::unique_ptr<Dict>(new Dict(name, Policy::Final, directory + pretrained, dictCapacity))));
      }
    }
    else
    {
      if(sscanf(buffer, "%s %d %s %s\n", name, &dim, modeStr, pretrained) != 4)
      {
        if(sscanf(buffer, "%s %d %s\n", name, &dim, modeStr) != 3)
        {
          fprintf(stderr, "ERROR (%s) : line \'%s\' do not describe a dictionary. Aborting.\n", ERRINFO, buffer);
          exit(1);
        }
        sprintf(pretrained, "_");
      }

      auto it = str2dict.find(name);
      if(it != str2dict.end())
      {
        fprintf(stderr, "ERROR (%s) : dictionary \'%s\' already exists. Aborting.\n", ERRINFO, name);
        exit(1);
      }

      if (std::string(pretrained) == "_")
      {
        str2dict.insert(std::make_pair(name, std::unique_ptr<Dict>(new Dict(name, Policy::Final, directory + name + ".dict", dictCapacity))));
      }
      else
      {
        str2dict.insert(std::make_pair(name, std::unique_ptr<Dict>(new Dict(name, Policy::Final, directory + pretrained, dictCapacity))));
      }
    }
  }
}

int Dict::getDimension()
{
  return dimension;
}

void Dict::printForDebug(FILE * output)
{
  fprintf(output, "Dict name \'%s\' nbElems = %lu\n", name.c_str(), str2index.size());
}

void Dict::deleteDicts()
{
  str2dict.clear();
}

dynet::LookupParameter & Dict::getLookupParameter()
{
  return lookupParameter;
}

