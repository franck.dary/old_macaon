/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// \file macaon_convert_embeddings.cpp
/// \author Franck Dary
/// @version 1.0
/// @date 2019-04-16

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "File.hpp"
#include "util.hpp"
#include "Dict.hpp"
#include "ProgramParameters.hpp"
#include <boost/program_options.hpp>

namespace po = boost::program_options;

/// @brief Get the list of mandatory and optional program arguments.
///
/// @return The lists.
po::options_description getOptionsDescription()
{
  po::options_description desc("Command-Line Arguments ");

  po::options_description req("Required");
  req.add_options()
    ("input,i", po::value<std::string>()->required(),
      "File containing the embeddings")
    ("output,o", po::value<std::string>()->required(),
      "Name of the desired output file")
    ("dictCapacity", po::value<int>()->default_value(30000),
      "The maximal size of each Dict (number of differents embeddings).");

  po::options_description opt("Optional");
  opt.add_options()
    ("help,h", "Produce this help message")
    ("debug,d", "Print infos on stderr");

  desc.add(req).add(opt);
  return desc;
}

/// @brief Store the program arguments inside a variables_map
///
/// @param od The description of all the possible options.
/// @param argc The number of arguments given to this program.
/// @param argv The values of arguments given to this program.
///
/// @return The variables map
po::variables_map checkOptions(po::options_description & od, int argc, char ** argv)
{
  po::variables_map vm;

  try {po::store(po::parse_command_line(argc, argv, od), vm);}
  catch(std::exception& e)
  {
    std::cerr << "Error: " << e.what() << "\n";
    od.print(std::cerr);
    exit(1);
  }

  if (vm.count("help"))
  {
    std::cout << od << "\n";
    exit(0);
  }

  try {po::notify(vm);}
  catch(std::exception& e)
  {
    std::cerr << "Error: " << e.what() << "\n";
    od.print(std::cerr);
    exit(1);
  }

  return vm;
}

/// @brief Given a fplm file (pairs of word / lemma), compute rules that will transform these words into lemmas, as well as exceptions.
///
/// @param argc The number of arguments given to this program.
/// @param argv[] Array of arguments given to this program.
///
/// @return 0 if there was no crash.
int main(int argc, char * argv[])
{
  auto od = getOptionsDescription();

  po::variables_map vm = checkOptions(od, argc, argv);

  std::string inputFilename = vm["input"].as<std::string>();
  std::string outputFilename = vm["output"].as<std::string>();
  ProgramParameters::dictCapacity = vm["dictCapacity"].as<int>();

  File input(inputFilename, "r");

  int nbEmbeddings;
  int embeddingsSize;
  char buffer[100000];
  if (fscanf(input.getDescriptor(), "%d %d\n", &nbEmbeddings, &embeddingsSize) != 2)
  {
    fprintf(stderr, "ERROR (%s) : Wrong format, expected numberOfEmbeddings and embeddingsSize. Aborting.\n", ERRINFO);
    exit(1);
  }

  std::vector<float> embedding;

  dynet::initialize(argc, argv);
  dynet::ParameterCollection pc;
  Dict dict(outputFilename, embeddingsSize, Dict::Mode::Embeddings, nbEmbeddings+5);
  dict.init(pc);

  while (fscanf(input.getDescriptor(), "%[^\n]\n", buffer) == 1)
  {
    embedding.clear();
    auto splited = util::split(buffer, ' ');
    if ((int)splited.size() != embeddingsSize+1)
    {
      fprintf(stderr, "ERROR (%s) : line \'%s\' wrong format. Aborting.\n", ERRINFO, buffer);
      exit(1);
    }

    for (unsigned int i = 1; i < splited.size(); i++)
      embedding.emplace_back(std::stof(splited[i]));

    dict.addEntry(splited[0], embedding);
  }

  dict.save();

  return 0;
}

