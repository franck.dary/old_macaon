/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "util.hpp"
#include "File.hpp"
#include "utf8.hpp"
#include <algorithm>
#include <cstring>
#include <ctime>

namespace util
{
bool isAlpha(char c)
{
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

bool isNum(char c)
{
	return c >= '0' && c <= '9';
}

bool isSeparator(char c)
{
	return (c == EOF) || endLine(c) || (c == ' ' || c == '\t');
}

bool isUtf8Space(const std::string::iterator & c)
{
  auto it = c;
  utf8::next(it, it+10);
  int nbChar = it - c;

  if (nbChar == 1)
  {
    if ((unsigned char)c[0] == 0x20)
      return true;
  }
  else if (nbChar == 2)
  {
    if ((unsigned char)c[0] == 0xC2 && (unsigned char)c[1] == 0xA0)
      return true;
  }
  else if (nbChar == 3)
  {
    if ((unsigned char)c[0] == 0xE1 && (unsigned char)c[1] == 0x9A && (unsigned char)c[2] == 0x80)
      return true;
    if ((unsigned char)c[0] == 0xE2 && (unsigned char)c[1] == 0x80 && (unsigned char)c[2] >= 0x80 && (unsigned char)c[2] <= 0x89)
      return true;
    if ((unsigned char)c[0] == 0xE2 && (unsigned char)c[1] == 0x80 && (unsigned char)c[2] == 0x8A)
      return true;
    if ((unsigned char)c[0] == 0xE2 && (unsigned char)c[1] == 0x80 && (unsigned char)c[2] == 0xAF)
      return true;
    if ((unsigned char)c[0] == 0xE2 && (unsigned char)c[1] == 0x81 && (unsigned char)c[2] == 0x9F)
      return true;
    if ((unsigned char)c[0] == 0xE3 && (unsigned char)c[1] == 0x80 && (unsigned char)c[2] == 0x80)
      return true;
  }

  return false;
}

bool isUtf8Separator(const std::string::iterator & c)
{
  return (c[0] == EOF) || endLine(c[0]) ||  (c[0] == '\t') || isUtf8Space(c);
}

bool isNotSeparator(char c)
{
  return !isSeparator(c);
}

std::string getFilenameFromPath(const std::string & s)
{
	std::string result;

  for (int i = s.size()-1; i >= 0 && s[i] != '/'; i--)
	{
    result.push_back(s[i]);
	}

	std::reverse(result.begin(), result.end());

	return result;
}

unsigned int lengthPrinted(const std::string & s)
{
  return getNbSymbols(s);
}

bool isUpper(char c)
{
	return c >= 'A' && c <= 'Z';
}

bool endSentence(char c)
{
	return c == '.' || c == '!' || c == '?' || c == '\n' || c == EOF;
}

bool endLine(char c)
{
	return c == '\n' || c == EOF || c == '\r' || c == 10;
}

bool isNewline(char c)
{
	return c == 10 || c == 13;
}

void toLowerCase(std::string & s, unsigned int i)
{
	// Uncapitalize basic letters
	if (s[i] >= 'A' && s[i] <= 'Z')
	{
		s[i] += -'A' + 'a';
		return;
	}

	// Uncapitalize accentuated letters
	if (i == s.size()-1)
		return;
	unsigned char s1 = s[i], s2 = s[i+1];
	if (s1 == 195 && s2 >= 128 && s2 <= 158)
	{
		s2 += 32;
		s[i+1] = s2;
		return;
	}
}

std::string toLowerCase(std::string s)
{
  for (auto & letter : s)
    if(letter >= 'A' && letter <= 'Z')
      letter += 'a' - 'A';

  return s;
}

void toUpperCase(std::string & s, unsigned int i)
{
	// Capitalize basic letters
	if (s[i] >= 'a' && s[i] <= 'z')
	{
		s[i] += 'A' - 'a';
		return;
	}
}

std::string noAccentLower(const std::string & s)
{
  return toLowerCase(toUpperCase(s));
}

std::string toUpperCase(const std::string & s)
{
  std::string res;

  for (unsigned int i = 0; i < s.size(); i++)
  {
    if (s[i] >= 0)
    {
	    if (s[i] >= 'a' && s[i] <= 'z')
        res.push_back(s[i] + 'A' - 'a');
      else
        res.push_back(s[i]);
    }
    else
    {
      if(s[i+1] <= -85 && s[i+1] >= -88)
        res.push_back('E');
      else if (s[i+1] <= -92 && s[i+1] >= -96)
        res.push_back('A');
      else if (s[i+1] <= -74 && s[i+1] >= -76)
        res.push_back('O');
      else if (s[i+1] <= -68 && s[i+1] >= -71)
        res.push_back('U');
      else if (s[i+1] <= -81 && s[i+1] >= -84)
        res.push_back('I');
      else if (s[i+1] <= -65 && s[i+1] >= -65)
        res.push_back('Y');
      else if (s[i] == -61)
        res.push_back('C');
      else
        res.push_back('?');
      i++;
    }
  }

  return res;
}

std::vector<std::string> split(const std::string & s)
{
  std::vector<std::string> res;
  res.emplace_back();

  for (unsigned int i = 0; i < s.size(); i++)
  {
    if (isSeparator(s[i]))
    {
      res.emplace_back();

      while (isSeparator(s[i]))
        i++;

      i--;
      continue;
    }

    res.back().push_back(s[i]);
  }

  if (res.back().empty())
    res.pop_back();

  return res;
}

std::vector<std::string> split(const std::string & s, char sep)
{
  std::vector<std::string> res;
  res.emplace_back();

  for (unsigned int i = 0; i < s.size(); i++)
  {
    if (s[i] == sep)
    {
      res.emplace_back();

      while (s[i] == sep)
        i++;

      i--;
      continue;
    }

    res.back().push_back(s[i]);
  }

  if (!res.empty() && res.back().empty())
    res.pop_back();

  if (res.empty())
  {
    fprintf(stderr, "ERROR (%s) : asked to split \'%s\' with separator \'%c\'. Aborting.\n", ERRINFO, s.c_str(), sep);
    exit(1);
  }

  return res;
}

std::string getRule(const std::string & Ufrom, const std::string & Uto)
{
  std::string from = toLowerCase(Ufrom);
  std::string to = toLowerCase(Uto);

  int fromL = getNbSymbols(from);
  int toL = getNbSymbols(to);
  int minL = std::min(fromL, toL);

  int longestCommonPrefix = 0;

  for (int i = 0; i < minL; i++)
  {
    int limitFrom = getEndIndexOfNthSymbol(from, i);
    int limitTo = getEndIndexOfNthSymbol(to, i);

    if (limitFrom == limitTo && !memcmp(from.c_str(), to.c_str(), limitFrom+1))
      longestCommonPrefix++;
    else
      break;
  }

  int prefixEndIndex = getEndIndexOfNthSymbol(from, longestCommonPrefix-1);
  int suffixStartIndex = prefixEndIndex + 1;

  std::string toDelete(from.begin()+suffixStartIndex, from.end());
  std::string toAdd(to.begin()+suffixStartIndex, to.end());

  return "@" + toDelete + "@" + toAdd;
}

bool ruleIsAppliable(const std::string & Ufrom, const std::string & rule)
{
  std::string from = toLowerCase(Ufrom);

  unsigned int sepIndex;
  for(sepIndex = 1; sepIndex < rule.size(); sepIndex++)
    if(rule[sepIndex] == '@')
      break;

  int suffixLength = sepIndex - 1;

  if (suffixLength > (int)Ufrom.size())
    return false;

  for(unsigned int j = 1, i = from.size() - suffixLength; j < sepIndex; i++, j++)
    if((rule[j] != '*') && (rule[j] != from[i]))
      return false;

  return true;
}

std::string applyRule(const std::string & from, const std::string & rule)
{
  if(from.empty())
  {
    fprintf(stderr, "ERROR (%s) : trying to apply rule to empty string. Aborting.\n", ERRINFO);
    exit(1);
  }
  if(rule.empty())
  {
    fprintf(stderr, "ERROR (%s) : trying to apply empty rule to string. Aborting.\n", ERRINFO);
    exit(1);
  }

  if (!ruleIsAppliable(from, rule))
    return applyRule(from, "@@");

  unsigned int sepIndex;
  for(sepIndex = 1; sepIndex < rule.size(); sepIndex++)
    if(rule[sepIndex] == '@')
      break;

  if(sepIndex >= rule.size())
  {
    fprintf(stderr, "ERROR (%s) : trying to apply bad rule '%s'. Aborting.\n", ERRINFO, rule.c_str());
    exit(1);
  }

  unsigned int fromSuffixLength = sepIndex - 1;
  unsigned int toSuffixLength = rule.size() - 1 - sepIndex;

  std::string to = from;
  while(to.size() < from.size() + toSuffixLength - fromSuffixLength)
    to.push_back('a');
  while(to.size() > from.size() + toSuffixLength - fromSuffixLength)
    to.pop_back();

  for(unsigned int j = 0, i = from.size() - fromSuffixLength; j < toSuffixLength; i++, j++)
    to[i] = rule[sepIndex + j + 1];

  return to;
}

void printColumns(FILE * output, const std::vector< std::vector<std::string> > & cols, int nbSpaces)
{
  std::vector<int> sizes;

  for (auto & col : cols)
  {
    int maxSize = 0;
    for (auto & s : col)
      maxSize = std::max<int>(maxSize, lengthPrinted(s));

    sizes.emplace_back(maxSize);
  }

  for (unsigned int i = 0; i < cols[0].size(); i++)
  {
    for (unsigned int j = 0; j < cols.size(); j++)
    {
      int len = lengthPrinted(cols[j][i]);
      fprintf(output, "%s", cols[j][i].c_str());
      for(int k = 0; k < nbSpaces + sizes[j] - len; k++)
        fprintf(output, " ");
    }

    fprintf(output, "\n");
  }
}

std::string float2str(float f, const char * format)
{
  static char buffer[1024];

  sprintf(buffer, format, f);

  return std::string(buffer);
}

bool isNum(const std::string & s)
{
  bool digitHapened = false;
  bool dotHapened = false;

  for (unsigned int i = 0; i < s.size(); i++)
  {
    if (s[i] == '.')
    {
      if (dotHapened || !digitHapened)
        return false;
      dotHapened = true;
      continue;
    }

    if ((i == 0 && s[i] != '+' && s[i] != '-' && !isNum(s[i])) || (i != 0 && !isNum(s[i])))
      return false;

    if (isNum(s[i]))
      digitHapened = true;
  }

  return true;
}

std::string removeSuffix(const std::string & s, const std::string & suffix)
{
  int lastIndex = s.size()-1;
  while (lastIndex >= 0 && s[lastIndex] == suffix[suffix.size()-1-(s.size()-1-lastIndex)])
    lastIndex--;

  std::string result = std::string(s.begin(), s.begin()+lastIndex+1);

  return result;
}

std::string getTime()
{
  time_t rawtime;
  char buffer[80];

  time(&rawtime);

  strftime(buffer, sizeof(buffer), "%H:%M:%S", localtime(&rawtime));
  return std::string(buffer);
}

bool fileExists(const std::string & s)
{
  FILE * f = fopen(s.c_str(), "r");

  if (!f)
    return false;

  fclose(f);
  return true;
}

float getRandomValueInRange(int range)
{
  int maxValue = 1000000;

  float sign = choiceWithProbability(0.5) ? 1.0 : -1.0; 
  float result = sign*range*(rand() % (maxValue+1)) / maxValue;

  return result;
}

bool choiceWithProbability(float probability)
{
  if (probability < 0 || probability > 1.0)
  {
    fprintf(stderr, "ERROR (%s) : invalid probability \'%f\'. Aborting.\n", ERRINFO, probability);
    exit(1);
  }

  int maxVal = 100000;
  int threshold = maxVal * probability;

  return (rand() % maxVal) < threshold;
}

std::string int2humanStr(int number)
{
  std::string nb = std::to_string(number);
  std::string result;

  for (unsigned int i = 0; i < nb.size(); i++)
  {
    result.push_back(nb[i]);
    if (((nb.size()-i-1) % 3 == 0) && i < nb.size()-1)
      result.push_back(' ');
  }

  return result;
}

int getNbLines(const std::string & filename)
{
  File file(filename, "r");

  char buffer[10000];
  
  int nbLines = 0;
  while (fscanf(file.getDescriptor(), "%[^\n]\n", buffer) == 1)
    nbLines++;

  return nbLines;
}

int getStartIndexOfNthSymbol(const std::string & s, int n)
{
  auto it = s.begin();
  for (int i = 0; i < n; i++)
    try {utf8::next(it, s.end());}
    catch (utf8::not_enough_room &) {return -1;}

  return it - s.begin();
}

int getStartIndexOfNthSymbolFrom(const std::string::iterator & s, const std::string::iterator & end, int n)
{
  if (n >= 0)
  {
    auto it = s;
    for (int i = 0; i < n; i++)
      try {utf8::next(it, end);}
      catch (utf8::not_enough_room &) {return -1;}

    return it - s;
  }

  auto it = s;
  for (int i = 0; i < -n; i++)
    try {utf8::prior(it, end);}
    catch (utf8::not_enough_room &) {return 1;}
  
  return it - s;
}

int getEndIndexOfNthSymbol(const std::string & s, int n)
{
  auto it = s.begin();
  for (int i = 0; i < n+1; i++)
    try {utf8::next(it, s.end());}
    catch (utf8::not_enough_room &) {return i == n ? s.end() - s.begin() - 1 : -1;}

  return (it-1) - s.begin();
}

int getEndIndexOfNthSymbolFrom(const std::string::iterator & s, const std::string::iterator & end, int n)
{
  auto it = s;
  for (int i = 0; i < n+1; i++)
    try {utf8::next(it, end);}
    catch (utf8::not_enough_room &) {return i == n ? end - s - 1 : -1;}

  return (it-1) - s;
}

unsigned int getNbSymbols(const std::string & s)
{
  return utf8::distance(s.begin(), s.end());
}

std::string shrinkString(const std::string & base, int maxSize, const std::string token)
{
  int baseSize = getNbSymbols(base);
  int tokenSize = getNbSymbols(token);

  if (baseSize <= maxSize)
  {
    if (!utf8::is_valid(base.begin(), base.end()))
    {
      fprintf(stderr, "ERROR (%s) : ShrinkString with base=<%s> maxSize=<%d> token=<%s> produced the following invalid UTF-8 string <%s>. Aborting.\n", ERRINFO, base.c_str(), maxSize, token.c_str(), base.c_str());
      exit(1);
    }
    return base;
  }

  int nbToTakeBegin = ((maxSize-tokenSize) / 2) + ((maxSize-tokenSize)%2);
  int nbToTakeEnd = maxSize-tokenSize-nbToTakeBegin;

  std::string result(base.begin(), base.begin()+getEndIndexOfNthSymbol(base, nbToTakeBegin-1)+1);
  std::string strBegin = result;
  result += token;
  result += std::string(base.begin()+getStartIndexOfNthSymbol(base,baseSize-nbToTakeEnd), base.end());

  if (!utf8::is_valid(result.begin(), result.end()))
  {
    fprintf(stderr, "ERROR (%s) : ShrinkString with base=<%s> maxSize=<%d> token=<%s> produced the following invalid UTF-8 string <%s>. Aborting.\n", ERRINFO, base.c_str(), maxSize, token.c_str(), result.c_str());
    exit(1);
  }

  return result;
}

std::string strip(const std::string & s)
{
  std::string res;
  unsigned int i = 0;
  while (i < s.size() && isSeparator(s[i]))
    i++;
  while (i < s.size() && !isSeparator(s[i]))
    res.push_back(s[i++]);

  return res;
}

};
