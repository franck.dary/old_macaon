/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "ProgramParameters.hpp"

ProgramParameters::ProgramParameters()
{
}

std::string ProgramParameters::input;
std::string ProgramParameters::expName;
std::string ProgramParameters::baseExpName;
std::string ProgramParameters::expPath;
std::string ProgramParameters::langPath;
std::string ProgramParameters::templatePath;
std::string ProgramParameters::newTemplatePath;
std::string ProgramParameters::templateName;
std::string ProgramParameters::tmFilename;
std::string ProgramParameters::tmName;
std::string ProgramParameters::bdFilename;
std::string ProgramParameters::bdName;
std::string ProgramParameters::mcdFilename;
std::string ProgramParameters::mcdName;
bool ProgramParameters::debug;
std::string ProgramParameters::trainFilename;
std::string ProgramParameters::trainName;
std::string ProgramParameters::devFilename;
std::string ProgramParameters::devName;
std::string ProgramParameters::lang;
std::string ProgramParameters::optimizer;
std::string ProgramParameters::dicts;
int ProgramParameters::nbIter;
int ProgramParameters::seed;
bool ProgramParameters::removeDuplicates;
bool ProgramParameters::shuffleExamples;
float ProgramParameters::learningRate;
float ProgramParameters::beta1;
float ProgramParameters::beta2;
float ProgramParameters::bias;
bool ProgramParameters::interactive;
int ProgramParameters::dynamicEpoch;
float ProgramParameters::dynamicProbability;
int ProgramParameters::showFeatureRepresentation;
bool ProgramParameters::randomEmbeddings;
bool ProgramParameters::randomParameters;
bool ProgramParameters::printEntropy;
bool ProgramParameters::printTime;
bool ProgramParameters::errorAnalysis;
bool ProgramParameters::meanEntropy;
bool ProgramParameters::onlyPrefixes;
int ProgramParameters::iterationSize;
int ProgramParameters::nbTrain;
std::string ProgramParameters::sequenceDelimiterTape;
std::string ProgramParameters::sequenceDelimiter;
std::string ProgramParameters::classifierName;
int ProgramParameters::batchSize;
std::string ProgramParameters::loss;
std::map<std::string,std::string> ProgramParameters::featureModelByClassifier;
int ProgramParameters::nbErrorsToShow;
int ProgramParameters::nbIndividuals;
int ProgramParameters::beamSize;
int ProgramParameters::nbChilds;
int ProgramParameters::tapeSize;
int ProgramParameters::devTapeSize;
int ProgramParameters::readSize;
bool ProgramParameters::printOutputEntropy;
int ProgramParameters::dictCapacity;
std::string ProgramParameters::tapeToMask;
float ProgramParameters::maskRate;
bool ProgramParameters::featureExtraction;
bool ProgramParameters::randomDebug;
float ProgramParameters::randomDebugProbability;
bool ProgramParameters::alwaysSave;
bool ProgramParameters::noNeuralNetwork;
bool ProgramParameters::showActions;
bool ProgramParameters::delayedOutput;
int ProgramParameters::maxStackSize;
bool ProgramParameters::rawInput;

