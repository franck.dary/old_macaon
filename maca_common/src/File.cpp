/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "File.hpp"

File File::stdOut(stdout);

File::File(FILE * file)
{
	this->file = file;
}

File::File(const File & model)
{
	this->buffer = model.buffer;
	this->endHasBeenReached = model.endHasBeenReached;
	this->filename = model.filename;

  std::fpos_t otherPos;
  std::fgetpos(model.file, &otherPos);

  file = fopen(filename.c_str(), "r");
	if (!file)
	{
		fprintf(stderr, "ERROR (%s) : cannot open file %s\n", ERRINFO, filename.c_str());

		exit(1);
	}

  std::fsetpos(file, &otherPos);
}

char File::peek()
{
	if (!buffer.empty())
		return buffer.top();

	if (endHasBeenReached)
		return EOF;

	char c = fgetc(file);

	if (c == EOF)
		endHasBeenReached = true;
	else
		ungetc(c, file);

	return c;
}

File::File(const std::string & filename, const std::string & mode)
{
	this->filename = filename;
	endHasBeenReached = false;

	if (mode != "r" && mode != "w" && mode != "a")
	{
		fprintf(stderr, "ERROR (%s) : \"%s\" is an invalid mode when opening a file\n", ERRINFO, mode.c_str());

		exit(1);
	}

	if (filename == "stdin")
	{
		file = stdin;
		return;
	}
	else if (filename == "stdout")
	{
		file = stdout;
		return;
	}

	file = fopen(filename.c_str(), mode.c_str());

	if (!file)
	{
		fprintf(stderr, "ERROR (%s) : cannot open file \'%s\'\n", ERRINFO, filename.c_str());

		exit(1);
	}
}

File::~File()
{
	if (file != stdin && file != stdout)
		fclose(file);
}

bool File::isFinished()
{
	return buffer.empty() && peek() == EOF;
}

char File::getChar()
{
	if (buffer.empty())
		return getc(file);

	char result = buffer.top();
	buffer.pop();

	return result;
}

void File::ungetChar(char c)
{
	buffer.push(c);
}

FILE * File::getDescriptor()
{
	return file;
}

const std::string & File::getName()
{
	return filename;
}

void File::readUntil(char c)
{
	while (!isFinished() && peek() != c)
    getChar();
}

void File::readUntil(const std::function<bool(char)> & condition)
{
	while (!isFinished() && !condition(peek()))
    getChar();
}

void File::readUntil(std::string & dest, const std::function<bool(char)> & condition)
{
	while (!isFinished() && !condition(peek()))
		dest.push_back(getChar());
}

void File::rewind()
{
	fclose(file);

	endHasBeenReached = false;
	while (!buffer.empty())
		buffer.pop();

	file = fopen(filename.c_str(), "r");

	if (!file)
	{
		fprintf(stderr, "ERROR (%s) : Cannot open file %s\n", ERRINFO, filename.c_str());

		exit(1);
	}
}

