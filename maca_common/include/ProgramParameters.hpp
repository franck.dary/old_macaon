/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file ProgramParameters.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-09-27
#ifndef PROGRAMPARAMETERS__H
#define PROGRAMPARAMETERS__H

#include <string>
#include <map>

struct ProgramParameters
{
  static std::string input;
  static std::string expName;
  static std::string expPath;
  static std::string baseExpName;
  static std::string langPath;
  static std::string templatePath;
  static std::string newTemplatePath;
  static std::string templateName;
  static std::string tmFilename;
  static std::string tmName;
  static std::string bdFilename;
  static std::string bdName;
  static std::string mcdFilename;
  static std::string mcdName;
  static bool debug;
  static std::string trainFilename;
  static std::string trainName;
  static std::string devFilename;
  static std::string devName;
  static std::string lang;
  static std::string optimizer;
  static int nbIter;
  static int seed;
  static bool removeDuplicates;
  static bool shuffleExamples;
  static float learningRate;
  static float beta1;
  static float beta2;
  static float bias;
  static bool interactive;
  static int dynamicEpoch;
  static float dynamicProbability;
  static int showFeatureRepresentation;
  static int iterationSize;
  static int nbTrain;
  static bool randomEmbeddings;
  static bool randomParameters;
  static bool printEntropy;
  static bool printTime;
  static std::string sequenceDelimiterTape;
  static std::string sequenceDelimiter;
  static std::string classifierName;
  static int batchSize;
  static std::string loss;
  static std::string dicts;
  static bool errorAnalysis;
  static bool meanEntropy;
  static bool onlyPrefixes;
  static std::map<std::string,std::string> featureModelByClassifier;
  static int nbErrorsToShow;
  static int nbIndividuals;
  static int beamSize;
  static int nbChilds;
  static int tapeSize;
  static int devTapeSize;
  static int readSize;
  static int dictCapacity;
  static bool printOutputEntropy;
  static std::string tapeToMask;
  static float maskRate;
  static bool featureExtraction;
  static bool randomDebug;
  static float randomDebugProbability;
  static bool alwaysSave;
  static bool noNeuralNetwork;
  static bool showActions;
  static bool delayedOutput;
  static int maxStackSize;
  static bool rawInput;

  private :

  ProgramParameters();
};

#endif
