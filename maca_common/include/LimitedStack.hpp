/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file LimitedStack.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2019-01-21

#ifndef LIMITEDSTACK__H
#define LIMITEDSTACK__H

#include <vector>
#include <unordered_map>
#include "Action.hpp"

namespace std
{
  template<>
  struct hash< pair<string, Action> >
  {
    size_t operator()(const pair<std::string, Action> & p) const
    {
      static hash<string> h;
      return h(p.first + p.second.name);
    }
  };
}

template<typename T>
class LimitedStack
{
  private :

  std::vector<T> data;
  std::unordered_map<T, int> present;
  int nbElements;
  int lastElementIndex;

  public :

  LimitedStack(unsigned int limit) : data(limit)
  {
    clear();
  }

  void clear()
  {
    nbElements = 0;
    lastElementIndex = -1; 
    present.clear();
  }

  void push(T elem)
  {
    nbElements++;
    if (nbElements > (int)data.size())
      nbElements = data.size();
    lastElementIndex++;
    if (lastElementIndex >= (int)data.size())
      lastElementIndex = 0;

    present[data[lastElementIndex]]--;
    if (present[data[lastElementIndex]] <= 0)
      present.erase(present.find(data[lastElementIndex]));

    data[lastElementIndex] = elem;
    present[data[lastElementIndex]]++;
  }

  T pop()
  {
    if (nbElements <= 0)
    {
      fprintf(stderr, "ERROR (%s) : popping stack of size %d. Aborting.\n", ERRINFO, nbElements);
      exit(1);
    }

    int elementIndex = lastElementIndex;

    nbElements--;
    lastElementIndex--;

    if (lastElementIndex < 0)
      lastElementIndex = data.size() - 1;

    present[data[elementIndex]]--;
    return data[elementIndex];
  }

  T top()
  {
    if (nbElements <= 0)
    {
      fprintf(stderr, "ERROR (%s) : topping stack of size %d. Aborting.\n", ERRINFO, nbElements);
      exit(1);
    }

    return data[lastElementIndex];
  }

  bool empty()
  {
    return nbElements == 0;
  }

  bool contains(const T & element)
  {
    return present[element] > 0;
  }

  unsigned int size()
  {
    return nbElements;
  }

  T getElem(int index)
  {
    if (index < 0 || index >= nbElements)
    {
      fprintf(stderr, "ERROR (%s) : requesting element %d in stack of size %d. Aborting.\n", ERRINFO, index, nbElements);
      exit(1);
    }

    int sol = lastElementIndex - index;
    while (sol < 0)
      sol += data.size();

    return data[sol];
  }
};

#endif
