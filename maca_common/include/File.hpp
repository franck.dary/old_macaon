/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file File.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-03

#ifndef FILE__H
#define FILE__H

#include <cstdio>
#include <stack>
#include <string>
#include <functional>
#include "util.hpp"

/// @brief A class for opening and reading a file.
///
/// It can simply be seen as a wrapper for FILE *\n
/// It provides usefull functions and also permit to unget a large number of char
/// in a row, something that we can't do with FILE * alone.
class File
{
	public : 
	
  /// @brief Instance of File wrapped around stdout.
	static File stdOut;

	private :

  /// @brief Construct a File from a FILE *
  ///
  /// @param file Already opened file.
	File(FILE * file);

	private :

  /// @brief Pointer to the file description.
	FILE * file;
  /// @brief A buffer used to store char from ungetChar.
	std::stack<char> buffer;
  /// @brief Whether or not the whole file has been read.
	bool endHasBeenReached;
  /// @brief The filename, usefull for the rewind function.
	std::string filename;

	public :

  /// @brief Construct and open a new file.
  ///
  /// @param filename The filename of the file.
  /// @param mode C's style opening mode. "r"ead or "w"rite.
	File(const std::string & filename, const std::string & mode);
  /// @brief Open the file again and place the reading cursor at the same point.
  ///
  /// @param model 
	File(const File & model);
  /// @brief File is not copyable.
  ///
  /// @param model
  ///
  /// @return 
	File & operator=(const File & model) = delete;
  /// @brief The destructor, in which we close the file.
	~File();
  /// @brief Peek the next char in the file but without consuming it.
  ///
  /// @return The next char in the file.
	char peek();
  /// @brief Whether or not the whole file has been read.
  ///
  /// @return Whether or not the whole file has been read.
	bool isFinished();
  /// @brief Get the next char in the file and consume it.
  ///
  /// @return The next char in the file.
	char getChar();
  /// @brief Put back a char in the File.
  ///
  /// It is not mandatory to use a char that was read from the file.\n
  /// The next call to getChar will return c.\n
  /// You can call ungetChar successively without problem.
  /// @param c The char that will be pulled back.
	void ungetChar(char c);
  /// @brief Return the underlying file descriptor.
  ///
  /// This is usefull if you need to use fscanf or fprintf.
  /// @return The file descriptor.
	FILE * getDescriptor();
  /// @brief Get the filename of the File.
  ///
  /// @return The filename.
	const std::string & getName();
  /// @brief Read the file until the character c is met.
  ///
  /// The next call to getChar will return c.
  /// @param c The character to read until.
	void readUntil(char c);
  /// @brief Read the file until the current character meets a condition.
  ///
  /// The next call to condition(getChar()) will return true.
  /// @param condition The condition the read char will have to meet.
	void readUntil(const std::function<bool(char)> & condition);
  /// @brief Read the file until the current character meets a condition.
  ///
  /// The next call to condition(getChar()) will return true.
  /// @param dest Where to store all the read characters.
  /// @param condition The condition the read char will have to meet.
	void readUntil(std::string & dest, const std::function<bool(char)> & condition);
  /// @brief Read the file again from the start, in "r"ead mode.
	void rewind();
};

#endif
