/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file LimitedArray.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2019-02-08

#ifndef LIMITEDARRAY__H
#define LIMITEDARRAY__H

#include <vector>

template<typename T>
class LimitedArray
{
  private :

  std::vector< std::pair<T,bool> > data;
  int nbElements;
  int lastElementDataIndex;
  int lastElementRealIndex;
  T mask;

  public :

  LimitedArray(unsigned int limit, const T & mask) : data(limit)
  {
    clear();
    this->mask = mask;
  }

  void clear()
  {
    nbElements = 0;
    lastElementDataIndex = -1; 
    lastElementRealIndex = -1;
  }

  void push(const T & elem)
  {
    nbElements++;
    if (nbElements > (int)data.size())
      nbElements = data.size();

    lastElementDataIndex++;
    if (lastElementDataIndex >= (int)data.size())
      lastElementDataIndex = 0;

    lastElementRealIndex++;

    data[lastElementDataIndex].first = elem;
    data[lastElementDataIndex].second = false;
  }

  const T & get(unsigned int index) const
  {
    if (data[index % data.size()].second)
      return mask;

    return data[index % data.size()].first;
  }

  void set(unsigned int index, const T & value)
  {
    data[index % data.size()].first = value;
    data[index % data.size()].second = false;
  }

  void maskIndex(unsigned int index)
  {
    data[index % data.size()].second = true;
  }

  void unmaskIndex(unsigned int index)
  {
    data[index % data.size()].second = false;
  }

  int getLastIndex() const
  {
    return lastElementRealIndex;
  }

  void copy(LimitedArray<T> & other, unsigned int from, unsigned int to)
  {
    std::copy(other.data.begin()+from, other.data.begin()+to, std::back_inserter(data));
  }

  void printForDebug(FILE * out) const
  {
    for (int i = 0; i < std::min(nbElements,(int)data.size()); i++)
      fprintf(out, "<%s>", data[i].first.c_str());
    fprintf(out, "\n");
  }

  void clearData()
  {
    data.clear();
  }

  int getNextOverridenDataIndex() const
  {
    if (lastElementRealIndex < (int)data.size()-1)
      return -1;

    int res = lastElementDataIndex+1;
    if (res >= (int)data.size())
      res = 0;

    return res;
  }

  int getNextOverridenRealIndex() const
  {
    if (lastElementRealIndex < (int)data.size()-1)
      return -1;

    return lastElementRealIndex - (data.size()-1);
  }

  int getDataSize() const
  {
    return std::min((int)data.size(), nbElements);
  }
};

#endif

