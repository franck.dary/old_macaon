/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// \file util.hpp
/// \brief Contains a variety of useful functions that doesn't operate on custom classes.
/// \author Franck Dary
/// @version 1.0
/// @date 2018-08-02

#ifndef UTIL__H
#define UTIL__H

#include <string>
#include <vector>

namespace util
{
/// @brief Whether or not this symbol can separate words (e.g. a space).
///
/// @param c The symbol to check.
///
/// @return Whether or not this symbol can separate words.
bool isSeparator(char c);
/// @brief Negation of isSeparator.
///
/// @param c The symbol to check.
///
/// @return !isSeparator(c)
bool isNotSeparator(char c);
/// @brief Whether or not this symbol is the start of a new line (e.g. \\n).
///
/// @param c The symbol to check.
///
/// @return Whether or not this symbol is the start of a new line.
bool isNewline(char c);
/// @brief Whether or not this symbol is a space encoded in utf8.
///
/// @param c iterator to the strat of the symbol to check. 
///
/// @return Whether or not this symbol is an utf8 space.
bool isUtf8Space(const std::string::iterator & c);
/// @brief Whether or not this symbol is a separator encoded in utf8.
///
/// @param c iterator to the strat of the symbol to check. 
///
/// @return Whether or not this symbol is an utf8 separator.
bool isUtf8Separator(const std::string::iterator & c);
/// @brief Get the filename of a file given it's relative or absolute path.
///
/// getFilenameFromPath(/home/toto/File.ex) = File.ex
/// @param s The path to the file.
///
/// @return The name of the file.
std::string getFilenameFromPath(const std::string & s);
/// @brief Get the number of columns a string will take when printed into a terminal.
///
/// Usefull for doing pretty prints where all is aligned.
/// @param s The string
///
/// @return The number of columns s will take when printed into a terminal.
unsigned int lengthPrinted(const std::string & s);
/// @brief Whether or not this symbol is a digit.
///
/// @param c The symbol to check.
///
/// @return Whether or not c is a digit.
bool isNum(char c);
/// @brief Whether or not this string is a digit.
///
/// @param c The string to check
///
/// @return Whether or not s is a digit.
bool isNum(const std::string & c);
/// @brief Whether or not a symbol can be the end of a well-formed sentence in Natural Language.
///
/// e.g. '?', '.', '!', ...
/// @param c The symbol to check.
///
/// @return Whether or c can be the end of a well-formed sentence in NL.
bool endSentence(char c);
/// @brief Whether or not this symbol is the end of a line.
///
/// @param c The symbol to check.
///
/// @return Whether or not c is the end of a line.
bool endLine(char c);
/// @brief Converts a symbol of a string to lower case.
///
/// @param s The string.
/// @param i The index of the symbol that will be set to lowercase.
void toLowerCase(std::string & s, unsigned int i);
/// @brief Get the lower case copy of a string.
///
/// @param s The input string.
///
/// @return The lowercase copy of s.
std::string toLowerCase(std::string s);
/// @brief Converts a symbol of a string to upper case.
///
/// @param s The string.
/// @param i The index of the symbol that will be set to uppercase.
void toUpperCase(std::string & s, unsigned int i);
/// @brief Get the uppercase copy of a string.
///
/// @param s The input string.
///
/// @return The uppercase copy of s.
std::string toUpperCase(const std::string & s);
/// @brief Get the lowercase copy of a string, removing the accents.
///
/// noAccentLower("Formaté") = "formate"
/// @param s The input string.
///
/// @return The lowercase copy of s, without accents.
std::string noAccentLower(const std::string & s);
/// @brief Whether or not this symbol is an uppercase letter.
///
/// @param c The symbol to check.
///
/// @return Whether or not c is an uppercase letter.
bool isUpper(char c);
/// @brief Split a string into its substrings divided by separators.
///
/// @param s The string to split.
///
/// @return The substrings.
std::vector<std::string> split(const std::string & s);
/// @brief Split a string into its substrings divided by specified separator.
///
/// @param s The string to split.
/// @param sep The separator to use.
///
/// @return The substrings.
std::vector<std::string> split(const std::string & s, char sep);
/// @brief Get the transformation rule that will transform the string from to the string to.
///
/// getRule("vendaient", "vendre") = "@aient@re"\n
/// Meaning delete the suffig "aient" and add the suffix "re".
/// @param from The string to be transformed.
/// @param to The end result of the rule application to from.
///
/// @return The transformation rule that will transform from to the string to.
std::string getRule(const std::string & from, const std::string & to);
/// @brief Whether or not a transformation rule is appliable to this specific string.
///
/// ruleIsAppliable("vendaient", "@e@a") = false\n
/// ruleIsAppliable("vendaient", "@aient@re") = true\n
/// @param from The string the rule should be applied to.
/// @param rule The transformation rule.
///
/// @return Whether or not it is possible to apply rule to from.
bool ruleIsAppliable(const std::string & from, const std::string & rule);
/// @brief Apply the transformation rule to a string.
///
/// applyRule("vendaient", "@aient@re") = "vendre"
/// @param from The string to which the rule will be applied.
/// @param rule The transformation rule to apply.
///
/// @return The resulting string.
std::string applyRule(const std::string & from, const std::string & rule);
/// @brief Print columns containing strings, with perfect alignment.
///
/// @param output Where to print.
/// @param cols Vector of columns.
/// @param nbSpaces The minimal number of spaces separating each columns.
void printColumns(FILE * output, const std::vector< std::vector<std::string> > & cols, int nbSpaces = 1);
/// @brief Converts a float to a string, using printf's style format specifier.
///
/// float2str(0.233333, "%.2f") = "0.23"
/// @param f The float.
/// @param format How to format the float.
///
/// @return The formated string.
std::string float2str(float f, const char * format);
/// @brief Return a version of s not suffixed by suffix.
///
/// @param s The base string.
/// @param suffix The suffix to remove.
///
/// @return s without suffix at the end.
std::string removeSuffix(const std::string & s, const std::string & suffix);

/// @brief Return a string with the current system time.
///
/// @return Current system time.
std::string getTime();

/// @brief Test if a file exists on the filesystem
///
/// @param s The path to the file
///
/// @return True if the file exists
bool fileExists(const std::string & s);

/// @brief Return true with a certain probability.
///
/// @param probability The probability (between 0.0 and 1.0) to return true.
///
/// @return True with a certain probability, false otherwise.
bool choiceWithProbability(float probability);

/// @brief Return a string representing the number
///
/// @param number The number
///
/// @return A string representing the number
std::string int2humanStr(int number);

/// @brief Return a random number between -range and +range
///
/// @param range The random value will be between -range and +range
///
/// @return Random number between -range and +range
float getRandomValueInRange(int range);

/// @brief Return the number of lines in a file.
///
/// @param filename The name of the file.
///
/// @return The number of lines in that file.
int getNbLines(const std::string & filename);

int getStartIndexOfNthSymbol(const std::string & s, int n);
int getStartIndexOfNthSymbolFrom(const std::string::iterator & s, const std::string::iterator & end, int n);
int getEndIndexOfNthSymbol(const std::string & s, int n);
int getEndIndexOfNthSymbolFrom(const std::string::iterator & s, const std::string::iterator & end, int n);
unsigned int getNbSymbols(const std::string & s);
std::string shrinkString(const std::string & base, int maxSize, const std::string token);
std::string strip(const std::string & s);
};

/// @brief Macro giving informations about an error.
#define ERRINFO (util::getFilenameFromPath(std::string(__FILE__))+ ":l." + std::to_string(__LINE__)).c_str()

#endif
