/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file ProgramOutput.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2019-02-12

#ifndef PROGRAMOUTPUT__H
#define PROGRAMOUTPUT__H

#include <vector>
#include <string>
#include <cstdio>

struct ProgramOutput
{
  private :

  std::vector< std::vector< std::pair<std::string, float> > > matrix;

  public :

  static ProgramOutput instance;

  public :

  void print(FILE * output);
  void addLine(FILE * output, const std::vector< std::pair<std::string, float> > & line, unsigned int index);
};

#endif

