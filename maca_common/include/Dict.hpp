/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// \file Dict.hpp
/// \author Franck Dary
/// @version 1.0
/// @date 2018-08-02

#ifndef DICT__H
#define DICT__H

#include <string>
#include <vector>
#include <map>
#include <set>
#include <memory>
#include <fasttext/fasttext.h>
#include <dynet/dynet.h>

/// @brief Maps strings to real vectors (embeddings or one-hot encoded).
///
/// Each entry of the Dict is a pair of string / Index.\n
/// The strings can be seen as features, while the index
/// points to the corresponding embedding value.
class Dict
{
  public :

  /// @brief How values are encoded.
  /// 
  /// OneHot means that each vector will consists of zeroes except for one index
  /// where the value will be 1.0.\n
  /// Embeddings means that each vector will consists of unbounded float values.
  enum Mode
  {
    OneHot,
    Embeddings
  };

  /// @brief How the Dict is updated.
  ///
  /// Final means that once it is read from a file, the Dict values will never
  /// be updated, and new entries cannot be added.\n
  /// Modifiable means that entries and their initial values will be read from
  /// a file, and they will be updated during the training step. Also new entries
  /// can be added at any times.\n
  /// FromZero means that no entries will be read from the file, the Dict will
  /// starts with 0 elements. New entries can be added at any times.
  enum Policy
  {
    Final,
    Modifiable,
    FromZero
  };

  /// @brief The name of this Dict.
  ///
  /// As it appears in the .dict file.
  std::string name;
  /// @brief The Mode of this Dict.
  Mode mode;
  /// @brief The Policy of this Dict.
  Policy policy;
  /// @brief Maximal number of entries.
  int dictCapacity;

  public :

  /// @brief When a feature value makes no sense (e.g. the fourth letter of 'He')
  ///
  /// it is mapped to this string.
  static std::string nullValueStr;
  /// @brief The name of the classifier corresponding to the current state in the TransitionMachine.
  ///
  /// So that this class can return the Dict corresponding to the correct Classifier.
  static std::string currentClassifierName;
  /// @brief When a feature value is unknown to this Dict, and this Dict is Final
  ///
  /// it is mapped to this string.
  static std::string unknownValueStr;

  /// @brief Converts a string to a Mode
  ///
  /// @param s The string we want to convert.
  ///
  /// @return The Mode corresponding to s. If s is unknown, the program aborts.
  static Mode str2mode(const std::string & s);
  /// @brief Converts a Mode to a string
  ///
  /// @param mode The Mode we want to convert.
  ///
  /// @return The string corresponding to mode.
  static const char * mode2str(Mode mode);
  /// @brief Converts a string to a Policy
  ///
  /// @param s The string we want to convert.
  ///
  /// @return The Policy corresponding to s. If s is unknown, the program aborts.
  static Policy str2policy(const std::string & s);

  private :

  /// @brief The dimension of each vector of this Dict (in number of float).
  int dimension;
  /// @brief A storage that map every string entry of this Dict to its index as a lookup parameter.
  std::map<std::string, unsigned int> str2index;
  dynet::LookupParameter lookupParameter;
  /// @brief The filename to which this Dict will be saved.
  std::string filename;
  /// @brief When in OneHot Mode, the Dict uses this variable to initialize new entries' vector to a unique OneHot value.
  int oneHotIndex;

  /// @brief The filename of the FastText's file containing pre-trained embeddings, can be empty if we're not using FastText.
  std::string ftFilename;
  /// @brief FastText object containing pre-trained embeddings, can be empty if we're not using FastText.
  std::unique_ptr<fasttext::FastText> ftEmbeddings;
  /// @brief A real vector.
  ///
  /// This is used by FastText to load its pre-trained word embeddings.
  std::unique_ptr<fasttext::Vector> ftVector;
  /// @brief This is where we store FastText's pre-trained word embeddings.
  ///
  /// We need this to be able to update embeddings values in the training step.
  std::map< std::string, std::vector<float> > ftVocab;

  /// @brief Where all the Dict objects must be stored.
  ///
  /// When getDict is called it will find the requested Dict here,
  /// or construct it.
  static std::map< std::string, std::unique_ptr<Dict> > str2dict;
  /// @brief Whether or not the init function has been called.
  bool isInit;
  /// @brief Wheter this Dict is a new one or a trained one.
  bool isTrained;

  private :

  /// @brief Return the vector value of an entry of this Dict.
  ///
  /// @param s An entry of this Dict.
  ///
  /// @return The FastText's pre-trained word embedding value corresponding to s.
  unsigned int getValueFasttext(const std::string & s);
  /// @brief Initialize a new embedding for this Dict.
  ///
  /// This function zero-initialize the embedding.
  /// @param s The entry whose embedding we will initialize.
  /// @param index The index of the entry.
  void initParameterAsEmbedding(const std::string & s, unsigned int index);
  /// @brief Initialize a parameter as a one hot value
  ///
  /// @param s The entry whose embedding we will initialize.
  /// @param index The index of the parameter to initialize.
  void initParameterAsOneHot(const std::string & s, unsigned int index);
  /// @brief Randomly initialize a new embedding for this Dict.
  ///
  /// Each float value of vec will be randomly set between -1.0 and +1.0
  /// @param index The index of the parameter to initialize.
  void initEmbeddingRandom(unsigned int index);
  /// @brief Zero-initialize a new embedding for this Dict.
  ///
  /// Each float value of vec will be set to 0.0
  /// @param index The index of the parameter to initialize.
  void initEmbeddingZero(unsigned int index);
  /// @brief Initialize a new embedding using FastText's value for this string.
  ///
  /// @param s The entry whose embedding we will initialize.
  /// @param index The index of the parameter.
  void initEmbeddingFromFasttext(const std::string & s, unsigned int index);
  /// @brief Add a new entry to this Dict.
  ///
  /// @param s The new entry to add.
  ///
  /// @return The lookupParameter index of the newly added entry.
  unsigned int addEntry(const std::string & s);
  void initFromFile(dynet::ParameterCollection & pc);
  /// @brief Read and construct a new Dict from a file.
  ///
  /// @param name The name the new Dict.
  /// @param policy The Policy of the new Dict.
  /// @param filename The filename we will read the new Dict from.
  /// @param dictCapacity Maximal number of entries.
  Dict(const std::string & name, Policy policy, const std::string & filename, int dictCapacity);
  /// @brief Get a pointer to the entry matching s.
  ///
  /// This is used when we need a permanent pointer to a string matching s,
  /// but s has a limited lifespan (probably because it was stack allocated).\n
  /// If no corresponding entry exists, a new one will be added.\n
  ///
  /// @param s The string value we want our returned pointed string value to match.
  ///
  /// @return A pointer to an entry matching the value of s.
  const std::string * getStrFasttext(const std::string & s);
  /// @brief Create file.
  void createFile();

  public :

  /// @brief Construct a new Dict.
  ///
  /// @param name The name of the Dict to construct.
  /// @param dimension The dimension of the vectors in the new Dict.
  /// @param mode The Mode of the new Dict.
  /// @param dictCapacity Maximal number of entries.
  Dict(const std::string & name, int dimension, Mode mode, int dictCapacity);

  void init(dynet::ParameterCollection & pc);

  void initParameterAsValue(unsigned int index, const std::vector<float> & value);

  unsigned int addEntry(const std::string & s, const std::vector<float> & embedding);

  /// @brief Get a pointer to a Dict.
  ///
  /// If the Dict doesn't exist, it will be constructed.
  /// @param policy The policy of the Dict.
  /// @param filename The filename of the Dict.
  ///
  /// @return A pointer to the corresponding Dict.
  static Dict * getDict(Policy policy, const std::string & filename);
  /// @brief Get a pointer to a Dict.
  ///
  /// If the Dict doesn't exist, the program will abort.
  /// @param name The name of the Dict.
  ///
  /// @return  A pointer to the corresponding Dict.
  static Dict * getDict(const std::string & name);
  /// @brief Read and construct multiple Dict from a file.
  ///
  /// @param directory The directory in which we will find the file.
  /// @param filename The name of the file containing the Dict descriptions.
  /// @param trainMode Wheter or not the program is in train mode (values will be updated)
  static void readDicts(const std::string & directory, const std::string & filename, bool trainMode);
  /// @brief Save every Dict whose name starts with the prefix to their corresponding files.
  ///
  /// @param directory The directory in which we will save every Dict.
  /// @param namePrefix The prefix of the name of the dicts we need to save.
  static void saveDicts(const std::string & directory, const std::string & namePrefix);
  /// @brief Create a file for every Dict matching the prefix.
  ///
  /// @param directory The directory in which the files will be saved.
  /// @param namePrefix The prefix that Dict must match.
  static void createFiles(const std::string & directory, const std::string & namePrefix);
  static void initDicts(dynet::ParameterCollection & pc, const std::string & namePrefix);
  /// @brief Delete all Dicts.
  static void deleteDicts();
  /// @brief Save the current Dict in the corresponding file.
  void save();
  /// @brief Get the index.
  ///
  /// @param s The entry which value we need.
  ///
  /// @return The index of the lookupParameter corresponding to s.
  unsigned int getValue(const std::string & s);
  /// @brief Get a pointer to the entry matching s.
  ///
  /// This is used when we need a permanent pointer to a string matching s,
  /// but s has a limited lifespan (probably because it was stack allocated).\n
  /// If no corresponding entry exists, a new one will be added.
  /// @param s The string value we want our returned pointed string value to match.
  ///
  /// @return A pointer to an entry matching the value of s.
  const std::string * getStr(const std::string & s);
  /// @brief Get the index of unknown values.
  ///
  /// @return Index of the entry unknownValueStr.
  unsigned int getUnknownValue();
  /// @brief Get the index of null values.
  ///
  /// @return Index of the entry nullValueStr.
  unsigned int getNullValue();
  /// @brief Get the dimmension of the vectors of this Dict.
  ///
  /// @return Dimmension of the vectors of this Dict.
  int getDimension();
  /// @brief Print this Dict for debug purpose.
  ///
  /// @param output The FILE to which the Dict will be printed.
  void printForDebug(FILE * output);
  dynet::LookupParameter & getLookupParameter();
};

#endif
