/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "Error.hpp"

Error::Error(std::string & prediction, std::string & gold, Classifier::WeightedActions & weightedActions, int cost, int linkLengthPrediction, int linkLengthGold) :
prediction(prediction), gold(gold), weightedActions(weightedActions), cost(cost), linkLengthPrediction(linkLengthPrediction), linkLengthGold(linkLengthGold)
{
  type = prediction + "->" + gold;
  if (ProgramParameters::onlyPrefixes)
    type = util::split(prediction,' ')[0] + "->" + util::split(gold,' ')[0];
  indexOfPrediction = -1;
  indexOfGold = -1;
  distanceWithGold = 0;
  entropy = Classifier::computeEntropy(weightedActions);

  for (unsigned int i = 0; i < weightedActions.size(); i++)
  {
    if (weightedActions[i].second.second == prediction)
      indexOfPrediction = i;
    if (weightedActions[i].second.second == gold)
      indexOfGold = i;
    if (weightedActions[i].first && indexOfPrediction != -1 && indexOfGold == -1)
      distanceWithGold++;
  }

  if (indexOfPrediction == -1 || indexOfGold == -1)
  {
    fprintf(stderr, "ERROR (%s) : weightedActions do not contain prediction or gold. Aborting.\n", ERRINFO);
    exit(1);
  }

  if (distanceWithGold != 0 && !isError())
  {
    fprintf(stderr, "ERROR (%s) : incoherent Error initialization. Aborting.\n", ERRINFO);
    exit(1);
  }
}

bool Error::goldWasAtDistance(int distance) const
{
  return distanceWithGold == distance;
}

const std::string & Error::getType() const
{
  return type;
}

float Error::getEntropy() const
{
  return entropy;
}

float Error::getCost() const
{
  return cost;
}

void ErrorSequence::add(const Error & error)
{
  sequence.emplace_back(error);
  types[error.getType()] = true;
}

const std::map<std::string, bool> & ErrorSequence::getTypes() const
{
  return types;
}

const std::vector<Error> & ErrorSequence::getSequence() const
{
  return sequence;
}

void Errors::newSequence()
{
  sequences.emplace_back();
}

void Errors::add(const Error & error)
{
  sequences.back().add(error);
}

bool Error::isError() const
{
  return prediction != gold;
}

void Errors::printStats()
{
  unsigned int minDistanceToCheck = 1;
  unsigned int maxDistanceToCheck = 5;
  int window = 20;
  int nbErrorsToKeep = ProgramParameters::nbErrorsToShow;
  std::map<std::string, int> nbErrorOccurencesByType;
  std::map<std::string, int> nbFirstErrorOccurencesByType;
  std::map<std::string, float> nbFirstErrorIntroduced;
  std::map<std::string, int> nbOccurencesByType;
  std::map<std::string, float> meanEntropyByType;
  std::map< std::string, std::vector<int> > distanceOfGoldByType;
  std::map< std::string, std::vector<float> > meanEntropyByDistanceByType;
  std::map< std::string, std::vector<int> > distanceOfGoldByFirstType;
  std::map< std::string, std::vector<float> > meanEntropyByDistanceByFirstType;
  std::map<int, int> hypothesisByLength;
  std::map<int, int> referenceByLength;
  std::map<int, float> hypothesisErrorByLength;
  std::map<int, float> referenceErrorByLength;
  int nbErrorsTotal = 0;
  int nbFirstErrorsTotal = 0;
  int nbActionsTotal = 0;

  auto printLine = []()
  {
    for (int i = 0; i < 80; i++)
      fprintf(stderr, "-");
    fprintf(stderr, "\n");
  };

  for (auto & sequence : sequences)
  {
    bool firstErrorMet = false;
    for (unsigned index = 0; index < sequence.getSequence().size(); index++)
    {
      auto & error = sequence.getSequence()[index];
      nbOccurencesByType[error.getType()]++;
      meanEntropyByType[error.getType()] += error.getEntropy();
      nbActionsTotal++;
      hypothesisByLength[error.getLinkLengthPrediction()]++;
      referenceByLength[error.getLinkLengthGold()]++;
      if (!error.isError())
      {
      }
      else
      {
        nbErrorOccurencesByType[error.getType()]++;
        hypothesisErrorByLength[error.getLinkLengthPrediction()]++;
        referenceErrorByLength[error.getLinkLengthGold()]++;
        if (!firstErrorMet)
        {
          nbFirstErrorOccurencesByType[error.getType()]++;
          nbFirstErrorsTotal++;
          nbFirstErrorIntroduced[error.getType()] += error.getCost();
          for (unsigned int i = index+1; i < sequence.getSequence().size(); i++)
            if (sequence.getSequence()[i].isError())
            {
              if ((int)(i - index) > window && window)
                break;
              nbFirstErrorIntroduced[error.getType()] += sequence.getSequence()[i].getCost();
            }
        }
        for (unsigned int i = minDistanceToCheck; i <= maxDistanceToCheck; i++)
        {
          while (distanceOfGoldByType[error.getType()].size() < (unsigned)(i+1))
            distanceOfGoldByType[error.getType()].emplace_back(0);
          while (meanEntropyByDistanceByType[error.getType()].size() < (unsigned)(i+1))
            meanEntropyByDistanceByType[error.getType()].emplace_back(0.0);
          distanceOfGoldByType[error.getType()][i] += error.goldWasAtDistance(i) ? 1 : 0;
          meanEntropyByDistanceByType[error.getType()][i] += error.goldWasAtDistance(i) ? error.getEntropy() : 0;

          if (!firstErrorMet)
          {
            while (distanceOfGoldByFirstType[error.getType()].size() < (unsigned)(i+1))
              distanceOfGoldByFirstType[error.getType()].emplace_back(0);
            while (meanEntropyByDistanceByFirstType[error.getType()].size() < (unsigned)(i+1))
              meanEntropyByDistanceByFirstType[error.getType()].emplace_back(0.0);
            distanceOfGoldByFirstType[error.getType()][i] += error.goldWasAtDistance(i) ? 1 : 0;
            meanEntropyByDistanceByFirstType[error.getType()][i] += error.goldWasAtDistance(i) ? error.getEntropy() : 0;
          }
        }
        nbErrorsTotal++;
        firstErrorMet = true;
      }
    }
  }

  for (auto & it : meanEntropyByDistanceByType)
    for (unsigned int i = 0; i < it.second.size(); i++)
      it.second[i] /= distanceOfGoldByType[it.first][i];

  for (auto & it : meanEntropyByDistanceByFirstType)
    for (unsigned int i = 0; i < it.second.size(); i++)
      it.second[i] /= distanceOfGoldByFirstType[it.first][i];

  for (auto & it : meanEntropyByType)
    it.second /= nbOccurencesByType[it.first];

  for (auto & it : nbFirstErrorOccurencesByType)
    nbFirstErrorIntroduced[it.first] /= it.second;

  std::vector< std::pair<std::string,int> > typesOccurences;
  for (auto & it : nbErrorOccurencesByType)
    typesOccurences.emplace_back(std::pair<std::string,int>(it.first,it.second));

  std::sort(typesOccurences.begin(), typesOccurences.end(),
  [](const std::pair<std::string,int> & a, const std::pair<std::string,int> & b)
  {
    return a.second > b.second;
  });

  typesOccurences.resize(std::min(nbErrorsToKeep, (int)typesOccurences.size()));

  fprintf(stderr, "%.2f%% of predicted actions where correct (%d / %d)\n",
      100.0*(nbActionsTotal-nbErrorsTotal)/nbActionsTotal, nbActionsTotal-nbErrorsTotal,nbActionsTotal);
  fprintf(stderr, "Format : Predicted->Gold\n");
 
  std::vector< std::vector<std::string> > columns;
  columns.clear();
  columns.resize(ProgramParameters::meanEntropy ? 5 : 4);
  for (auto & it : typesOccurences)
  {
    columns[0].emplace_back(it.first);
    columns[1].emplace_back("= " + util::float2str(it.second*100.0/nbErrorsTotal,"%.2f%%"));
    columns[2].emplace_back("of errors");
    columns[3].emplace_back("("+std::to_string(it.second) + " / " + std::to_string(nbErrorsTotal) + ")");
    if (ProgramParameters::meanEntropy)
      columns[4].emplace_back("mean entropy : " + util::float2str(meanEntropyByType[it.first], "%.2f"));

    for (unsigned int dist = minDistanceToCheck; dist <= maxDistanceToCheck; dist++)
    {
      columns[0].emplace_back("    Gold at distance");
      columns[1].emplace_back(std::to_string(dist));
      columns[2].emplace_back(util::float2str(distanceOfGoldByType[it.first][dist]*100.0/nbErrorOccurencesByType[it.first],"%.2f%%"));
      columns[3].emplace_back("of the time");
      if (ProgramParameters::meanEntropy)
        columns[4].emplace_back("mean entropy : " + util::float2str(meanEntropyByDistanceByType[it.first][dist], "%.2f"));
    }

    for (auto & col : columns)
      col.emplace_back("");
  }

  printLine();
  util::printColumns(stderr, columns, 1);
  printLine();

  std::vector< std::pair<std::string,int> > typesFirstOccurences;
  for (auto & it : nbFirstErrorOccurencesByType)
    typesFirstOccurences.emplace_back(std::pair<std::string,int>(it.first,it.second));

  std::sort(typesFirstOccurences.begin(), typesFirstOccurences.end(),
  [](const std::pair<std::string,int> & a, const std::pair<std::string,int> & b)
  {
    return a.second > b.second;
  });

  typesFirstOccurences.resize(std::min(nbErrorsToKeep, (int)typesFirstOccurences.size()));

  columns.clear();
  columns.resize(ProgramParameters::meanEntropy ? 6 : 5);
  for (auto & it : typesFirstOccurences)
  {
    columns[0].emplace_back(it.first);
    columns[1].emplace_back("= " + util::float2str(it.second*100.0/nbFirstErrorsTotal,"%.2f%%"));
    columns[2].emplace_back("of first errors");
    columns[3].emplace_back("("+std::to_string(it.second) + " / " + std::to_string(nbFirstErrorsTotal) + ")");
    columns[4].emplace_back("introduces " + util::float2str(nbFirstErrorIntroduced[it.first],"%.2f errors"));
    if (ProgramParameters::meanEntropy)
      columns[5].emplace_back("mean entropy : " + util::float2str(meanEntropyByType[it.first], "%.2f"));

    for (unsigned int dist = minDistanceToCheck; dist <= maxDistanceToCheck; dist++)
    {
      columns[0].emplace_back("    Gold at distance");
      columns[1].emplace_back(std::to_string(dist));
      columns[2].emplace_back(util::float2str(distanceOfGoldByFirstType[it.first][dist]*100.0/nbFirstErrorOccurencesByType[it.first],"%.2f%%"));
      columns[3].emplace_back("of the time");
      columns[4].emplace_back("");
      if (ProgramParameters::meanEntropy)
        columns[5].emplace_back("mean entropy : " + util::float2str(meanEntropyByDistanceByFirstType[it.first][dist], "%.2f"));
    }

    for (auto & col : columns)
      col.emplace_back("");
  }

  util::printColumns(stderr, columns, 1);
  printLine();
  columns.clear();
  columns.resize(4);

  for (int i = 0; i < 30; i++)
  {
    if (!hypothesisByLength.count(i))
      continue;

    int totalHypo = hypothesisByLength[i];
    int errorsHypo = hypothesisErrorByLength[i];
    int totalGold = referenceByLength[i];
    int errorsGold = referenceErrorByLength[i];
    float percHypo = 100.0*errorsHypo / totalHypo;
    float percGold = 100.0*errorsGold / totalGold;
    columns[0].emplace_back("Errors when link is of length " + std::to_string(i));
    columns[1].emplace_back("in the hypothesis");
    columns[2].emplace_back(": "+util::float2str(percHypo, "%.2f%%"));
    columns[3].emplace_back("("+std::to_string(errorsHypo)+"/"+std::to_string(totalHypo)+")");
    columns[0].emplace_back("Errors when link is of length " + std::to_string(i));
    columns[1].emplace_back("in the reference");
    columns[2].emplace_back(": "+util::float2str(percGold, "%.2f%%"));
    columns[3].emplace_back("("+std::to_string(errorsGold)+"/"+std::to_string(totalGold)+")");
  }

  util::printColumns(stderr, columns, 1);
}

int Error::getLinkLengthPrediction() const
{
  return linkLengthPrediction;
}

int Error::getLinkLengthGold() const
{
  return linkLengthGold;
}

