/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file Error.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-12-12

#ifndef ERROR__H
#define ERROR__H

#include "Classifier.hpp"
#include <vector>
#include <string>

class Error
{
  private :

  std::string prediction;
  std::string gold;
  Classifier::WeightedActions weightedActions;
  std::string type;
  int indexOfPrediction;
  int indexOfGold;
  int distanceWithGold;
  float entropy;
  int cost;
  int linkLengthPrediction;
  int linkLengthGold;
  
  public :

  Error(std::string &, std::string &, Classifier::WeightedActions &, int cost, int linkLengthPrediction, int linkLengthGold);
  bool isError() const;
  const std::string & getType() const;
  bool goldWasAtDistance(int distance) const;
  float getEntropy() const;
  float getCost() const;
  int getLinkLengthPrediction() const;
  int getLinkLengthGold() const;
};

class ErrorSequence
{
  private :

  std::vector<Error> sequence;
  std::map<std::string, bool> types;

  public :

  void add(const Error & error);
  const std::map<std::string, bool> & getTypes() const;
  const std::vector<Error> & getSequence() const;
};

class Errors
{
  private :

  std::vector<ErrorSequence> sequences;

  public :

  void newSequence();
  void add(const Error & error);
  void printStats();
};

#endif
