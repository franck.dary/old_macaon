# - Try to find dynet
# Once done this will define
#  DYNET_FOUND - System has dynet
#  DYNET_INCLUDE_DIRS - The dynet include directories
#  DYNET_LIBRARIES - The libraries needed to use dynet
#  DYNET_DEFINITIONS - Compiler switches required for using dynet

find_package(PkgConfig)
  pkg_check_modules(PC_DYNET QUIET dynet)

  find_path(DYNET_INCLUDE_DIR dynet/model.h
                HINTS ${PC_DYNET_INCLUDEDIR} ${PC_DYNET_INCLUDE_DIRS}
                          PATH_SUFFIXES dynet )

  find_library(DYNET_LIBRARY NAMES dynet
                   HINTS ${PC_DYNET_LIBDIR} ${PC_DYNET_LIBRARY_DIRS} )

  include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set DYNET_FOUND to TRUE
# if all listed variables are TRUE
  find_package_handle_standard_args(dynet  DEFAULT_MSG
                                        DYNET_LIBRARY DYNET_INCLUDE_DIR)

  mark_as_advanced(DYNET_INCLUDE_DIR DYNET_LIBRARY )

  set(DYNET_LIBRARIES ${DYNET_LIBRARY} )
  set(DYNET_INCLUDE_DIRS ${DYNET_INCLUDE_DIR}/dynet )
