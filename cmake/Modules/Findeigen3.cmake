# - Try to find eigen3
# Once done this will define
#  EIGEN3_FOUND - System has eigen3
#  EIGEN3_INCLUDE_DIRS - The eigen3 include directories
#  EIGEN3_LIBRARIES - The libraries needed to use eigen3
#  EIGEN3_DEFINITIONS - Compiler switches required for using eigen3

find_package(PkgConfig)
  pkg_check_modules(PC_EIGEN3 QUIET eigen3)

  find_path(EIGEN3_INCLUDE_DIR eigen3/model.h
                HINTS ${PC_EIGEN3_INCLUDEDIR} ${PC_DYNET_INCLUDE_DIRS}
                          PATH_SUFFIXES eigen3 )

set(EIGEN3_LIBRARIES ${DYNET_LIBRARY} )
set(EIGEN3_INCLUDE_DIRS ${DYNET_INCLUDE_DIR}/eigen3 )
