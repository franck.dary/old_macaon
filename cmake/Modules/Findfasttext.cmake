# - Try to find fasttext
# Once done this will define
#  FASTTEXT_FOUND - System has fasttext
#  FASTTEXT_INCLUDE_DIRS - The fasttext include directories
#  FASTTEXT_LIBRARIES - The libraries needed to use fasttext
#  FASTTEXT_DEFINITIONS - Compiler switches required for using fasttext

find_package(PkgConfig)
  pkg_check_modules(PC_FASTTEXT QUIET fasttext)

  find_path(FASTTEXT_INCLUDE_DIR fasttext/fasttext.h
                HINTS ${PC_FASTTEXT_INCLUDEDIR} ${PC_FASTTEXT_INCLUDE_DIRS}
                          PATH_SUFFIXES fasttext )

  find_library(FASTTEXT_LIBRARY NAMES fasttext
                   HINTS ${PC_FASTTEXT_LIBDIR} ${PC_FASTTEXT_LIBRARY_DIRS} )

  include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set FASTTEXT_FOUND to TRUE
# if all listed variables are TRUE
  find_package_handle_standard_args(fasttext  DEFAULT_MSG
                                        FASTTEXT_LIBRARY FASTTEXT_INCLUDE_DIR)

  mark_as_advanced(FASTTEXT_INCLUDE_DIR FASTTEXT_LIBRARY )

  set(FASTTEXT_LIBRARIES ${FASTTEXT_LIBRARY} )
  set(FASTTEXT_INCLUDE_DIRS ${FASTTEXT_INCLUDE_DIR}/fasttext )
