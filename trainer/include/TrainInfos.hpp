/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file TrainInfos.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-12-20

#ifndef TRAININFOS__H
#define TRAININFOS__H

#include <string>
#include <vector>
#include "ProgramParameters.hpp"
#include "Config.hpp"

class TrainInfos
{
  private :

  std::string filename;
  int lastEpoch;
  int lastSaved;
  std::map< std::string, std::vector<float> > trainLossesPerClassifierPerEpoch;
  std::map< std::string, std::vector<float> > devLossesPerClassifierPerEpoch;
  std::map< std::string, std::vector<float> > trainScoresPerClassifierPerEpoch;
  std::map< std::string, std::vector<float> > devScoresPerClassifierPerEpoch;
  std::map< std::string, std::vector<bool> > mustSavePerClassifierPerEpoch;

  std::map<std::string, float> trainLossCounter;
  std::map<std::string, float> devLossCounter;

  std::map<std::string, bool> topologyPrinted;

  private :

  void readFromFilename();
  void saveToFilename();
  void addTrainScore(const std::string & classifier, float score);
  void addDevScore(const std::string & classifier, float score);
  float computeScoreOnTapes(Config & c, std::vector<std::string> tapes, int from, int to);

  public :

  std::map<std::string, bool> lastActionWasPredicted;

  public :

  TrainInfos();
  void addTrainLoss(const std::string & classifier, float loss);
  void addDevLoss(const std::string & classifier, float loss);
  void computeTrainScores(Config & c);
  void computeDevScores(Config & c);
  void computeMustSaves();
  int getEpoch();
  bool isTopologyPrinted(const std::string & classifier);
  void setTopologyPrinted(const std::string & classifier);
  void nextEpoch();
  bool mustSave(const std::string & classifier);
  void printScores(FILE * output);
  void setLastIndexTreated(int index);
};

#endif
