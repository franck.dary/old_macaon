/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file macaon_train.cpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-07

#include <cstdio>
#include <cstdlib>
#include "BD.hpp"
#include "Config.hpp"
#include "TransitionMachine.hpp"
#include "Trainer.hpp"
#include "ProgramParameters.hpp"
#include "programOptionsTemplates.hpp"

/// @brief Train a TransitionMachine to predict and add information to a structured input file, by using annotated examples.
///
/// @param argc The number of arguments given to this program.
/// @param argv[] Array of arguments given to this program.
///
/// @return 0 if there was no crash.
int main(int argc, char * argv[])
{
  loadTrainProgramParameters(argc, argv);

  if (ProgramParameters::nbTrain)
  {
    updatePaths();
    createTemplatePath();
    for (int i = 0; i < ProgramParameters::nbTrain; i++)
    {
      fprintf(stderr, "Training number %d / %d :\n", i+1, ProgramParameters::nbTrain);
      ProgramParameters::expName = ProgramParameters::baseExpName + "_" + std::to_string(i);
      updatePaths();
      createExpPath();
      Dict::deleteDicts();
      launchTraining();
    }
    removeTemplatePath();
  }
  else
  {
    updatePaths();
    ProgramParameters::newTemplatePath = ProgramParameters::templatePath;
    createExpPath();
    launchTraining();
  }

  return 0;
}

