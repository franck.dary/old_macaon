/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "TrainInfos.hpp"
#include "File.hpp"
#include "util.hpp"

TrainInfos::TrainInfos()
{
  filename = ProgramParameters::expPath + "trainInfos.txt";
  lastEpoch = 0;
  lastSaved = 0;

  if (util::fileExists(filename))
    readFromFilename();
}

void TrainInfos::readFromFilename()
{
  File file(filename, "r");
  FILE * filePtr = file.getDescriptor();

  char buffer[10000];

  auto badFormatAndExit = [](const std::string & errinfo)
  {
    fprintf(stderr, "ERROR (%s) : bad format. Aborting.\n", errinfo.c_str());
    exit(1);
  };

  if (fscanf(filePtr, "%d\n", &lastEpoch) != 1)
    badFormatAndExit(ERRINFO);
  lastSaved = lastEpoch;
  while (fscanf(filePtr, "%[^\n]\n", buffer) == 1)
  {
    auto splitted = util::split(buffer, '\t');
    if (splitted.empty() || splitted[0] == "---")
      break;

    for (unsigned int i = 1; i < splitted.size(); i++)
      trainLossesPerClassifierPerEpoch[splitted[0]].emplace_back(std::stof(splitted[i]));
  }
  while (fscanf(filePtr, "%[^\n]\n", buffer) == 1)
  {
    auto splitted = util::split(buffer, '\t');
    if (splitted.empty() || splitted[0] == "---")
      break;

    for (unsigned int i = 1; i < splitted.size(); i++)
      devLossesPerClassifierPerEpoch[splitted[0]].emplace_back(std::stof(splitted[i]));
  }
  while (fscanf(filePtr, "%[^\n]\n", buffer) == 1)
  {
    auto splitted = util::split(buffer, '\t');
    if (splitted.empty() || splitted[0] == "---")
      break;

    for (unsigned int i = 1; i < splitted.size(); i++)
      trainScoresPerClassifierPerEpoch[splitted[0]].emplace_back(std::stof(splitted[i]));
  }
  while (fscanf(filePtr, "%[^\n]\n", buffer) == 1)
  {
    auto splitted = util::split(buffer, '\t');
    if (splitted.empty() || splitted[0] == "---")
      break;

    for (unsigned int i = 1; i < splitted.size(); i++)
      devScoresPerClassifierPerEpoch[splitted[0]].emplace_back(std::stof(splitted[i]));
  }
  while (fscanf(filePtr, "%[^\n]\n", buffer) == 1)
  {
    auto splitted = util::split(buffer, '\t');
    if (splitted.empty() || splitted[0] == "---")
      break;

    for (unsigned int i = 1; i < splitted.size(); i++)
      mustSavePerClassifierPerEpoch[splitted[0]].push_back(splitted[i] == "true" ? true : false);
  }
}

void TrainInfos::saveToFilename()
{
  File file(filename, "w");
  FILE * filePtr = file.getDescriptor();

  fprintf(filePtr, "%d\n", lastSaved);
  for (auto & it : trainLossesPerClassifierPerEpoch)
  {
    fprintf(filePtr, "%s\t", it.first.c_str());
    for (auto & val : it.second)
      fprintf(filePtr, "%f\t", val);
    fprintf(filePtr, "\n");
  }
  fprintf(filePtr, "---\n");
  for (auto & it : devLossesPerClassifierPerEpoch)
  {
    fprintf(filePtr, "%s\t", it.first.c_str());
    for (auto & val : it.second)
      fprintf(filePtr, "%f\t", val);
    fprintf(filePtr, "\n");
  }
  fprintf(filePtr, "---\n");
  for (auto & it : trainScoresPerClassifierPerEpoch)
  {
    fprintf(filePtr, "%s\t", it.first.c_str());
    for (auto & val : it.second)
      fprintf(filePtr, "%f\t", val);
    fprintf(filePtr, "\n");
  }
  fprintf(filePtr, "---\n");
  for (auto & it : devScoresPerClassifierPerEpoch)
  {
    fprintf(filePtr, "%s\t", it.first.c_str());
    for (auto & val : it.second)
      fprintf(filePtr, "%f\t", val);
    fprintf(filePtr, "\n");
  }
  fprintf(filePtr, "---\n");
  for (auto & it : mustSavePerClassifierPerEpoch)
  {
    fprintf(filePtr, "%s\t", it.first.c_str());
    for (unsigned int i = 0; i < it.second.size(); i++)
      fprintf(filePtr, "%s\t", it.second[i] ? "true" : "false");
    fprintf(filePtr, "\n");
  }
  fprintf(filePtr, "---\n");
}

void TrainInfos::addTrainLoss(const std::string & classifier, float loss)
{
  trainLossesPerClassifierPerEpoch[classifier].back() += loss;
}

void TrainInfos::addDevLoss(const std::string & classifier, float loss)
{
  devLossesPerClassifierPerEpoch[classifier].back() += loss;
}

void TrainInfos::addTrainScore(const std::string & classifier, float score)
{
  trainScoresPerClassifierPerEpoch[classifier].emplace_back(score);
}

void TrainInfos::addDevScore(const std::string & classifier, float score)
{
  devScoresPerClassifierPerEpoch[classifier].emplace_back(score);
}

float TrainInfos::computeScoreOnTapes(Config & c, std::vector<std::string> tapes, int from, int to)
{
  float res = 0.0;

  for (auto & tape : tapes)
    res += c.getTape(tape).getScore(from, to);

  return res / tapes.size();
}

void TrainInfos::computeTrainScores(Config & c)
{
  std::string name;
  {
    File tmpOutTrain("bin/"+ProgramParameters::expName+"/tmpOutTrain.txt", "w");
    name = tmpOutTrain.getName();
    c.setOutputFile(tmpOutTrain.getDescriptor());
    c.printTheRest(false);
    c.setOutputFile(nullptr);
    c.setLastIndexPrinted(-1);
  }

  std::string name2;
  {
    File tmpOutTrain("bin/"+ProgramParameters::expName+"/tmpOutTrainRef.txt", "w");
    name2 = tmpOutTrain.getName();
    c.setOutputFile(tmpOutTrain.getDescriptor());
    c.printTheRest(true);
    c.setOutputFile(nullptr);
    c.setLastIndexPrinted(-1);
  }

  {
    FILE * trainInGoodConllFormat = popen(("../tools/conlluAddMissingColumns.py " + name + " data/conllu.mcd > bin/" + ProgramParameters::expName + "/tmpOutTrain.conllu").c_str(), "w");
    pclose(trainInGoodConllFormat);
  }
  {
    FILE * trainInGoodConllFormat = popen(("../tools/conlluAddMissingColumns.py " + name2 + " data/conllu.mcd > bin/" + ProgramParameters::expName + "/tmpOutTrainRef.conllu").c_str(), "w");
    pclose(trainInGoodConllFormat);
  }

  std::map<std::string, std::string> scoresStr;
  std::map<std::string, float> scoresFloat;
  {
    FILE * evalFromUD = popen(("../scripts/conll18_ud_eval.py " + std::string(" bin/") + ProgramParameters::expName + "/tmpOutTrainRef.conllu " + std::string(" bin/") + ProgramParameters::expName + "/tmpOutTrain.conllu -v").c_str(), "r");
    char buffer[10000];
    while (fscanf(evalFromUD, "%[^\n]\n", buffer) == 1)
    {
      auto splited = util::split(buffer, '|');
      if (splited.size() > 2)
        scoresStr[util::strip(splited[0])] = util::strip(splited[3]);
    }
    pclose(evalFromUD);
  }

  for (auto & it : scoresStr)
    try {scoresFloat[it.first] = std::stof(it.second);} catch(std::exception &){}

  for (auto & it : topologyPrinted)
  {
    if (it.first == "Parser")
      addTrainScore(it.first, (scoresFloat["UAS"] + scoresFloat["LAS"]) / 2);
    else if (it.first == "Segmenter")
      addTrainScore(it.first, scoresFloat["Sentences"]);
    else if (it.first == "Tokenizer")
      addTrainScore(it.first, scoresFloat["Tokens"]);
    else if (it.first == "Tagger")
      addTrainScore(it.first, scoresFloat["UPOS"]);
    else if (it.first == "Morpho")
      addTrainScore(it.first, scoresFloat["UFeats"]);
    else if (it.first == "Lemmatizer_Rules" || it.first == "Lemmatizer_Case")
      addTrainScore(it.first, scoresFloat["Lemmas"]);
    else if (util::split(it.first, '_')[0] == "Error")
      addTrainScore(it.first, 100.0);
    else
    {
      fprintf(stderr, "ERROR (%s) : No instructions to compute score of tool \'%s\'. Aborting.\n", ERRINFO, it.first.c_str());
      exit(1);
    }
  }
}

void TrainInfos::computeDevScores(Config & c)
{
  std::string name;
  {
    File tmpOutDev("bin/"+ProgramParameters::expName+"/tmpOutDev.txt", "w");
    name = tmpOutDev.getName();
    c.setOutputFile(tmpOutDev.getDescriptor());
    c.printTheRest(false);
    c.setOutputFile(nullptr);
    c.setLastIndexPrinted(-1);
  }

  {
    FILE * devInGoodConllFormat = popen(("../tools/conlluAddMissingColumns.py " + name + " data/conllu.mcd > bin/" + ProgramParameters::expName + "/tmpOutDev.conllu").c_str(), "w");
    pclose(devInGoodConllFormat);
  }

  std::map<std::string, std::string> scoresStr;
  std::map<std::string, float> scoresFloat;
  {
    FILE * evalFromUD = popen(("../scripts/conll18_ud_eval.py " + ProgramParameters::devFilename + " bin/" + ProgramParameters::expName + "/tmpOutDev.conllu -v").c_str(), "r");
    char buffer[10000];
    while (fscanf(evalFromUD, "%[^\n]\n", buffer) == 1)
    {
      auto splited = util::split(buffer, '|');
      if (splited.size() > 2)
        scoresStr[util::strip(splited[0])] = util::strip(splited[3]);
    }
    pclose(evalFromUD);
  }

  for (auto & it : scoresStr)
    try {scoresFloat[it.first] = std::stof(it.second);} catch(std::exception &){}

  for (auto & it : topologyPrinted)
  {
    if (it.first == "Parser")
      addDevScore(it.first, (scoresFloat["UAS"] + scoresFloat["LAS"]) / 2);
    else if (it.first == "Segmenter")
      addDevScore(it.first, scoresFloat["Sentences"]);
    else if (it.first == "Tokenizer")
      addDevScore(it.first, scoresFloat["Tokens"]);
    else if (it.first == "Tagger")
      addDevScore(it.first, scoresFloat["UPOS"]);
    else if (it.first == "Morpho")
      addDevScore(it.first, scoresFloat["UFeats"]);
    else if (it.first == "Lemmatizer_Rules" || it.first == "Lemmatizer_Case")
      addDevScore(it.first, scoresFloat["Lemmas"]);
    else if (util::split(it.first, '_')[0] == "Error")
      addDevScore(it.first, 100.0);
    else
    {
      fprintf(stderr, "ERROR (%s) : No instructions to compute score of tool \'%s\'. Aborting.\n", ERRINFO, it.first.c_str());
      exit(1);
    }
  }
}

int TrainInfos::getEpoch()
{
  return lastEpoch + 1;
}

bool TrainInfos::isTopologyPrinted(const std::string & classifier)
{
  return topologyPrinted.count(classifier) && topologyPrinted[classifier];
}

void TrainInfos::setTopologyPrinted(const std::string & classifier)
{
  topologyPrinted[classifier] = true;
  trainLossesPerClassifierPerEpoch[classifier].emplace_back(0.0);
}

void TrainInfos::nextEpoch()
{
  lastEpoch++;
  saveToFilename();
  for (auto & it : topologyPrinted)
    trainLossesPerClassifierPerEpoch[it.first].emplace_back(0.0);
}

void TrainInfos::computeMustSaves()
{
  if (!devScoresPerClassifierPerEpoch.empty())
    for (auto & it : devScoresPerClassifierPerEpoch)
    {
      mustSavePerClassifierPerEpoch[it.first].push_back(true);
      if (!ProgramParameters::alwaysSave)
        for (auto & score : it.second)
          if (score > it.second.back())
            mustSavePerClassifierPerEpoch[it.first].back() = false;

      if (mustSavePerClassifierPerEpoch[it.first].back())
        lastSaved = getEpoch();
    }
  else
    for (auto & it : trainScoresPerClassifierPerEpoch)
    {
      mustSavePerClassifierPerEpoch[it.first].push_back(true);
      if (!ProgramParameters::alwaysSave)
        for (auto & score : it.second)
          if (score > it.second.back())
            mustSavePerClassifierPerEpoch[it.first].back() = false;

      if (mustSavePerClassifierPerEpoch[it.first].back())
        lastSaved = getEpoch();
    }
}

void TrainInfos::printScores(FILE * output)
{
  std::vector<std::string> names;
  std::vector<std::string> acc;
  std::vector<std::string> train;
  std::vector<std::string> lossTrain;
  std::vector<std::string> dev;
  std::vector<std::string> lossDev;
  std::vector<std::string> savedStr;

  for (auto & it : trainScoresPerClassifierPerEpoch)
  {
    names.emplace_back(it.first);
    acc.emplace_back("accuracy");
    train.emplace_back(": train(" + util::float2str(it.second.back(), "%.2f") + "%)");
    lossTrain.emplace_back(trainLossesPerClassifierPerEpoch.empty() ? "loss(?)" : "loss(" +util::float2str(trainLossesPerClassifierPerEpoch[it.first].back(), "%.2f") + ")");
    dev.emplace_back(devScoresPerClassifierPerEpoch.empty() ? "" : "dev(" +util::float2str(devScoresPerClassifierPerEpoch[it.first].back(), "%.2f") + "%)");
    lossDev.emplace_back(devLossesPerClassifierPerEpoch.empty() ? "loss(?)" : "loss(" +util::float2str(devLossesPerClassifierPerEpoch[it.first].back(), "%.2f") + ")");
    savedStr.emplace_back(mustSavePerClassifierPerEpoch[it.first].back() ? "SAVED" : "");
  }

  if (ProgramParameters::interactive)
    fprintf(output, "                            \r");
  if (ProgramParameters::printTime)
    fprintf(output, "[%s] ", util::getTime().c_str());
  fprintf(output, "Iteration %d/%d :                                      \n", getEpoch(), ProgramParameters::nbIter);

  util::printColumns(output, {names, acc, train, lossTrain, dev, lossDev, savedStr});
}

bool TrainInfos::mustSave(const std::string & classifier)
{
  return mustSavePerClassifierPerEpoch.count(classifier) && mustSavePerClassifierPerEpoch[classifier].back();
}

