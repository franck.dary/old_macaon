/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "MLP.hpp"

MLP::MLP() : mlp("MLP")
{
  randomSeed = ProgramParameters::seed;
  trainer.reset(createTrainer());
  initDynet();
}

MLP::MLP(const std::string & filename) : mlp("MLP")
{
  randomSeed = ProgramParameters::seed;
  trainer.reset(createTrainer());
  initDynet();

  mlp.loadStruct(model, filename, 0);
  mlp.loadParameters(model, filename);
}

void MLP::init(int nbInputs, const std::string & topology, int nbOutputs)
{
  setBatchSize(0);
  mlp.init(model, nbInputs, topology, nbOutputs);
}

dynet::Trainer * MLP::createTrainer()
{
  auto optimizer = util::noAccentLower(ProgramParameters::optimizer);

  dynet::Trainer * trainer = nullptr;

  if (optimizer == "amsgrad")
    trainer = new dynet::AmsgradTrainer(model, ProgramParameters::learningRate, ProgramParameters::beta1, ProgramParameters::beta2, ProgramParameters::bias);
  else if (optimizer == "adam")
    trainer =  new dynet::AdamTrainer(model, ProgramParameters::learningRate, ProgramParameters::beta1, ProgramParameters::beta2, ProgramParameters::bias);
  else if (optimizer == "sgd")
    trainer =  new dynet::SimpleSGDTrainer(model, ProgramParameters::learningRate);
  else if (optimizer == "none")
    return nullptr;

  if (trainer)
  {
    trainer->sparse_updates_enabled = true;
    return trainer;
  }

  fprintf(stderr, "ERROR (%s) : unknown optimizer \'%s\'. Aborting.\n", ERRINFO, optimizer.c_str());

  exit(1);

  return nullptr;
}

std::vector<float> MLP::predict(FeatureModel::FeatureDescription & fd)
{
  return mlp.predict(fd);
}

float MLP::update(FeatureModel::FeatureDescription & fd, int gold)
{
  mlp.setBatchSize(getBatchSize());
  try
  {
    float loss = mlp.update(fd, gold);
    trainer->update();
    return loss;
  } catch (BatchNotFull &)
  {
    return 0.0;
  }
}

float MLP::update(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold)
{
  mlp.setBatchSize(getBatchSize());
  try
  {
    float loss = mlp.update(fd, gold);
    trainer->update();
    return loss;
  } catch (BatchNotFull &)
  {
    return 0.0;
  }
}

float MLP::getLoss(FeatureModel::FeatureDescription & fd, int gold)
{
  mlp.setBatchSize(getBatchSize());
  try
  {
    float loss = mlp.getLoss(fd, gold);
    return loss;
  } catch (BatchNotFull &)
  {
    return 0.0;
  }
}

float MLP::getLoss(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold)
{
  mlp.setBatchSize(getBatchSize());
  try
  {
    float loss = mlp.getLoss(fd, gold);
    return loss;
  } catch (BatchNotFull &)
  {
    return 0.0;
  }
}

void MLP::save(const std::string & filename)
{
  File * file = new File(filename, "w");
  delete file;
  mlp.saveStruct(filename);
  mlp.saveParameters(filename);
}

void MLP::printTopology(FILE * output)
{
  mlp.printTopology(output);
}

void MLP::endOfIteration()
{
  mlp.endOfIteration();
}

