/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "ReversedMLP.hpp"

ReversedMLP::ReversedMLP() : mlpPos("MLP_POS"), mlpNeg("MLP_NEG")
{
  randomSeed = ProgramParameters::seed;
  trainer.reset(createTrainer());
  initDynet();
}

ReversedMLP::ReversedMLP(const std::string & filename) : mlpPos("MLP_POS"), mlpNeg("MLP_NEG")
{
  randomSeed = ProgramParameters::seed;
  trainer.reset(createTrainer());
  initDynet();

  mlpPos.loadStruct(model, filename, 0);
  mlpPos.loadParameters(model, filename);

  mlpNeg.loadStruct(model, filename, 1);
  mlpNeg.loadParameters(model, filename);
}

void ReversedMLP::init(int nbInputs, const std::string & topology, int nbOutputs)
{
  std::string safeTopology = "";
  for (unsigned int i = 1; i < topology.size(); i++)
    safeTopology.push_back(topology[i]);

  setBatchSize(0);
  mlpPos.init(model, nbInputs, safeTopology, nbOutputs);
  mlpNeg.init(model, nbInputs, safeTopology, nbOutputs);
}

dynet::Trainer * ReversedMLP::createTrainer()
{
  auto optimizer = util::noAccentLower(ProgramParameters::optimizer);

  dynet::Trainer * trainer = nullptr;

  if (optimizer == "amsgrad")
    trainer = new dynet::AmsgradTrainer(model, ProgramParameters::learningRate, ProgramParameters::beta1, ProgramParameters::beta2, ProgramParameters::bias);
  else if (optimizer == "adam")
    trainer =  new dynet::AdamTrainer(model, ProgramParameters::learningRate, ProgramParameters::beta1, ProgramParameters::beta2, ProgramParameters::bias);
  else if (optimizer == "sgd")
    trainer =  new dynet::SimpleSGDTrainer(model, ProgramParameters::learningRate);
  else if (optimizer == "none")
    return nullptr;

  if (trainer)
  {
    trainer->sparse_updates_enabled = true;
    return trainer;
  }

  fprintf(stderr, "ERROR (%s) : unknown optimizer \'%s\'. Aborting.\n", ERRINFO, optimizer.c_str());

  exit(1);

  return nullptr;
}

std::vector<float> ReversedMLP::predict(FeatureModel::FeatureDescription & fd)
{
  auto predPos = mlpPos.predict(fd);
  auto predNeg = mlpNeg.predict(fd);

  for (unsigned int i = 0; i < predPos.size(); i++)
    predPos[i] -= predNeg[i];

  return predPos;
}

float ReversedMLP::update(FeatureModel::FeatureDescription & fd, int gold)
{
  mlpPos.setBatchSize(getBatchSize());
  mlpNeg.setBatchSize(getBatchSize());
  try
  {
    float loss = 0.0;
    if (gold >= 0)
    {
      loss = mlpPos.update(fd, gold);
    }
    else
    {
      gold = -gold;
      gold--;
      loss = mlpPos.update(fd, gold);
    }

    trainer->update();
    return loss;
  } catch (BatchNotFull &)
  {
    return 0.0;
  }
}

float ReversedMLP::update(FeatureModel::FeatureDescription &, const std::vector<float> &)
{
  fprintf(stderr, "ERROR (%s) : only classification is supported. Aborting.\n", ERRINFO);
  exit(1);
  return 0.0;
}

float ReversedMLP::getLoss(FeatureModel::FeatureDescription & fd, int gold)
{
  mlpPos.setBatchSize(getBatchSize());
  mlpNeg.setBatchSize(getBatchSize());
  try
  {
    float loss = 0.0;
    if (gold >= 0)
    {
      loss = mlpPos.getLoss(fd, gold);
    }
    else
    {
      gold = -gold;
      gold--;
      loss = mlpPos.getLoss(fd, gold);
    }
    return loss;
  } catch (BatchNotFull &)
  {
    return 0.0;
  }
}

float ReversedMLP::getLoss(FeatureModel::FeatureDescription &, const std::vector<float> &)
{
  fprintf(stderr, "ERROR (%s) : only classification is supported. Aborting.\n", ERRINFO);
  exit(1);
  return 0.0;
}

void ReversedMLP::save(const std::string & filename)
{
  File * file = new File(filename, "w");
  delete file;
  mlpPos.saveStruct(filename);
  mlpPos.saveParameters(filename);

  mlpNeg.saveStruct(filename);
  mlpNeg.saveParameters(filename);
}

void ReversedMLP::printTopology(FILE * output)
{
  mlpPos.printTopology(output);
}

void ReversedMLP::endOfIteration()
{
  mlpPos.endOfIteration();
  mlpNeg.endOfIteration();
}

