/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "MultiMLP.hpp"
#include <cmath>

MultiMLP::MultiMLP()
{
  randomSeed = ProgramParameters::seed;
  trainer.reset(createTrainer());
  initDynet();
}

MultiMLP::MultiMLP(const std::string & filename)
{
  randomSeed = ProgramParameters::seed;
  trainer.reset(createTrainer());
  initDynet();

  File * file = new File(filename, "r");
  int nbMlps;
  if (fscanf(file->getDescriptor(), "#NBMLP:%d# # {1,1} 0 _\n", &nbMlps) != 1)
  {
    fprintf(stderr, "ERROR (%s) : Ill formated model, aborting.\n", ERRINFO);
    exit(1);
  }
  delete file;

  for (int i = 0; i < nbMlps; i++)
  {
    mlps.emplace_back(std::string("MLP_")+std::to_string(i));
    mlps.back().loadStruct(model, filename, i);
    mlps.back().loadParameters(model, filename);   
  }
}

void MultiMLP::init(int nbInputs, const std::string & topology, int nbOutputs)
{
  std::string safeTopology = "";
  for (unsigned int i = 1; i < topology.size(); i++)
    safeTopology.push_back(topology[i]);

  setBatchSize(0);
  if (mlps.empty())
  {
    for (int i = 0; i < nbOutputs; i++)
      mlps.emplace_back(std::string("MLP_")+std::to_string(i));
  }

  for (auto & mlp : mlps)
    mlp.init(model, nbInputs, safeTopology, 2);
}

dynet::Trainer * MultiMLP::createTrainer()
{
  auto optimizer = util::noAccentLower(ProgramParameters::optimizer);

  dynet::Trainer * trainer = nullptr;

  if (optimizer == "amsgrad")
    trainer = new dynet::AmsgradTrainer(model, ProgramParameters::learningRate, ProgramParameters::beta1, ProgramParameters::beta2, ProgramParameters::bias);
  else if (optimizer == "adam")
    trainer =  new dynet::AdamTrainer(model, ProgramParameters::learningRate, ProgramParameters::beta1, ProgramParameters::beta2, ProgramParameters::bias);
  else if (optimizer == "sgd")
    trainer =  new dynet::SimpleSGDTrainer(model, ProgramParameters::learningRate);
  else if (optimizer == "none")
    return nullptr;

  if (trainer)
  {
    trainer->sparse_updates_enabled = true;
    return trainer;
  }

  fprintf(stderr, "ERROR (%s) : unknown optimizer \'%s\'. Aborting.\n", ERRINFO, optimizer.c_str());

  exit(1);

  return nullptr;
}

std::vector<float> MultiMLP::predict(FeatureModel::FeatureDescription & fd)
{
  double totalSum = 0.0;
  std::vector<float> prediction(mlps.size());
  for (unsigned int i = 0; i < mlps.size(); i++)
  {
    int id = std::stoi(util::split(mlps[i].name, '_')[1]);
    auto value = mlps[i].predict(fd);
    prediction[id] = exp(value[1]);
    totalSum += prediction[id];
  }

  for (unsigned int i = 0; i < prediction.size(); i++)
    prediction[i] /= totalSum;

  return prediction;
}

float MultiMLP::update(FeatureModel::FeatureDescription & fd, int gold)
{
  float loss = 0.0;
  for (auto & mlp : mlps)
    try
    {
      int id = std::stoi(util::split(mlp.name, '_')[1]);
      mlp.setBatchSize(getBatchSize());
      if (gold >= 0)
        loss += mlp.update(fd, id == gold ? 1 : 0);
      else
        loss += mlp.update(fd, id == (-1-gold) ? 0 : 1);

      trainer->update();
    } catch (BatchNotFull &)
    {
    }

  return loss;
}

float MultiMLP::update(FeatureModel::FeatureDescription &, const std::vector<float> &)
{
  fprintf(stderr, "ERROR (%s) : only classification is supported. Aborting.\n", ERRINFO);
  exit(1);
  return 0.0;
}

float MultiMLP::getLoss(FeatureModel::FeatureDescription & fd, int gold)
{
  float loss = 0.0;
  for (auto & mlp : mlps)
    try
    {
      int id = std::stoi(util::split(mlp.name, '_')[1]);
      mlp.setBatchSize(getBatchSize());
      if (gold >= 0)
        loss += mlp.update(fd, id == gold ? 1 : 0);
      else
        loss += mlp.update(fd, id == (-1-gold) ? 0 : 1);
    } catch (BatchNotFull &)
    {
    }

  return loss;
}

float MultiMLP::getLoss(FeatureModel::FeatureDescription &, const std::vector<float> &)
{
  fprintf(stderr, "ERROR (%s) : only classification is supported. Aborting.\n", ERRINFO);
  exit(1);
  return 0.0;
}

void MultiMLP::save(const std::string & filename)
{
  File * file = new File(filename, "w");
  fprintf(file->getDescriptor(), "#NBMLP:%lu# # {1,1} 0 _\n", mlps.size());
  delete file;
  for (auto & mlp : mlps)
  {
    mlp.saveStruct(filename);
    mlp.saveParameters(filename);
  }
}

void MultiMLP::printTopology(FILE * output)
{
  mlps.back().printTopology(output);
}

void MultiMLP::endOfIteration()
{
  for (auto & mlp : mlps)
    mlp.endOfIteration();
}

