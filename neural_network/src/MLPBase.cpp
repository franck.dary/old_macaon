/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "MLPBase.hpp"

MLPBase::MLPBase(std::string name)
{
  this->name = name;
  dropoutActive = true;
  batchSize = 0;
}

void MLPBase::init(dynet::ParameterCollection & model, int nbInputs, const std::string & topology, int nbOutputs)
{
  std::string topo = topology;
  std::replace(topo.begin(), topo.end(), '(', ' ');
  std::replace(topo.begin(), topo.end(), ')', ' ');

  auto groups = util::split(topo);
  for (auto group : groups)
  {
    if(group.empty())
      continue;

    std::replace(group.begin(), group.end(), ',', ' ');
    auto layer = util::split(group);

    if (layer.size() != 3)
    {
      fprintf(stderr, "ERROR (%s) : invalid topology \'%s\'. Aborting.\n", ERRINFO, topology.c_str());
      exit(1);
    }

    int input = layers.empty() ? nbInputs : layers.back().output_dim;
    int output = std::stoi(layer[0]); 
    float dropout = std::stof(layer[2]);
    layers.emplace_back(input, output, dropout, NeuralNetwork::str2activation(layer[1]));
  }

  layers.emplace_back(layers.back().output_dim, nbOutputs, 0.0, NeuralNetwork::Activation::LINEAR);

  checkLayersCompatibility();

  for(Layer layer : layers)
    addLayerToModel(model, layer);
}

void MLPBase::addLayerToModel(dynet::ParameterCollection & model, Layer & layer)
{
  dynet::Parameter W = model.add_parameters({(unsigned)layer.output_dim, (unsigned)layer.input_dim});
  dynet::Parameter b = model.add_parameters({(unsigned)layer.output_dim});
  if (!ProgramParameters::randomParameters)
  {
    W.set_value(std::vector<float>((unsigned)layer.output_dim * (unsigned)layer.input_dim, 1.0));
    b.set_value(std::vector<float>((unsigned)layer.output_dim, 1.0));
  }
  parameters.push_back({W,b});
}

void MLPBase::checkLayersCompatibility()
{
  if(layers.empty())
  {
    fprintf(stderr, "ERROR (%s) : constructed mlp with 0 layers. Aborting.\n", ERRINFO);
    exit(1);
  }

  for(unsigned int i = 0; i < layers.size()-1; i++)
    if(layers[i].output_dim != layers[i+1].input_dim)
    {
      fprintf(stderr, "ERROR (%s) : constructed mlp with incompatible layers. Aborting.\n", ERRINFO);
      exit(1);
    }
}

std::vector<float> MLPBase::predict(FeatureModel::FeatureDescription & fd)
{
  bool currentDropoutActive = dropoutActive;
  dropoutActive = false;
  dynet::ComputationGraph cg;

  std::vector<dynet::Expression> expressions;

  for (auto & featValue : fd.values)
    expressions.emplace_back(NeuralNetwork::featValue2Expression(cg, featValue));

  dynet::Expression input = dynet::concatenate(expressions);

  dynet::Expression output = run(cg, input);

  dropoutActive = currentDropoutActive;

  return as_vector(cg.forward(dynet::softmax(output)));
}

float MLPBase::update(FeatureModel::FeatureDescription & fd, int gold)
{
  fdsOneHot.emplace_back(fd);
  goldsOneHot.emplace_back(gold);

  int effectiveBatchSize = batchSize ? batchSize : ProgramParameters::batchSize;

  if ((int)fdsOneHot.size() < effectiveBatchSize)
    throw BatchNotFull();

  std::vector<dynet::Expression> inputs;
  dynet::ComputationGraph cg;

  for (auto & example : fdsOneHot)
  {
    std::vector<dynet::Expression> expressions;

    for (auto & featValue : example.values)
      expressions.emplace_back(NeuralNetwork::featValue2Expression(cg, featValue));

    dynet::Expression input = dynet::concatenate(expressions);
    inputs.emplace_back(input);
  }

  dynet::Expression batchedInput = dynet::concatenate_to_batch(inputs);
  dynet::Expression output = run(cg, batchedInput);
  dynet::Expression batchedLoss;
 
  if (ProgramParameters::loss == "neglogsoftmax")
  {
    batchedLoss = dynet::sum_batches(pickneglogsoftmax(output, goldsOneHot));
  }
  else
  {
    fprintf(stderr, "ERROR (%s) : Unknown loss function \'%s\'. Aborting.\n", ERRINFO, ProgramParameters::loss.c_str());
    exit(1);
  }

  cg.backward(batchedLoss);

  checkGradients();

  fdsOneHot.clear();
  goldsOneHot.clear();

  return as_scalar(batchedLoss.value());
}

float MLPBase::update(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold)
{
  fdsContinuous.emplace_back(fd);
  goldsContinuous.emplace_back(gold);

  int effectiveBatchSize = batchSize ? batchSize : ProgramParameters::batchSize;

  if ((int)fdsContinuous.size() < effectiveBatchSize)
    throw BatchNotFull();

  std::vector<dynet::Expression> inputs;
  dynet::ComputationGraph cg;

  for (auto & example : fdsContinuous)
  {
    std::vector<dynet::Expression> expressions;

    for (auto & featValue : example.values)
      expressions.emplace_back(NeuralNetwork::featValue2Expression(cg, featValue));

    dynet::Expression input = dynet::concatenate(expressions);
    inputs.emplace_back(input);
  }

  dynet::Expression batchedInput = dynet::concatenate_to_batch(inputs);
  dynet::Expression output = run(cg, batchedInput);
  dynet::Expression batchedLoss;
  std::vector<dynet::Expression> goldExpressions;
  for (auto & gold : goldsContinuous)
    goldExpressions.emplace_back(dynet::input(cg, dynet::Dim({(unsigned int)gold.size()}), gold));
 
  dynet::Expression batchedGold = dynet::concatenate_to_batch(goldExpressions);
  batchedLoss = dynet::sum_batches(dynet::squared_distance(output, batchedGold));

  cg.backward(batchedLoss);

  checkGradients();

  fdsContinuous.clear();
  goldsContinuous.clear();

  return as_scalar(batchedLoss.value());
}

float MLPBase::getLoss(FeatureModel::FeatureDescription & fd, int gold)
{
  fdsOneHot.emplace_back(fd);
  goldsOneHot.emplace_back(gold);

  int effectiveBatchSize = batchSize ? batchSize : ProgramParameters::batchSize;

  if ((int)fdsOneHot.size() < effectiveBatchSize)
    throw BatchNotFull();

  std::vector<dynet::Expression> inputs;
  dynet::ComputationGraph cg;

  for (auto & example : fdsOneHot)
  {
    std::vector<dynet::Expression> expressions;

    for (auto & featValue : example.values)
      expressions.emplace_back(NeuralNetwork::featValue2Expression(cg, featValue));

    dynet::Expression input = dynet::concatenate(expressions);
    inputs.emplace_back(input);
  }

  dynet::Expression batchedInput = dynet::concatenate_to_batch(inputs);
  dynet::Expression output = run(cg, batchedInput);
  dynet::Expression batchedLoss;
 
  if (ProgramParameters::loss == "neglogsoftmax")
  {
    batchedLoss = dynet::sum_batches(pickneglogsoftmax(output, goldsOneHot));
  }
  else
  {
    fprintf(stderr, "ERROR (%s) : Unknown loss function \'%s\'. Aborting.\n", ERRINFO, ProgramParameters::loss.c_str());
    exit(1);
  }

  checkGradients();

  fdsOneHot.clear();
  goldsOneHot.clear();

  return as_scalar(batchedLoss.value());
}

float MLPBase::getLoss(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold)
{
  fdsContinuous.emplace_back(fd);
  goldsContinuous.emplace_back(gold);

  int effectiveBatchSize = batchSize ? batchSize : ProgramParameters::batchSize;

  if ((int)fdsContinuous.size() < effectiveBatchSize)
    throw BatchNotFull();

  std::vector<dynet::Expression> inputs;
  dynet::ComputationGraph cg;

  for (auto & example : fdsContinuous)
  {
    std::vector<dynet::Expression> expressions;

    for (auto & featValue : example.values)
      expressions.emplace_back(NeuralNetwork::featValue2Expression(cg, featValue));

    dynet::Expression input = dynet::concatenate(expressions);
    inputs.emplace_back(input);
  }

  dynet::Expression batchedInput = dynet::concatenate_to_batch(inputs);
  dynet::Expression output = run(cg, batchedInput);
  dynet::Expression batchedLoss;
  std::vector<dynet::Expression> goldExpressions;
  for (auto & gold : goldsContinuous)
    goldExpressions.emplace_back(dynet::input(cg, dynet::Dim({1,(unsigned int)gold.size()}), gold));
 
  dynet::Expression batchedGold = dynet::concatenate_to_batch(goldExpressions);
  batchedLoss = dynet::sum_batches(dynet::squared_distance(output, batchedGold));

  checkGradients();

  fdsContinuous.clear();
  goldsContinuous.clear();

  return as_scalar(batchedLoss.value());
}

void MLPBase::checkGradients()
{
  bool printGradients = false;

  if (printGradients)
  {
    fprintf(stderr, "Gradients :\n");
    for (auto & layer : parameters)
      for (auto & param : layer)
      {
        auto dim = param.dim();
        auto gradients = param.gradients()->v;
        fprintf(stderr, "Parameter's gradients :\n");
        int nbRows = dim.rows();
        int nbCols = dim.cols();

        for (int i = 0; i < nbRows; i++)
          for (int j = 0; j < nbCols; j++)
            fprintf(stderr, "%8.5f%s", gradients[i*nbRows + j], j == nbCols-1 ? "\n" : " ");
      }
  }
}

dynet::Expression MLPBase::run(dynet::ComputationGraph & cg, dynet::Expression x)
{
  static std::vector< std::pair<std::string,dynet::Expression> > exprForDebug;

  // Expression for the current hidden state
  dynet::Expression h_cur = x;

  if (ProgramParameters::showFeatureRepresentation)
  {
    if (ProgramParameters::showFeatureRepresentation == 1)
      for (unsigned int i = 0; i < 81; i++)
        fprintf(stderr, "%s", i == 80 ? "\n" : "-");
    exprForDebug.clear();
    if (ProgramParameters::showFeatureRepresentation == 1)
      exprForDebug.emplace_back("Input layer", h_cur);
    if (ProgramParameters::showFeatureRepresentation >= 2)
      exprForDebug.emplace_back("", h_cur);
  }

  for(unsigned int l = 0; l < layers.size(); l++)
  {
    // Initialize parameters in computation graph
    dynet::Expression W = parameter(cg, parameters[l][0]);
    dynet::Expression b = parameter(cg, parameters[l][1]);
    // Apply affine transform
    dynet::Expression a = dynet::affine_transform({b, W, h_cur});
    // Apply activation function
    dynet::Expression h = NeuralNetwork::activate(a, layers[l].activation);
    // Take care of dropout
    dynet::Expression h_dropped;
    if(layers[l].dropout_rate > 0){
      if(dropoutActive){
        dynet::Expression mask = random_bernoulli(cg, 
         {(unsigned int)layers[l].output_dim}, 1 - layers[l].dropout_rate);
        h_dropped = cmult(h, mask);
      }
      else{
        h_dropped = h * (1 - layers[l].dropout_rate);
      }
    }
    else{
      h_dropped = h;
    }

    if (ProgramParameters::showFeatureRepresentation)
    {
      if (ProgramParameters::showFeatureRepresentation == 1)
      {
        exprForDebug.emplace_back("Result of h = h*W_" + std::to_string(l) + " + b_" + std::to_string(l), a);
        exprForDebug.emplace_back("Result of h = a_" + std::to_string(l) + "(h)", h);
        exprForDebug.emplace_back("Result of h = dropout_" + std::to_string(l) + "(h)", h_dropped);
      }
      else if (ProgramParameters::showFeatureRepresentation >= 2)
      {
        exprForDebug.emplace_back("", a);
        exprForDebug.emplace_back("", h);
      }
    }

    h_cur = h_dropped;
  }

  if (ProgramParameters::showFeatureRepresentation)
  {
    cg.forward(h_cur);

    if (ProgramParameters::showFeatureRepresentation == 1)
    {
      for (auto & it : exprForDebug)
        fprintf(stderr, "%s (dimension=%lu) :\n%s\n", it.first.c_str(), dynet::as_vector(it.second.value()).size(), NeuralNetwork::expression2str(it.second).c_str());
      for (unsigned int i = 0; i < 81; i++)
        fprintf(stderr, "%s", i == 80 ? "\n" : "-");
    }
    else if (ProgramParameters::showFeatureRepresentation >= 2)
    {
      for (auto & it : exprForDebug)
        fprintf(stderr, "| %s |", NeuralNetwork::expression2str(it.second).c_str());
      fprintf(stderr, "\n");
    }
  }

  return h_cur;
}

void MLPBase::printParameters(FILE * output)
{
  fprintf(output, "Parameters : NOT IMPLEMENTED\n");
}

void MLPBase::saveStruct(const std::string & filename)
{
  File file(filename, "a");
  FILE * fd = file.getDescriptor();

  for (auto & layer : layers)
  {
    fprintf(fd, "#TOPOLOGY# # {1,1} 0 Layer : %d %d %s %.2f\n", layer.input_dim, layer.output_dim, NeuralNetwork::activation2str(layer.activation).c_str(), layer.dropout_rate);
  }
}

void MLPBase::saveParameters(const std::string & filename)
{
  dynet::TextFileSaver s(filename, true);
  std::string prefix(name + "_Layer_");

  for(unsigned int i = 0; i < parameters.size(); i++)
  {
    s.save(parameters[i][0], prefix + std::to_string(i) + "_W");
    s.save(parameters[i][1], prefix + std::to_string(i) + "_b");
  }
}

void MLPBase::loadStruct(dynet::ParameterCollection & model, const std::string & filename, unsigned int index)
{
  File file(filename, "r");
  FILE * fd = file.getDescriptor();

  char activation[1024];
  int input;
  int output;
  float dropout;

  for (unsigned int i = 0; i < index; i++)
  {
    while (fscanf(fd, "#TOPOLOGY# # {1,1} 0 Layer : %d %d %s %f\n", &input, &output, activation, &dropout) != 4)
      if (fscanf(fd, "%10[^\n]\n", activation) != 1)
      {
        fprintf(stderr, "ERROR (%s) : Unexpected end of file \'%s\'. Aborting.\n", ERRINFO, file.getName().c_str());
        exit(1);
      }

    do
    {
    } while (fscanf(fd, "#TOPOLOGY# # {1,1} 0 Layer : %d %d %s %f\n", &input, &output, activation, &dropout) == 4);
  }

  while (fscanf(fd, "#TOPOLOGY# # {1,1} 0 Layer : %d %d %s %f\n", &input, &output, activation, &dropout) != 4)
    if (fscanf(fd, "%10[^\n]\n", activation) != 1)
    {
      fprintf(stderr, "ERROR (%s) : Unexpected end of file \'%s\'. Aborting.\n", ERRINFO, file.getName().c_str());
      exit(1);
    }

  do
  {
    layers.emplace_back(input, output, dropout, NeuralNetwork::str2activation(activation));
  } while (fscanf(fd, "#TOPOLOGY# # {1,1} 0 Layer : %d %d %s %f\n", &input, &output, activation, &dropout) == 4);

  checkLayersCompatibility();

  for (auto & layer : layers)
    addLayerToModel(model, layer);
}

void MLPBase::loadParameters(dynet::ParameterCollection & model, const std::string & filename)
{
  dynet::TextFileLoader loader(filename);
  std::string prefix(name + "_Layer_");

  for(unsigned int i = 0; i < parameters.size(); i++)
  {
    try
    {
      parameters[i][0] = loader.load_param(model, prefix + std::to_string(i) + "_W");
      parameters[i][1] = loader.load_param(model, prefix + std::to_string(i) + "_b");
    } catch(const std::runtime_error & e)
    {
      fprintf(stderr, "WARNING (%s) : Could not find parameter with key \'%s\' in the model. Ignore this if this model was trained with an older version of Macaon.\n", ERRINFO, (prefix+std::to_string(i) + "_W").c_str());
      prefix = "Layer_";
      parameters[i][0] = loader.load_param(model, prefix + std::to_string(i) + "_W");
      parameters[i][1] = loader.load_param(model, prefix + std::to_string(i) + "_b");     
    }
  }
}

void MLPBase::printTopology(FILE * output)
{
  fprintf(output, "(");
  for(unsigned int i = 0; i < layers.size(); i++)
  {
    auto & layer = layers[i];

    if(i == 0)
      fprintf(output, "%d", layer.input_dim);
    fprintf(output, "->%d", layer.output_dim);
  }

  fprintf(output, ")\n");
}

void MLPBase::endOfIteration()
{
  fdsOneHot.clear();
  fdsContinuous.clear();
  goldsOneHot.clear();
  goldsContinuous.clear();
}

void MLPBase::setBatchSize(int batchSize)
{
  this->batchSize = batchSize;
}

