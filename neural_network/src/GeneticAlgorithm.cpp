/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "GeneticAlgorithm.hpp"
#include "ProgramParameters.hpp"
#include "util.hpp"

int GeneticAlgorithm::Individual::idCount = 0;

GeneticAlgorithm::GeneticAlgorithm()
{
  randomSeed = ProgramParameters::seed;
  initDynet();
}

GeneticAlgorithm::GeneticAlgorithm(const std::string & filename)
{
  randomSeed = ProgramParameters::seed;
  initDynet();

  load(filename);
}

void GeneticAlgorithm::init(int nbInputs, const std::string & topology, int nbOutputs)
{
  this->nbInputs = nbInputs;
  this->nbOutputs = nbOutputs;
  this->topology = topology;

  auto splited = util::split(topology, ' ');
  if (splited.size() != 2 || !util::isNum(splited[0]))
  {
    fprintf(stderr, "ERROR (%s) : wrong topology \'%s\'. Aborting.\n", ERRINFO, topology.c_str());
    exit(1);
  }

  int nbElems = std::stoi(splited[0]);

  for (int i = 0; i < nbElems; i++)
    generation.emplace_back(new Individual(model, nbInputs, splited[1], nbOutputs));
}

std::vector<float> GeneticAlgorithm::predict(FeatureModel::FeatureDescription & fd)
{
  int toAsk = ProgramParameters::nbIndividuals;

  if (toAsk < 0 || toAsk > (int)generation.size())
  {
    fprintf(stderr, "ERROR (%s) : trying to use \'%d\' individuals out of a population of \'%lu\'. Aborting.\n", ERRINFO, toAsk, generation.size());
    exit(1);
  }

  auto prediction = generation[0]->mlp.predict(fd);
  for (int i = 1; i < toAsk; i++)
  {
    auto otherPrediction = generation[i]->mlp.predict(fd);
    for (unsigned int j = 0; j < prediction.size(); j++)
      prediction[j] += otherPrediction[j];
  }

  for (unsigned int j = 0; j < prediction.size(); j++)
    prediction[j] /= toAsk;

  return prediction;
}

float GeneticAlgorithm::getLoss(FeatureModel::FeatureDescription &, int)
{
  float loss = 0.0;

  for (auto & individual : generation)
    loss += individual->loss;

  return loss;
}

float GeneticAlgorithm::getLoss(FeatureModel::FeatureDescription &, const std::vector<float> &)
{
  fprintf(stderr, "ERROR (%s) : not implemented. Aborting.\n", ERRINFO);
  exit(1);
}

float GeneticAlgorithm::update(FeatureModel::FeatureDescription & , const std::vector<float> & )
{
  fprintf(stderr, "ERROR (%s) : not implemented. Aborting.\n", ERRINFO);
  exit(1);
}

float GeneticAlgorithm::update(FeatureModel::FeatureDescription & fd, int gold)
{
  bool haveBeenUpdated = false;

  for (auto & individual : generation)
  {
    float loss = individual->mlp.update(fd, gold);
    if (loss != 0.0)
    {
      individual->loss = loss / ProgramParameters::batchSize;
      individual->value = loss2value(individual->loss);
      haveBeenUpdated = true;
    }
  }

  if (!haveBeenUpdated)
    return 0.0;

  std::sort(generation.begin(), generation.end(),
            [](const std::unique_ptr<Individual> & a, const std::unique_ptr<Individual> & b)
  {
    return a->value > b->value;
  });

  fprintf(stderr, "Best : %3d with value %.2f Worst : %3d with value %.2f\n", generation[0]->id, generation[0]->value, generation.back()->id, generation.back()->value);

  unsigned int quarter = generation.size() / 4;
  if (quarter%2)
    quarter++;
  std::vector<unsigned int> reproductors;
  std::vector<unsigned int> candidates;
  for (unsigned int i = 0; i < generation.size(); i++)
    candidates.push_back(i);

  while (reproductors.size() < quarter)
    for (unsigned int i = 0; i < candidates.size(); i++)
    {
      int parts = candidates.size() * ((candidates.size()+1) / 2.0);
      float probability = (candidates.size()-i) / (1.0*parts);

      if (util::choiceWithProbability(probability))
      {
        reproductors.push_back(candidates[i]);
        candidates.erase(candidates.begin() + i);
        i--;
      }
    }

  while (reproductors.size() != quarter)
    reproductors.pop_back();

  // Reproduction
  for (unsigned int i = 2*quarter; i < 3*quarter; i++)
  {
    int momIndex = reproductors.size()-1-(i-2*quarter);
    int dadIndex = i-2*quarter;

    generation[i]->becomeChildOf(generation[reproductors[momIndex]].get(), generation[reproductors[dadIndex]].get());
  }

  // Mutation
  for (unsigned int i = 1*quarter; i < 2*quarter; i++)
    generation[i]->mutate(0.01);

  // Death and replace
  for (unsigned int i = 3*quarter; i < generation.size(); i++)
    generation[i]->mutate(1.0);

  return generation[0]->loss;
}

void GeneticAlgorithm::save(const std::string & filename)
{
  int toSave = ProgramParameters::nbIndividuals;

  if (toSave < 0 || toSave > (int)generation.size())
  {
    fprintf(stderr, "ERROR (%s) : trying to save \'%d\' individuals out of a population of \'%lu\'. Aborting.\n", ERRINFO, toSave, generation.size());
    exit(1);
  }

  File * file = new File(filename, "w");
  for (int i = 0; i < toSave; i++)
    fprintf(file->getDescriptor(), "#SAVED_ID# # {1,1} 0 %u\n", generation[i]->id);
  delete file;
  
  for (int i = 0; i < toSave; i++)
  {
    generation[i]->mlp.saveStruct(filename);
    generation[i]->mlp.saveParameters(filename);
  }
}

void GeneticAlgorithm::printTopology(FILE * output)
{
  if (generation.empty())
  {
    fprintf(output, "0 x ()\n");
  }
  else
  {
    fprintf(output, "%lu x ", generation.size());
    generation[0]->mlp.printTopology(output);
  }
}

void GeneticAlgorithm::load(const std::string & filename)
{
  std::vector<int> ids;
  File * file = new File(filename, "r");
  unsigned int id;
  while (fscanf(file->getDescriptor(), "#SAVED_ID# # {1,1} 0 %u\n", &id) == 1)
    ids.emplace_back(id);
  delete file;

  if (ids.empty())
  {
    fprintf(stderr, "ERROR (%s) : Missing MLP\''s ids in file \'%s\'. Aborting.\n", ERRINFO, filename.c_str());
    exit(1);
  }

  ProgramParameters::nbIndividuals = ids.size();

  for (auto & id : ids)
  {
    generation.emplace_back(new Individual(id));

    generation.back()->mlp.loadStruct(model, filename, generation.size()-1);
    generation.back()->mlp.loadParameters(model, filename);
  }
}

GeneticAlgorithm::Individual::Individual(dynet::ParameterCollection & model, int nbInputs, const std::string & topology, int nbOutputs) : mlp("MLP_" + std::to_string(idCount))
{
  this->id = idCount++;
  this->loss = 0.0;
  this->value = 0.0;
  mlp.init(model, nbInputs, topology, nbOutputs);
  // mutate with probability 1.0 = random init
  mutate(1.0);
}

GeneticAlgorithm::Individual::Individual(unsigned int bestId)
  : mlp("MLP_" + std::to_string(bestId))
{
  this->id = bestId;
  this->loss = 0.0;
  this->value = 0.0;
}

float GeneticAlgorithm::loss2value(float loss)
{
  return 1000.0 / loss;
}

void GeneticAlgorithm::Individual::becomeChildOf(Individual * other)
{
  auto & thisParameters = mlp.parameters;
  auto & otherParameters = other->mlp.parameters;

  if (thisParameters.size() != otherParameters.size())
  {
    fprintf(stderr, "ERROR (%s) : The two individuals are not compatibles. Sizes %lu and %lu. Aborting.\n", ERRINFO, thisParameters.size(), otherParameters.size());
    exit(1);
  }

  for (unsigned int i = 0; i < thisParameters.size(); i++)
    for (unsigned int j = 0; j < thisParameters[i].size(); j++)
    {
      auto & thisParameter = thisParameters[i][j];
      auto & otherParameter = otherParameters[i][j];
      float * thisValues = thisParameter.values()->v;
      float * otherValues = otherParameter.values()->v;
      unsigned int nbValues = thisParameter.values()->d.size();

      for (unsigned int k = 0; k < nbValues; k++)
        if (util::choiceWithProbability(0.5))
          thisValues[k] = otherValues[k];
    }
}

void GeneticAlgorithm::Individual::becomeChildOf(Individual * mom, Individual * dad)
{
  auto & thisParameters = mlp.parameters;
  auto & momParameters = mom->mlp.parameters;
  auto & dadParameters = dad->mlp.parameters;

  if (thisParameters.size() != momParameters.size() || thisParameters.size() != dadParameters.size())
  {
    fprintf(stderr, "ERROR (%s) : The three individuals are not compatibles. Sizes %lu and %lu and %lu. Aborting.\n", ERRINFO, thisParameters.size(), momParameters.size(), dadParameters.size());
    exit(1);
  }

  for (unsigned int i = 0; i < thisParameters.size(); i++)
    for (unsigned int j = 0; j < thisParameters[i].size(); j++)
    {
      auto & thisParameter = thisParameters[i][j];
      auto & momParameter = momParameters[i][j];
      auto & dadParameter = dadParameters[i][j];
      float * thisValues = thisParameter.values()->v;
      float * momValues = momParameter.values()->v;
      float * dadValues = dadParameter.values()->v;
      unsigned int nbValues = thisParameter.values()->d.size();

      for (unsigned int k = 0; k < nbValues; k++)
        if (util::choiceWithProbability(0.5))
          thisValues[k] = momValues[k];
        else
          thisValues[k] = dadValues[k];
    }
}

void GeneticAlgorithm::Individual::mutate(float probability)
{
  auto & thisParameters = mlp.parameters;

  for (unsigned int i = 0; i < thisParameters.size(); i++)
    for (unsigned int j = 0; j < thisParameters[i].size(); j++)
    {
      auto & thisParameter = thisParameters[i][j];
      float * thisValues = thisParameter.values()->v;
      unsigned int nbValues = thisParameter.values()->d.size();

      for (unsigned int k = 0; k < nbValues; k++)
        if (util::choiceWithProbability(probability))
          thisValues[k] = util::getRandomValueInRange(3);
    }
}

void GeneticAlgorithm::endOfIteration()
{
  for (auto & it : generation)
    it->mlp.endOfIteration();
}

