/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file MLPBase.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2019-01-13

#ifndef MLPBASE__H
#define MLPBASE__H

#include <vector>
#include <map>
#include <string>
#include "ProgramParameters.hpp"
#include "NeuralNetwork.hpp"

/// @brief Multi Layer Perceptron.
///
/// Once trained, it can also be used to predict the class of a certain input.
class MLPBase
{
  public :

  using Layer = NeuralNetwork::Layer;
  /// @brief The name of this MLP.
  std::string name;
  /// @brief The Layers of the MLP.
  std::vector<Layer> layers;
  /// @brief The parameters corresponding to the layers of the MLP.
  std::vector< std::vector<dynet::Parameter> > parameters;

  /// @brief Must the Layer dropout rate be taken into account during the computations ? Usually it is only during the training step.
  bool dropoutActive;

  /// @brief The current minibatch for one hot golds.
  std::vector<FeatureModel::FeatureDescription> fdsOneHot;
  /// @brief gold classes of the current minibatch.
  std::vector<unsigned int> goldsOneHot;
  /// @brief The current minibatch for continuous golds.
  std::vector<FeatureModel::FeatureDescription> fdsContinuous;
  /// @brief gold outputs of the current minibatch.
  std::vector< std::vector<float> > goldsContinuous;
  int batchSize;

  private :

  /// \brief Check gradients values for debug purpose.
  void checkGradients();

  public :

  /// @brief Add the parameters of a layer into the dynet model.
  ///
  /// @param model The dynet model that will contains the parameters of the layer.
  /// @param layer The layer to add.
  void addLayerToModel(dynet::ParameterCollection & model, Layer & layer);
  /// @brief Abort the program if the layers are not compatible.
  void checkLayersCompatibility();
  /// @brief Compute the image of input x by the Multi Layer Perceptron.
  ///
  /// @param cg The current computation graph.
  /// @param x The input of the Multi Layer Perceptron.
  ///
  /// @return The result (values of the output Layer) of the computation of x by the Multi Layer Perceptron.
  dynet::Expression run(dynet::ComputationGraph & cg, dynet::Expression x);
  /// @brief Print the parameters.
  ///
  /// @param output Where the parameters will be printed to.
  void printParameters(FILE * output);
  /// @brief Save the structure of the MLP (all the Layer) to a file.
  ///
  /// The goal is to store the structure of the MLP into a file so that
  /// we can load it and use it another time.
  /// @param filename The file in which the structure will be saved.
  void saveStruct(const std::string & filename);
  /// @brief Save the learned parameters of the MLP to a file.
  ///
  /// Only the parameters of the Layers will be saved by this function.\n
  /// The parameters that are values inside of Dict, will be saved by their owner,
  /// the Dict object.
  /// @param filename The file in which the parameters will be saved.
  void saveParameters(const std::string & filename);
  /// @brief Load and construt all the Layer from a file.
  ///
  /// The file must have been written by the function saveStruct.
  /// @param model The dynet model that will contain the loaded parameters.
  /// @param filename The file from which the structure will be read.
  /// @param index The index of the structure in the file.
  void loadStruct(dynet::ParameterCollection & model, const std::string & filename, unsigned int index);
  /// @brief Load and populate the model with parameters from a file.
  ///
  /// The file must have been written by the function saveParameters.
  /// @param model The dynet model that will contain the loaded parameters.
  /// @param filename The file from which the parameters will be read.
  void loadParameters(dynet::ParameterCollection & model, const std::string & filename);
  /// @brief initialize a new untrained MLP from a desired topology.
  ///
  /// topology example for 2 hidden layers : (150,RELU,0.3)(50,ELU,0.2)\n
  /// Of sizes 150 and 50, activation functions RELU and ELU, and dropout rates
  /// of 0.3 and 0.2.
  /// @param model The dynet model that will contains all the MLP parameters.
  /// @param nbInputs The size of the input layer of the MLP.
  /// @param topology Description of each hidden Layer of the MLP.
  /// @param nbOutputs The size of the output layer of the MLP.
  void init(dynet::ParameterCollection & model, int nbInputs, const std::string & topology, int nbOutputs);
  /// @brief Construct a new MLP for training.
  ///
  /// @param name The name of this MLP.
  MLPBase(std::string name);
  /// @brief Give a score to each possible class, given an input.
  ///
  /// @param fd The input to use.
  ///
  /// @return A vector containing one score per possible class.
  std::vector<float> predict(FeatureModel::FeatureDescription & fd);
  /// @brief Update the parameters according to the given gold class.
  ///
  /// @param fd The input to use.
  /// @param gold The gold class of this input.
  ///
  /// @return The loss.
  float update(FeatureModel::FeatureDescription & fd, int gold);
  /// @brief Update the parameters according to the given gold output.
  ///
  /// @param fd The input to use.
  /// @param gold The gold output for this input.
  ///
  /// @return The loss.
  float update(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold);
  /// @brief Get the loss according to the given gold class.
  ///
  /// @param fd The input to use.
  /// @param gold The gold class of this input.
  ///
  /// @return The loss.
  float getLoss(FeatureModel::FeatureDescription & fd, int gold);
  /// @brief Get the loss according to the given output vector.
  ///
  /// @param fd The input to use.
  /// @param gold The gold output for this input.
  ///
  /// @return The loss.
  float getLoss(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold);
  /// @brief Print the topology (Layers) of the MLP.
  ///
  /// @param output Where the topology will be printed.
  void printTopology(FILE * output);
  /// @brief Clear the current batch.
  void endOfIteration();
  void setBatchSize(int batchSize);
};

#endif
