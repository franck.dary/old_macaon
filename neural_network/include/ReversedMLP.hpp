/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// \file ReversedMLP.hpp
/// \author Franck Dary
/// @version 1.0
/// @date 2019-08-08

#ifndef REVERSEDMLP__H
#define REVERSEDMLP__H

#include "NeuralNetwork.hpp"
#include "MLPBase.hpp"
#include "ProgramParameters.hpp"

/// @brief Classifier consisting in 2 MLP, can be trained with negative or positive classes.
///
/// It is capable of training itself given a batch of examples.\n
/// Once trained, it can also be used to predict the class of a certain input.
class ReversedMLP : public NeuralNetwork
{
  private :

  /// @brief The mlp that will be trained using positive gold classes.
  MLPBase mlpPos;
  /// @brief The mlp that will be trained using negative gold classes.
  MLPBase mlpNeg;
  /// @brief The training algorithm that will be used.
  std::unique_ptr<dynet::Trainer> trainer;

  public :

  /// @brief initialize a new untrained ReversedMLP from a desired topology.
  ///
  /// topology example for 2 hidden layers : (150,RELU,0.3)(50,ELU,0.2)\n
  /// Of sizes 150 and 50, activation functions RELU and ELU, and dropout rates
  /// of 0.3 and 0.2.
  /// @param nbInputs The size of the input layer of the MLP.
  /// @param topology Description of each hidden Layer of the MLP.
  /// @param nbOutputs The size of the output layer of the MLP.
  void init(int nbInputs, const std::string & topology, int nbOutputs) override;
  /// @brief Construct a new ReversedMLP for training.
  ReversedMLP();
  /// @brief Read and construct a trained ReversedMLP from a file.
  ///
  /// The file must have been written by save.
  /// @param filename The file to read the ReversedMLP from.
  ReversedMLP(const std::string & filename);
  /// @brief Give a score to each possible class, given an input.
  ///
  /// @param fd The input to use.
  ///
  /// @return A vector containing one score per possible class.
  std::vector<float> predict(FeatureModel::FeatureDescription & fd) override;
  /// @brief Update the parameters according to the given gold class.
  ///
  /// @param fd The input to use.
  /// @param gold The gold class of this input.
  ///
  /// @return The loss.
  float update(FeatureModel::FeatureDescription & fd, int gold) override;
  /// @brief Get the loss according to the given gold class.
  ///
  /// @param fd The input to use.
  /// @param gold The gold class of this input.
  ///
  /// @return The loss.
  float update(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold) override;
  /// @brief Get the loss according to the given gold class.
  ///
  /// @param fd The input to use.
  /// @param gold The gold class of this input.
  ///
  /// @return The loss.
  float getLoss(FeatureModel::FeatureDescription & fd, int gold) override;
  /// @brief Get the loss according to the given gold vector.
  ///
  /// @param fd The input to use.
  /// @param gold The gold vector for this input.
  ///
  /// @return The loss.
  float getLoss(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold) override;
  /// @brief Save the ReversedMLP to a file.
  /// 
  /// @param filename The file to write the ReversedMLP to.
  void save(const std::string & filename) override;
  /// @brief Print the topology (Layers) of the ReversedMLP.
  ///
  /// @param output Where the topology will be printed.
  void printTopology(FILE * output) override;
  /// @brief Allocate the correct trainer type depending on the program parameters.
  ///
  /// @return A pointer to the newly allocated trainer.
  dynet::Trainer * createTrainer();
  void endOfIteration() override;
};

#endif
