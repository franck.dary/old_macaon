/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#ifndef NEURALNETWORK__H
#define NEURALNETWORK__H

#include <dynet/nodes.h>
#include <dynet/dynet.h>
#include <dynet/training.h>
#include <dynet/timing.h>
#include <dynet/expr.h>
#include <dynet/io.h>
#include <string>
#include "FeatureModel.hpp"

struct BatchNotFull : public std::exception
{
  const char * what() const throw()
  {
    return "Current batch is not full, no need to update.";
  }
};

class NeuralNetwork
{
  public :

  /// @brief Activation function for a Layer.
  enum Activation
  {
    SIGMOID,
    TANH,
    RELU,
    ELU,
    LINEAR,
    SPARSEMAX,
    CUBE,
    SOFTMAX
  };

  /// @brief Get the string corresponding to an Activation.
  ///
  /// @param a The activation.
  ///
  /// @return The string corresponding to a.
  static std::string activation2str(Activation a);

  /// @brief Get the Activation corresponding to a string.
  ///
  /// @param s The string.
  ///
  /// @return The Activation corresponding to s. If s is unknown, the program abort.
  static Activation str2activation(std::string s);

  /// @brief A simple struct that represents a Layer.
  struct Layer
  {
    /// @brief Number of input neurons of this Layer.
    int input_dim;
    /// @brief Number of output neurons of this Layer.
    int output_dim;

    /// @brief The dropout rate to apply to this Layer when training.
    float dropout_rate;
    /// @brief The activation function for this Layer.
    Activation activation;

    /// @brief Construct a new Layer
    ///
    /// @param input_dim 
    /// @param output_dim
    /// @param dropout_rate
    /// @param activation
    Layer(int input_dim, int output_dim,
      float dropout_rate, Activation activation);
    /// @brief Print a description of this Layer.
    ///
    /// @param file Where to print the output.
    void print(FILE * file);
  };

  /// @brief Convert a FeatureValue to a dynet Expression that will be used as an input of the NeuralNetwork.
  ///
  /// @param cg The current Computation Graph.
  /// @param fv The FeatureValue that will be converted.
  ///
  /// @return A dynet Expression of value fv that can be used as an input in the NeuralNetwork 
  static dynet::Expression featValue2Expression(dynet::ComputationGraph & cg, const FeatureModel::FeatureValue & fv);

  /// @brief Compute the image of an expression by an activation function.
  ///
  /// @param h The expression we want the image of.
  /// @param f The activation function.
  ///
  /// @return f(h)
  static dynet::Expression activate(dynet::Expression h, Activation f);

  protected :

  /// @brief The seed that will be used by RNG (srand and dynet)
  static int randomSeed;

  /// @brief Tracks wether or not dynet has been initialized
  static bool dynetIsInit;

  /// @brief The dynet model containing the parameters to be trained.
  dynet::ParameterCollection model;

  /// @brief The size of batches, if not specified, use the global batchsize.
  int batchSize;

  protected :

  /// @brief Set dynet and srand() seeds.
  ///
  /// @return The DynetParams containing the set seed.
  dynet::DynetParams & getDefaultParams();
  /// @brief Initialize the dynet library.
  ///
  /// Must be called only once, and before any call to dynet functions.
  void initDynet();

  public :

  virtual ~NeuralNetwork() = 0;

  /// @brief Convert a dynet expression to a string (usefull for debug purposes)
  ///
  /// @param expr The expression to convert.
  ///
  /// @return A string representing the expression.
  static std::string expression2str(dynet::Expression & expr);

  /// @brief initialize a new untrained NeuralNetwork from a desired topology.
  ///
  /// @param nbInputs The size of the input layer of the NeuralNetwork.
  /// @param topology Description of the NeuralNetwork.
  /// @param nbOutputs The size of the output layer of the NeuralNetwork.
  virtual void init(int nbInputs, const std::string & topology, int nbOutputs) = 0;

  /// @brief Give a score to each possible class, given an input.
  ///
  /// @param fd The input to use.
  ///
  /// @return A vector containing one score per possible class.
  virtual std::vector<float> predict(FeatureModel::FeatureDescription & fd) = 0;

  /// @brief Update the parameters according to the given gold class.
  ///
  /// @param fd The input to use.
  /// @param gold The gold class of this input.
  ///
  /// @return The loss.
  virtual float update(FeatureModel::FeatureDescription & fd, int gold) = 0;

  /// @brief Update the parameters according to the given gold vector.
  ///
  /// @param fd The input to use.
  /// @param gold The gold vector for this input.
  ///
  /// @return The loss.
  virtual float update(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold) = 0;

  /// @brief Get the loss according to the given gold class.
  ///
  /// @param fd The input to use.
  /// @param gold The gold class of this input.
  ///
  /// @return The loss.
  virtual float getLoss(FeatureModel::FeatureDescription & fd, int gold) = 0;

  /// @brief Get the loss according to the given gold vector.
  ///
  /// @param fd The input to use.
  /// @param gold The gold vector for this input.
  ///
  /// @return The loss.
  virtual float getLoss(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold) = 0;

  /// @brief Save the NeuralNetwork to a file.
  /// 
  /// @param filename The file to write the NeuralNetwork to.
  virtual void save(const std::string & filename) = 0;

  /// @brief Print the topology of the NeuralNetwork.
  ///
  /// @param output Where the topology will be printed.
  virtual void printTopology(FILE * output) = 0;

  virtual void endOfIteration() = 0;

  /// @brief Return the model.
  ///
  /// @return The model of this NeuralNetwork.
  dynet::ParameterCollection & getModel();

  /// \brief How much input neurons a certain feature will take.
  ///
  /// \param fv The FeatureValue to measure the size of.
  ///
  /// \return The number of input neurons taken by fv.
  static unsigned int featureSize(const FeatureModel::FeatureValue & fv);

  /// @brief Set the batchSize.
  void setBatchSize(int batchSize);

  /// @brief Get the batchSize.
  int getBatchSize();
};

#endif
