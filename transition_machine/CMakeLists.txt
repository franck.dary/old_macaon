FILE(GLOB SOURCES src/*.cpp)

#compiling library
add_library(transition_machine STATIC ${SOURCES})
target_link_libraries(transition_machine maca_common)
target_link_libraries(transition_machine neural_network)
