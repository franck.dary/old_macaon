/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file BD.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-06

#ifndef BD__H
#define BD__H

#include <map>
#include <memory>
#include "Dict.hpp"

/// @brief This class contains the description of a buffer and of a MCD.
///
/// It is created by reading a .bd and a .mcd file.\n
/// It is used to construct the Config.
class BD
{
  /// @brief Describes a Line of the multi-tapes buffer.
  struct Line
  {
    /// @brief The index of this Line.
    int num;
    /// @brief The name of this Line.
    std::string name;
    /// @brief The name of the Dict containing the vocabulary of strings that can be written on this Line.
    std::string dict;
    /// @brief The column of the MCD this Line corresponds to.
    ///
    /// This can be -1 if the Line corresponds to nothing in the MCD.
    int inputColumn;
    /// @brief Whether or not this Line is part of the expected output of the program.
    bool mustPrint;
    /// @brief Whether or not the entirety of this Line is already known.
    ///
    /// If this Line's values don't need to be predicted.
    bool isKnown;
    /// @brief What column will it be in the output.
    int outputIndex;

    /// @brief Create a new Line.
    ///
    /// @param num The index of this Line.
    /// @param name The name of this Line.
    /// @param dictName The name of the Dict used by this Line.
    /// @param inputColumn The column of the MCD this Line corresponds to.
    /// @param mustPrint Whether or not this Line is part of the expected output of the program.
    /// @param isKnown Whether or not the entirety of this Line is already known.
    Line(int num, int outputIndex, std::string name, std::string dictName, int inputColumn, bool mustPrint, bool isKnown);
  };

  private :

  /// @brief The Lines of this BD.
  std::vector< std::unique_ptr<Line> > lines;
  /// @brief Maps each line's index to its Line.
  std::map<int, Line*> num2line;
  /// @brief Maps each line's name to its Line.
  std::map<std::string, Line*> name2line;
  /// @brief Maps each MCD's column to its corresponding Line.
  std::map<int, Line*> col2line;

  public :

  /// @brief Read and construct a new BD from files.
  /// 
  /// @param BDfilename The .bd file, that describes the buffer.
  /// @param MCDfilename The .mcd file, that describes the input.
  BD(const std::string & BDfilename, const std::string & MCDfilename);
  /// @brief Get the Dict of a Line from its index.
  ///
  /// @param num The index of the Line.
  ///
  /// @return The Dict of the Line.
  Dict * getDictOfLine(int num);
  /// @brief Get the Dict of a Line from its name.
  ///
  /// @param name The name of the Line.
  ///
  /// @return The Dict of the Line.
  Dict * getDictOfLine(const std::string & name);
  /// @brief Get the Dict of a Line from its corresponding MCD column.
  ///
  /// @param col The index of the MCD column.
  ///
  /// @return The Dict of the Line.
  Dict * getDictOfInputCol(int col);
  /// @brief Check if a line exists.
  ///
  /// @param name The name of the Line.
  ///
  /// @return Whether the line is in the BD.
  bool hasLineOfName(const std::string & name);
  /// @brief Get the index of a Line from its name.
  ///
  /// @param name The name of the Line.
  ///
  /// @return The index of the Line.
  int getLineOfName(const std::string & name);
  /// @brief Get the index of a Line from its corresponding MCD column.
  ///
  /// @param col The index of the MCD column.
  ///
  /// @return The index of the Line.
  int getLineOfInputCol(int col);
  /// @brief Get the number of Line.
  ///
  /// @return The number of Line.
  int getNbLines();
  /// @brief Whether or not this Line is part of the output.
  ///
  /// @param index The index of the Line.
  ///
  /// @return Whether or not this Line is part of the output.
  bool mustPrintLine(int index);
  /// @brief Return the column index of line index in the outut.
  ///
  /// @param index The index of the Line.
  ///
  /// @return The column index of this line in the output.
  int getOutputIndexOfLine(int index);
  /// @brief Get the name of a Line from its index.
  ///
  /// @param line The index of the Line.
  ///
  /// @return The name of the Line.
  const std::string & getNameOfLine(int line);
  /// @brief Whether or not the entirety of this Line is already known.
  ///
  /// @param line The index of the Line.
  ///
  /// @return Whether or not the entirety of this Line is already known.
  bool lineIsKnown(int line);
  /// @brief Whether or not this MCD column correspond to a Line.
  ///
  /// @param col The MCD column index.
  ///
  /// @return Whether or not this MCD column correspond to a Line.
  bool hasLineOfInputCol(int col);
};

#endif
