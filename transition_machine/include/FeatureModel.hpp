/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file FeatureModel.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-07

#ifndef FEATUREMODEL__H
#define FEATUREMODEL__H

#include <vector>
#include <functional>
#include "Config.hpp"

/// @brief A FeatureModel is a set of features, used by a specific Classifier.
///
/// It can be seen as a function that can break any Configuration into a FeatureDescription (a vector of real values that can be used as an input of a neural network).
class FeatureModel
{
  public :

  /// @brief The Policy of a Feature.
  ///
  /// If a Feature is Modifiable, its value will be updated during the training step. It will not if it's Final.
  enum Policy
  {
    Modifiable,
    Final
  };

  /// @brief How to transform a FeatureValue into an input expression for the NeuralNetwork.
  ///
  /// Concat = concatenate values, mean = sum then mean values.
  enum Function
  {
    Concat,
    Mean
  };

  /// @brief The value of a Feature (relative to a specific Config).
  ///
  /// Can be an aggregate of multiple Feature.
  struct FeatureValue
  {
    /// @brief The Dicts that contains the values and their real vector.
    std::vector<Dict *> dicts;
    /// @brief The names of the Features that compose this FeatureValue.
    std::vector<std::string> names;
    /// @brief The string value of the Features.
    std::vector<std::string> values;
    /// @brief The Policy of the Features.
    std::vector<Policy> policies;
    /// @brief The real valued vector as a string.
    ///
    /// @param i of the requested feature, if its an aggregate.
    ///
    /// @return The real valued vector.
    std::string toString(unsigned int i);
    /// \brief How to transform this FeatureValue into an input expression for the NeuralNetwork
    Function func;
    FeatureValue(Function func);
    FeatureValue(Dict *, const std::string &, const std::string &, Policy);
  };

  /// @brief The image of a Config by a FeatureModel
  struct FeatureDescription
  {
    /// @brief The FeatureValue of each of the FeatureModel.
    std::vector<FeatureValue> values;

    /// @brief Print the FeatureDescription for debug purposes.
    ///
    /// @param output Where to print.
    void printForDebug(FILE * output);

    /// @brief Return a string representing this FeatureDescription
    ///
    /// @return The string representing this FeatureDescription
    std::string toString();
    /// @brief Return a string representing the values of the features
    ///
    /// @return The string representing the values of the features
    std::string featureValues();
  };

  private :

  /// @brief A Feature is a way to describe a part of a Configuration.
  struct Feature
  {
    /// @brief The name of this Feature.
    std::string name;
    /// @brief The feature function that transform a Config into a FeatureValue.
    std::function<FeatureValue(Config &)> func;

    /// @brief Construct a new Feature by parsing a feature name.
    ///
    /// This function will uses the helper class FeatureBank.
    /// @param name The feature name.
    Feature(const std::string & name);
  };

  /// @brief The features composing this FeatureModel.
  std::vector<Feature> features;

  public :

  /// @brief The name of the FeatureModel file.
  std::string filename;

  public :

  /// @brief Tranform a Policy into the corresponding string.
  ///
  /// @param policy The Policy.
  ///
  /// @return The corresponding string.
  static const char * policy2str(Policy policy);

  /// @brief Read and construct a new FeatureModel from a file.
  ///
  /// @param filename The name of the file that constains the FeatureModel.
  FeatureModel(const std::string & filename);
  /// @brief Transofrm a Config into its FeatureDescription.
  ///
  /// @param config The Config to transform.
  ///
  /// @return The FeatureDescription of the Config.
  FeatureDescription & getFeatureDescription(Config & config);
};

#endif
