/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file TransitionMachine.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-07

#ifndef TRANSITIONMACHINE__H
#define TRANSITIONMACHINE__H

#include "Classifier.hpp"

/// @brief The purpose of a TransitionMachine is to predict a sequence of Action, that will transform an initial Config into a terminal Config.
///
/// The terminal Config will contains more information than the initial one, for
/// instance if the initial Config consists of words, the final Config could consist of words + their part of speech tags.\n
/// The TransitionMachine is made of states, linked together by Transition. Every step is associated with a Classifier.\n
/// The predictions are made by classifiers, which at each step give a weight to every possible Action. The most probable Action will then be chosen and the corresponding Transition will be taken.\n
class TransitionMachine
{
  public :

  struct State;

  /// @brief A Transition from one state to another.
  struct Transition
  {
    /// @brief The other end of the Transition.
    std::string dest;
    /// @brief The prefix of the Action name that will trigger the choice of this Transition.
    std::string actionPrefix;

    /// @brief Construct a new Transition.
    ///
    /// @param dest The other end of the Transition.
    /// @param prefix The prefix of the Action name that will trigger the choice of this Transition.
    Transition(const std::string & dest, const std::string & prefix);
  };

  /// @brief A State of the TransitionMachine. Can be seen as a graph node.
  struct State
  {
    /// @brief The name of this State.
    std::string name;
    /// @brief The Classifier used in this State.
    std::string classifier;
    /// @brief All the outgoing Transition from this State.
    std::vector<Transition> transitions;

    /// @brief Construct a new State.
    ///
    /// @param name The name of this State.
    /// @param classifier The Classifier used in this State.
    State(const std::string & name, const std::string & classifier);
  };

  private :

  /// @brief Whether or not the program is in train mode.
  bool trainMode;
  /// @brief Store the Classifier by their name.
  std::map< std::string, std::shared_ptr<Classifier> > str2classifier;
  /// @brief Store the State by their name.
  std::map< std::string, std::shared_ptr<State> > str2state;
  /// @brief Name of the initial State.
  std::string initialState;
  /// @brief Name of the current State.
  std::string currentState;

  public :

  /// @brief The name of this TransitionMachine.
  std::string name;

  public :

  /// @brief Read and construct a new TransitionMachine from a file.
  ///
  /// @param trainMode Whether or not the program is in train mode.
  TransitionMachine(bool trainMode);
  /// @brief Get the current TransitionMachine state.
  ///
  /// @return The current state.
  std::string & getCurrentState();
  /// @brief Get the Classifier of the current State.
  ///
  /// @return The Classifier of the current state.
  Classifier * getCurrentClassifier();
  /// @brief Get the Transition to take given the predicted Action.
  ///
  /// @param action The predicted Action.
  ///
  /// @return The outgoing Transition to take given the predicted Action.
  Transition * getTransition(const std::string & action);
  /// @brief Take the given Transition.
  ///
  /// @param transition The Transition to take.
  void takeTransition(Transition * transition);
  /// @brief Return all the Classifier used by this TransitionMachine.
  ///
  /// @return All the Classifier used by this TransitionMachine.
  std::vector<Classifier*> getClassifiers();
  /// @brief Reset the current state to the initial state
  void reset();
};

#endif
