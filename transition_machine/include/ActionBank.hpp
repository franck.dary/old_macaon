/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file ActionBank.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-06

#ifndef ACTIONBANK__H
#define ACTIONBANK__H

#include "Action.hpp"

/// @brief Retrieve a sequence of BasicAction from an action name.
///
/// The purpose of this class is to parse an action name and use it to construct 
/// the corresponding sequence of BasicAction.\n
/// The caller will then probably use this sequence of BasicAction to create an Action.
class ActionBank
{
  public :

  /// @brief Construct a sequence of BasicAction from an action name.
  ///
  /// The creation is done dynamically, and if the name given is not correct,
  /// the program will abort. 
  /// @param name The name of the action (e.g. 'WRITE 0 POS adj').
  ///
  /// @return The corresponding sequence of BasicAction.
  static std::vector<Action::BasicAction> str2sequence(const std::string & name);
  /// @brief Return the lenght of the link that the action will create. Only used in error analysis.
  ///
  /// For instance the link length for dependency parsing will be the absolute distance between the head and the dependent.
  ///
  /// @param c Current Config
  /// @param action The name of the action that will create the link
  ///
  /// @return The length of the link the action will create, default value 0 if not appliable.
  static int getLinkLength(const Config & c, const std::string & action);

  private :

  /// @brief Write a string into a specific cell of the multi-tapes buffer.
  ///
  /// This is a helper function that helps construct BasicAction.
  /// @param config The Config which buffer will be written into.
  /// @param tapeName The name of the tape of the buffer that will be written into.
  /// @param value The value that will be written.
  /// @param relativeIndex The index of the column that will be written into, relatively to the head of the Config.
  static void simpleBufferWrite(Config & config, const std::string & tapeName, 
    const std::string & value, int relativeIndex);

  /// @brief Whether or not a call to simpleBufferWrite is possible.
  ///
  /// This is a helper function that helps construct BasicAction.
  /// @param config The Config simpleBufferWrite would be called with.
  /// @param tapeName The name of the tape simpleBufferWrite would be called with.
  /// @param relativeIndex The relative index simpleBufferWrite would be called with.
  /// @param checkEmptyness If true, check if cell that will be written to is empty.
  ///
  /// @return Whether or not a call to simpleBufferWrite is possible.
  static bool simpleBufferWriteAppliable(Config & config,
    const std::string & tapeName, int relativeIndex, bool checkEmptyness);

  /// @brief Whether or not the transform rule is appliable.
  ///
  /// This is a helper function that helps construct BasicAction.
  /// @param config The current Config.
  /// @param fromTapeName The name of the tape the rule would be applied on.
  /// @param targetTapeName The name of the tape we will write to.
  /// @param relativeIndex The relative index of the cell of the tape.
  /// @param rule The rule.
  ///
  /// @return Whether or not rule can be applied to the cell of tapeName.
  static bool isRuleAppliable(Config & config,
    const std::string & fromTapeName, const std::string & targetTapeName, int relativeIndex, const std::string & rule);

  /// @brief Apply a transformation rule to a copy of a multi-tapes buffer cell, and write the result in another cell.
  ///
  /// A rule is in the form '\@er\@w' which in this case means 'remove er suffix and add w suffix'
  /// @param config The Config which buffer will be written into.
  /// @param fromTapeName The name of the buffer's tape containing the string to copy and transform.
  /// @param targetTapeName The name of the buffer's tape the result will be written to.
  /// @param rule The rule to apply.
  /// @param relativeIndex The index of the column that will be read and written into, relatively to the head of the Config.
  static void writeRuleResult(Config & config, const std::string & fromTapeName, const std::string & targetTapeName, const std::string & rule, int relativeIndex);

  static void addCharToBuffer(Config & config, const std::string & tapeName, int relativeIndex);

  static void removeCharFromBuffer(Config & config, const std::string & tapeName, int relativeIndex);

  /// \brief Write something on the buffer
  ///
  /// \param tapeName The tape we will write to
  /// \param value The value we will write
  /// \param relativeIndex The write index relative to the buffer's head
  /// \param checkEmptyness If true, check if the cell that will be written to is empty
  ///
  /// \return A BasicAction doing all of that
  static Action::BasicAction bufferWrite(std::string tapeName, std::string value, int relativeIndex, bool checkEmptyness);

  /// \brief Append a string to a cell in the buffer
  ///
  /// \param tapeName The tape we will write to
  /// \param value The value we will append
  /// \param relativeIndex The write index relative to the buffer's head
  ///
  /// \return A BasicAction doing all of that
  static Action::BasicAction bufferAdd(std::string tapeName, std::string value, int relativeIndex);

  /// \brief Modify a cell of the buffer.
  ///
  /// \param tapeName The tape we will write to
  /// \param relativeIndex The write index relative to the buffer's head
  /// \param modification The function that will be applied to the buffer's cell
  ///
  /// \return A BasicAction doing all of that
  static Action::BasicAction bufferApply(std::string tapeName, int relativeIndex, std::function<std::string(std::string)> modification);

  /// \brief Modify a cell of the buffer.
  ///
  /// \param tapeName The tape we will write to
  /// \param relativeIndex The write index relative to the stack's head
  /// \param modification The function that will be applied to the buffer's cell
  ///
  /// \return A BasicAction doing all of that
  static Action::BasicAction stackApply(std::string tapeName, int relativeIndex, std::function<std::string(std::string)> modification);

  /// \brief Append a string to a cell in the stack
  ///
  /// \param tapeName The tape we will write to
  /// \param value The value we will append
  /// \param relativeIndex The write index relative to the stack's head
  ///
  /// \return A BasicAction doing all of that
  static Action::BasicAction stackAdd(std::string tapeName, std::string value, int bufferIndex);

  /// \brief Move the buffer's head
  ///
  /// \param movement The relative movement of the buffer's head
  ///
  /// \return A BasicAction moving the head
  static Action::BasicAction moveHead(int movement);

  /// \brief Move the raw input head
  ///
  /// \param movement The relative movement of the raw input head
  ///
  /// \return A BasicAction moving the head
  static Action::BasicAction moveRawInputHead(int movement);

  /// \brief Verify if rawInput begins with word
  ///
  /// \param word The word to verify
  ///
  /// \return A BasicAction only appliable if word is the prefix of rawInput.
  static Action::BasicAction rawInputBeginsWith(std::string word);

  /// \brief Verify that certain cell is not empty.
  ///
  /// \param tape The tape to test
  /// \param relativeIndex The index on this tape
  ///
  /// \return A BasicAction only appliable if the tape at relativeIndex is not empty.
  static Action::BasicAction checkNotEmpty(std::string tape, int relativeIndex);

  static Action::BasicAction checkNotEndOfTapes();

  /// \brief Verify that the config is not final.
  ///
  /// \return A BasicAction only appliable if the config is not final.
  static Action::BasicAction checkConfigIsNotFinal();

  /// \brief Write something on the buffer
  ///
  /// \param tapeName The tape we will write to
  /// \param value The value we will write
  /// \param stackIndex The stack index of the buffer index
  /// \param checkEmptyness If true, check if the cell it will write to is empty
  ///
  /// \return A BasicAction doing all of that
  static Action::BasicAction stackWrite(std::string tapeName, std::string value, int stackIndex, bool checkEmptyness);

  /// \brief Push the head of the buffer into the stack
  ///
  /// \return The corresponding BasicAction
  static Action::BasicAction pushHead();

  /// \brief Verify that the character pointed by the rawInputHead is a space.
  ///
  /// \return A BasicAction only appliable if the character pointed by the rawInputHead is a space.
  static Action::BasicAction checkRawInputHeadIsSpace();

  /// \brief Verify that the character pointed by the rawInputHead is a separator.
  ///
  /// \return A BasicAction only appliable if the character pointed by the rawInputHead is a separator.
  static Action::BasicAction checkRawInputHeadIsSeparator();

  /// \brief Pop the stack
  ///
  /// \return The corresponding BasicAction
  static Action::BasicAction stackPop(bool checkGov);

  /// \brief Add size cells to all tapes, until there are size cells past head.
  ///
  /// \return The corresponding BasicAction
  static Action::BasicAction increaseTapesIfNeeded(int size);
};

#endif
