/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file FeatureBank.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-07

#ifndef FEATUREBANK__H
#define FEATUREBANK__H

#include <functional>
#include "FeatureModel.hpp"

/// @brief This class is able to create feature functions from a feature name.
///
/// It is used to construct FeatureModel.
class FeatureBank
{
  public :

  /// @brief Dynamically creates a feature function from the name of a feature.
  ///
  /// @param s The name of the feature.
  ///
  /// @return A corresponding feature function.
  static std::function<FeatureModel::FeatureValue(Config &)> str2func(const std::string & s);

  /// @brief Converts a Dict policy to a Feature policy.
  ///
  /// The purpose of the Feature policy is to state that if a Dict is Final, its values will not be updated during the training step.
  /// @param policy The policy of the Dict.
  ///
  /// @return The corresponding Feature policy.
  static FeatureModel::Policy dictPolicy2FeaturePolicy(Dict::Policy policy);

  private :

  /// @brief Get a previous Action in the history of the current state.
  ///
  /// @param config The Config to work with.
  /// @param index The relative index of the Action (e.g. -1 for the Action that came before the last Action performed).
  /// @param featName The name of this feature.
  ///
  /// @return The prefix of the name of the requested Action.
  static FeatureModel::FeatureValue actionHistory(Config & config, int index, const std::string & featName);
  /// @brief Get a previous entropy in the history of the current state.
  ///
  /// @param config The Config to work with.
  /// @param index The relative index of the entropy (e.g. -1 for the entropy of the Action choice that happened before the last Action performed).
  /// @param featName The name of this feature.
  ///
  /// @return The discretized value of the desired entropy.
  static FeatureModel::FeatureValue entropyHistory(Config & config, int index, const std::string & featName);
  /// @brief Get wether or not the given FeatureValue starts with an uppercase.
  ///
  /// @param config The Config to works with.
  /// @param fv The given FeatureValue.
  ///
  /// @return Wether of not the string value of fv starts with an uppercase. "true" or "false".
  static FeatureModel::FeatureValue getUppercase(Config & config, const FeatureModel::FeatureValue & fv);
  /// @brief Get the length of the given FeatureValue.
  ///
  /// @param config The Config to works with.
  /// @param fv The given FeatureValue.
  ///
  /// @return The length of the string value of fv.
  static FeatureModel::FeatureValue getLength(Config & config, const FeatureModel::FeatureValue & fv);
  /// @brief Get a substring of the given FeatureValue.
  ///
  /// @param config The Config to works with.
  /// @param fv The given FeatureValue.
  /// @param from The index from which the substring will begin.
  /// @param to The index the substring will end.
  ///
  /// @return A string composed of all the letters between the two indexes, of the string value of fv.
  static FeatureModel::FeatureValue getLetters(Config & config, const FeatureModel::FeatureValue & fv, int from, int to);
  /// @brief Get all the Config in the window [head-from,head+to] 
  ///
  /// @param c The Config.
  /// @param from The relative index of the left of the window.
  /// @param to The relative index of the right of the window.
  /// @param exceptions List of Tape to be ignored by this function.
  ///
  /// @return An aggregate of Features representing the window.
  static FeatureModel::FeatureValue aggregateBuffer(Config & c, int from, int to, const std::vector<std::string> & exceptions);
  /// @brief Get all the Config in the window stack[top,top-from] 
  ///
  /// @param c The Config.
  /// @param from The relative index of the left of the window.
  /// @param exceptions List of Tape to be ignored by this function.
  ///
  /// @return An aggregate of Features representing the window.
  static FeatureModel::FeatureValue aggregateStack(Config & c, int from, const std::vector<std::string> & exceptions);
  static FeatureModel::FeatureValue fasttext(Config & c, const FeatureModel::FeatureValue & word);
};

#endif
