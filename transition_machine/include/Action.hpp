/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file Action.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-06

#ifndef ACTION__H
#define ACTION__H

#include <vector>
#include <functional>

class Config;

/// @brief An Action is a function that transforms a Config.
///
/// More precisely it is a sequence of BasicAction.\n
/// It is also possible to undo any action that have been applied to a Config.
class Action
{
  public :

  /// @brief A BasicAction is an invertible function that transforms a Config.
  struct BasicAction
  {
    /// @brief How the BasicAction will act on the Config.
    ///
    /// Push means to add an element to the Config stack.\n
    /// Pop means to remove the top element of the Config stack.\n
    /// Write means to write a string to a Config's buffer cell.
    enum Type
    {
      Push,
      Pop,
      Write,
      Back,
      MoveHead
    };

    /// @brief The type of this BasicAction.
    Type type;
    /// @brief Some memory to remember what data have been erased from the Config.
    ///
    /// This is useful when a BasicAction of type Pop needs to be reverted.
    std::string data;

    /// @brief Apply this BasicAction to a Config.
    std::function<void(Config & config, BasicAction & ba)> apply;
    /// @brief Revert the application of this BasicAction to a Config.
    std::function<void(Config & config, BasicAction & ba)> undo;
    /// @brief Check if this BasicAction can be applied to the Config.
    std::function<bool(Config & config, BasicAction & ba)> appliable;
    /// @brief Convert to a string.
    ///
    /// @return A string describing this BasicAction.
    std::string to_string();
  };

  public :

  /// @brief The sequence of BasicAction this Action is made of.
  std::vector<BasicAction> sequence;
  /// @brief The name of this Action.
  std::string name;
  /// @brief The start of the name of this Action.
  ///
  /// This is useful to maintain a history of past actions, keeping only the type of the actions.
  std::string namePrefix;

  /// @brief The name of the machine's current state when this action was performed.
  std::string stateName; 
  
  public :

  /// @brief Check if this instance is equal to other.
  bool operator==(const Action & other) const;
  /// @brief Construct an empty Action.
  Action();
  /// @brief Construct an Action given its name.
  ///
  /// This function will use ActionBank to retrieve the sequence of BasicAction.
  /// @param name The name of this action.
  Action(const std::string & name);
  /// @brief Apply this Action to a Configuration.
  ///
  /// @param config The Configuration to transform.
  void apply(Config & config);
  /// @brief Revert the application of this Action.
  ///
  /// @param config The Configuration to restore.
  void undo(Config & config);
  /// @brief Revert the changes made by this Action on the stack of the Configuration.
  ///
  /// This is useful when doing a backtrack. The stack needs to be restored but the buffer have to keep the values predicted 'in the future'.
  /// @param config The Configuration to restore.
  void undoOnlyStack(Config & config);
  /// @brief Whether or not this action can be applied to the Configuration.
  ///
  /// @param config The Configuration.
  ///
  /// @return Whether or not this action can be applied to the Configuration.
  bool appliable(Config & config);
  /// @brief Print this Action for debug purposes.
  ///
  /// @param output Where to print.
  void printForDebug(FILE * output);
  /// @brief Set informations about this particular Action.
  ///
  /// These informations will be usefull when undoing the Action.
  ///
  /// @param stateName The name of the machine's current state when this action was performed.
  void setInfos(std::string stateName);
};

#endif
