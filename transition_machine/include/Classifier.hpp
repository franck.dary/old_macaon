/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file Classifier.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-06

#ifndef CLASSIFIER__H
#define CLASSIFIER__H

#include <string>
#include <memory>
#include "FeatureModel.hpp"
#include "ActionSet.hpp"
#include "Oracle.hpp"
#include "NeuralNetwork.hpp"
#include "ProgramParameters.hpp"

/// @brief Given a Config, a Classifier is capable of weighting its ActionSet.
class Classifier
{
  public :

  /// @brief A set of Actions with their corresponding weight.
  using WeightedActions = std::vector< std::pair< bool, std::pair<float, std::string> > >;

  /// @brief There are 3 types of Classifier.
  ///
  /// Prediction : Given a Configuration, this Classifier will predict a weight for each element of its ActionSet. This kind of Classifier must be trained beforehand.\n
  /// Information : Given a Configuration, this Classifier will give the correct Action of its ActionSet to apply. This is useful if you want to build a Classifier that is adding some information to your input. For instance add the lemma of each word, from a file containing every pair of word/lemma.\n
  /// Forced : This kind of Classifier will always give the same Action. This can be used to force a certain Action to be applied.
  enum Type
  {
    Prediction,
    Information,
    Forced
  };

  /// @brief The name of this Classifier.
  std::string name;

  private :

  /// @brief The type of this Classifier.
  Type type;
  /// @brief Whether or not the program is in train mode.
  ///
  /// In train mode, the underlying neural network's parameters will be zero-initialized, instead of being read from a file.
  bool trainMode;
  /// @brief The FeatureModel of this Classifier.
  std::shared_ptr<FeatureModel> fm;
  /// @brief The ActionSet of this Classifier.
  std::shared_ptr<ActionSet> as;
  /// @brief The neural network used by this Classifier.
  /// The neural network is only used for Classifier of type Prediction.
  std::shared_ptr<NeuralNetwork> nn;
  /// @brief A string describing the topology of the underlying neural network.
  std::string topology;
  /// @brief The size of a batch. If not specified, use the global batchsize.
  int batchSize;
  /// @brief The oracle being used by this Classifier.
  ///
  /// For Classifier of type Prediction, the Oracle is used in train mode, to find
  /// the correct Action to associate with a training example.\n
  /// For Classifier of type Information, the Oracle is used in train mode and decode mode too, it is simply a deterministic function that gives the correct Action given a Configuration.
  Oracle * oracle;
  /// @brief Is this classifier subject to a dynamic oracle.
  bool dynamic;

  private :

  /// @brief Create the correct type of NeuralNetwork.
  ///
  /// @return A pointer to the newly created NeuralNetwork.
  NeuralNetwork * createNeuralNetwork();

  /// @brief Create the correct type of NeuralNetwork, from a file.
  ///
  /// @param modelFilename The name of the file containing the NeuralNetwork description.
  ///
  /// @return A pointer to the newly created NeuralNetwork.
  NeuralNetwork * createNeuralNetwork(const std::string & modelFilename);

  public :

  /// @brief If the ActionScript has a default action, get it.
  ///
  /// @return The default action name, or an empty string.
  std::string getDefaultAction() const;
  /// @brief Return how many errors will an action introduce.
  ///
  /// @param config The current config.
  /// @param action The action to test.
  ///
  /// @return The number of errors that the action will introduce.
  int getActionCost(Config & config, const std::string & action);
  /// @brief Print the weight that has been given to each Action by this Classifier.
  ///
  /// @param output Where to print.
  /// @param wa The Action and their weight.
  /// @param threshold Print only the n most weighted Action.
  static void printWeightedActions(FILE * output, WeightedActions & wa, int threshold = 5);

  /// @brief Get a measure of confidence.
  ///
  /// @param wa The actions with a score associated for each one.
  ///
  /// @return A measure of confidence.
  static float computeEntropy(WeightedActions & wa);

  /// @brief Convert a string to its corresponding Type.
  ///
  /// If s is unknown, the program aborts.
  /// @param s The string to convert.
  ///
  /// @return The Type corresponding to s.
  static Type str2type(const std::string & s);
  /// @brief Read and construct a new Classifier from a file.
  ///
  /// @param filename The name of the file containing the Classifier.
  /// @param trainMode Whether or not the program is in train mode.
  Classifier(const std::string & filename, bool trainMode);
  /// @brief Give a weight to each Action of the ActionSet, given a Config.
  ///
  /// @param config The Config to take into account.
  ///
  /// @return Pairs of Action / weight.
  WeightedActions weightActions(Config & config);
  /// @brief Use the FeatureModel to get a FeatureDescription of the Config.
  ///
  /// @param config The Config to get the FeatureDescription of.
  ///
  /// @return The FeatureDescription of the Config.
  FeatureModel::FeatureDescription getFeatureDescription(Config & config);
  /// @brief Use the Oracle on the config to get the correct Action to take.
  ///
  /// @param config The Config to work with.
  ///
  /// @return The name of the correct Action to take.
  std::string getOracleAction(Config & config);
  /// @brief Get all the actions that are zero cost given this Config.
  ///
  /// @param config The current Config.
  ///
  /// @return A vector of all the actions that are zero cost given this Config.
  std::vector<std::string> getZeroCostActions(Config & config);
  /// @brief Print an explaination as of why some actions are not zero cost.
  ///
  /// @param output Where to write the explainations.
  /// @param config The current Config.
  void explainCostOfActions(FILE * output, Config & config);
  /// @brief Use the Oracle on the config to get the correct Action to take.
  ///
  /// @param config The Config to work with.
  ///
  /// @return The index of the correct Action to take.
  int getOracleActionIndex(Config & config);
  /// @brief Train the classifier on a training example.
  ///
  /// @param config The Config to work with.
  /// @param gold The gold class of the Config.
  ///
  /// @return The loss.
  float trainOnExample(Config & config, int gold);
  /// @brief Train the classifier on a training example.
  ///
  /// @param config The Config to work with.
  /// @param gold The gold vector for this Config.
  ///
  /// @return The loss.
  float trainOnExample(Config & config, const std::vector<float> & gold);
  /// @brief Train the classifier on a training example.
  ///
  /// @param fd The FeatureDescription to work with.
  /// @param gold The gold class of the FeatureDescription.
  ///
  /// @return The loss.
  float trainOnExample(FeatureModel::FeatureDescription & fd, int gold);
  /// @brief Train the classifier on a training example.
  ///
  /// @param fd The FeatureDescription to work with.
  /// @param gold The gold vector for this FeatureDescription.
  ///
  /// @return The loss.
  float trainOnExample(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold);
  /// @brief Get the loss of the classifier on a training example.
  ///
  /// @param config The Config to work with.
  /// @param gold The gold class of the Config.
  ///
  /// @return The loss.
  float getLoss(Config & config, int gold);
  /// @brief Get the loss of the classifier on a training example.
  ///
  /// @param config The Config to work with.
  /// @param gold The gold vector for this Config.
  ///
  /// @return The loss.
  float getLoss(Config & config, const std::vector<float> & gold);
  /// @brief Get the name of an Action from its index.
  ///
  /// The index of an Action can be seen as the index of the corresponding output neuron in the underlying neural network.
  /// @param actionIndex The index of the Action.
  ///
  /// @return The name of the Action.
  std::string getActionName(int actionIndex);
  /// @brief Get an Action from its name.
  ///
  /// @param name The name of the Action.
  ///
  /// @return A pointer to the Action.
  Action * getAction(const std::string & name);
  /// @brief Initialize the underlying neural network.
  ///
  /// Using the Config and the FeatureModel, the size of the input and output layers of the neural network will be computed. Using the topology string, the hidden layers will be computed.
  /// @param config A Configuration of the TransitionMachine.
  void initClassifier(Config & config);
  /// @brief Save the trained neural network of this Classifier into a file.
  ///
  /// @param modelFilename The name of the file to write to.
  void save(const std::string & modelFilename);
  /// @brief Whether or not this Classifier needs to be trained.
  ///
  /// The only type of Classifier that needs to be tained is Prediction.
  /// @return Whether or not this Classifier needs to be trained.
  bool needsTrain();
  /// @brief Print the topology of the underlying neural network.
  ///
  /// @param output Where to print.
  void printTopology(FILE * output);
  /// @brief Return the index of the Action.
  ///
  /// @param action The action.
  ///
  /// @return The index of the Action.
  int getActionIndex(const std::string & action);
  /// @brief Get the number of actions this classifier knows.
  ///
  /// @return The number of actions.
  unsigned int getNbActions();
  /// @brief Get a pointer to the FeatureModel.
  FeatureModel * getFeatureModel();
  /// @brief Prepare Classifier for next iteration.
  void endOfIteration();
  /// @brief Is this classifier subject to a dynamic oracle.
  bool isDynamic();
};

#endif
