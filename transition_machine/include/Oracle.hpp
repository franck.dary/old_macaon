/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file Oracle.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-06

#ifndef ORACLE__H
#define ORACLE__H

#include <string>
#include <map>
#include <memory>
#include <functional>
#include "Config.hpp"

/// @brief An Oracle is able to give the correct Action to take, given the current Config.
class Oracle
{
  private : 

  /// @brief Maps Oracle names to Oracle objects.
  static std::map< std::string, std::unique_ptr<Oracle> > str2oracle;

  /// @brief Construct a new Oracle.
  ///
  /// @param initialize The function that will be called at the start of the program, to initialize the Oracle.
  /// @param findAction The function that will return the optimal action to take given the Config, for classifiers that do not require training.
  /// @param getCost The function that will return the cost of the action regarding the Config. A non-zero cost means that the action is not optimal.
  Oracle(std::function<void(Oracle *)> initialize,
         std::function<std::string(Config &, Oracle *)> findAction,
         std::function<int(Config &, Oracle *, const std::string &)> getCost);

  private :

  /// @brief Return the cost of an action given the current Config.
  std::function<int(Config &, Oracle *, const std::string &)> getCost;
  /// @brief Return the optimal action to take, only for non-trainable Classifier.
  std::function<std::string(Config &, Oracle *)> findAction;
  /// @brief The function that will be called at the start of the program, to initialize the Oracle.
  std::function<void(Oracle *)> initialize;
  /// @brief Whether or not initialize have been called.
  bool isInit;
  /// @brief A filename.
  ///
  /// An Oracle can have a file. This file can be read during the initialize function to help the Oracle in its future calls to findAction. For instance the file can be a list of pairs of word / lemma, that can help the Oracle predict the lemmatization action.
  std::string filename;
  /// @brief Some data that can help the Oracle.
  ///
  /// The Oracle can fill this data structure in the function initialize, and uses its content in the function findAction.
  std::map<std::string, std::string> data;

  public :

  /// @brief Get an Oracle from its name.
  ///
  /// If the name is unknown, the program aborts.
  /// @param name The name of the Oracle.
  ///
  /// @return A pointer to the Oracle object.
  static Oracle * getOracle(const std::string & name);
  /// @brief Get an Oracle from its name.
  ///
  /// If the name is unknown, the program aborts.
  /// @param name The name of the Oracle.
  /// @param filename The name of the file the oracle can use.
  ///
  /// @return A pointer to the Oracle object.
  static Oracle * getOracle(const std::string & name, std::string filename);
  /// @brief Create all Oracle and store them in the member str2oracle.
  static void createDatabase();

  public :

  /// @brief Tests whether or not the action is optimal for the given Config.
  ///
  /// @param config The current Config.
  /// @param action The action to test.
  ///
  /// @return The cost of the action. zero-cost is optimal.
  int getActionCost(Config & config, const std::string & action);
  /// @brief Explain why an action is zero cost or not.
  ///
  /// @param output Where to write the explaination.
  /// @param config The current Config.
  /// @param action The current Action.
  static void explainCostOfAction(FILE * output, Config & config, const std::string & action);
  /// @brief Get the optimal action given the current Config, only for non-trainable Classifier..
  ///
  /// @param config The current Config.
  ///
  /// @return The optimal action to take given the current Config.
  std::string getAction(Config & config);
  /// @brief Initialize this oracle, by calling initialize.
  void init();
  /// @brief Set the member filename.
  ///
  /// @param filename The name of the file that can be used by the Oracle.
  void setFilename(const std::string & filename);
};

#endif
