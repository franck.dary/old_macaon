/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file Config.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-07

#ifndef CONFIG__H
#define CONFIG__H

#include <vector>
#include "BD.hpp"
#include "File.hpp"
#include "LimitedStack.hpp"
#include "LimitedArray.hpp"

class Action;

/// @brief Configuration of a TransitionMachine.
/// It consists of a multi-tapes buffer, a stack and a head.
class Config
{
  public :

  /// @brief A Tape of the multi-tapes buffer.
  ///
  /// Each cell can contain a string.
  class Tape
  {
    private :

    /// @brief The name of this Tape.
    std::string name;
    /// @brief Whether or not the content of this Tape is known at the start of the program (it is an input).
    bool isKnown;
    /// @brief The head of this Tape, an index.
    int head;
    /// @brief Current entropy of the owning Config.
    float totalEntropy;
    /// @brief Content of the cells of this Tape, that was given as an input to this program.
    LimitedArray<std::string> ref;
    /// @brief Content of the cells of this Tape, which have been predicted by the program so far.
    LimitedArray< std::pair<std::string,float> > hyp;

    public :

    /// @brief Access the value of a cell.
    ///
    /// If isKnown is true, the vector ref will be read, otherwise the vector hyp will be read.
    /// @param relativeIndex Index of the cell to access, relatively to the head.
    ///
    /// @return Value of the cell.
    const std::string & operator[](int relativeIndex);
    /// @brief Access the entropy of a cell.
    ///
    /// If isKnown is true, the vector ref will be read, otherwise the vector hyp will be read.
    /// @param relativeIndex Index of the cell to access, relatively to the head.
    ///
    /// @return Entropy of the cell.
    float getEntropy(int relativeIndex);
    /// @brief Access the value of a cell of the ref.
    ///
    /// @param relativeIndex The index of the cell relatively to the head.
    ///
    /// @return The content of the cell.
    const std::string & getRef(int relativeIndex) const;
    /// @brief Access the value of a cell of the hyp.
    ///
    /// @param relativeIndex The index of the cell relatively to the head.
    ///
    /// @return The content of the cell.
    const std::string & getHyp(int relativeIndex) const;
    /// @brief Set the value of a cell of the hyp.
    ///
    /// @param relativeIndex The index of the cell relatively to the head.
    /// @param elem The new content of the cell.
    void setHyp(int relativeIndex, const std::string & elem);
    /// @brief Set the value of a cell of the ref.
    ///
    /// @param relativeIndex The index of the cell relatively to the head.
    /// @param elem The new content of the cell.
    void setRef(int relativeIndex, const std::string & elem);
    void set(int relativeIndex, const std::string & elem);
    int getHead();
    /// @brief Return true if the head of this tape is on the last cell.
    ///
    /// @return True if the head of this tape is on the last cell.
    bool headIsAtEnd() const;
    /// @brief Must be used before copying another Tape's data into this one, but never used anywhere else.
    void clearDataForCopy();
    /// @brief construct an empty tape. 
    Tape(const std::string & name, bool isKnown);
    /// @brief Get the name of this Tape.
    ///
    /// @return The name of this Tape.
    const std::string & getName();
    void setKnown(bool known);
    bool getKnown();
    /// @brief Move the head of this tape.
    ///
    /// @param mvt The relative movement to apply to the head.
    void moveHead(int mvt);
    /// @brief Return the current size of the Tape, in number of cells.
    ///
    /// @return The current number of cells in this Tape.
    int size();
    /// @brief The size of the underlying container, in number of elements.
    ///
    /// @return The size of the underlying container, in number of elements.
    int dataSize();
    /// @brief Return the current size of the ref Tape, in number of cells.
    ///
    /// @return The current number of cells in this Tape.
    int refSize();
    /// @brief Return the current size of the hyp Tape, in number of cells.
    ///
    /// @return The current number of cells in this Tape.
    int hypSize();
    /// @brief Add cell to ref.
    ///
    /// @param elem The content of the new cell.
    void addToRef(const std::string & elem);
    /// @brief Add cell to hyp.
    ///
    /// @param elem The content of the new cell.
    void addToHyp(const std::string & elem);
    /// @brief Empty the Tape.
    void clear();
    /// @brief Copy a chunk of an other Tape inside this one.
    ///
    /// @param from first cell index of the chunk to copy.
    /// @param to last cell index of the chunk to copy.
    void copyPart(Tape & other, unsigned int from, unsigned int to);
    /// @brief Get the last tape index that will be overriden with the next read.
    int getNextOverridenDataIndex();
    /// @brief Get the last tape index that will be overriden with the next read.
    int getNextOverridenRealIndex();
    void setTotalEntropy(float entropy);
    /// @brief Mask a cell of the tape
    ///
    /// @param index the index to mask
    void maskIndex(int index);
    /// @brief Compare hyp and ref to give a matching score.
    ///
    /// @param from first index to evaluate
    /// @param to last index to evaluate
    ///
    /// @return The score as a percentage.
    float getScore(int from, int to);
  };

  private :

  const unsigned int HISTORY_SIZE = 2000;
  /// @brief True if eos tape have been modified by an action.
  bool eosTouched;
  /// @brief The name of the current state of the TransitionMachine.
  std::string currentStateName;
  /// @brief For each state of the TransitionMachine, an history of the Action that have been applied to this Config.
  std::map< std::string, LimitedStack<std::string> > actionHistory;
  /// @brief For each state of the TransitionMachine, an history of the entropies for each past decisions.
  std::map< std::string, LimitedStack<float> > entropyHistory;
  /// @brief A stack that can contain indexes of the multi-tapes buffer.
  std::vector<int> stack;
  /// @brief The lastest popped element from the stack
  int stackHistory;
  /// @brief The name of the input file used to fill the tapes.
  std::string inputFilename;
  /// @brief The BD that describes the tapes of this Config.
  BD & bd;
  /// @brief The head of this Config. An index of the multi-tapes buffer.
  int head;
  /// @brief The file containing the input.
  std::shared_ptr<File> file;
  /// @brief If the end of input was reached during reading.
  bool inputAllRead;
  /// @brief Where to print the output.
  FILE * outputFile;
  /// @brief The last cell index that have been printed.
  int lastIndexPrinted;
  /// @brief Measure of how much the model is confident in the predictons made in this Config.
  float totalEntropy;
  /// @brief For each cell of the buffer, history of actions that changed it along with their cost.
  std::map< std::string, std::vector< std::pair<std::string, int> > > actionsHistory;

  public :

  /// @brief The tapes of the multi-tapes buffer. They all have the same size.
  std::vector<Tape> tapes;
  /// @brief An history of hashes, can be used to detect loops.
  LimitedStack<std::size_t> hashHistory;
  /// @brief The sequence of Actions that made that Config.
  LimitedStack< std::pair<std::string, Action> > pastActions;
  /// @brief The last action that have been undone.
  std::pair<std::string, int> lastUndoneAction;
  /// @brief The input before tokenization.
  std::string rawInput;
  /// @brief Head of the raw input.
  int rawInputHead;
  /// @brief Index of the rawInputHead in term of bytes.
  int rawInputHeadIndex;
  /// @brief Index of current word in the sentence, as in conll format.
  int currentWordIndex;
  /// @brief The conll input as it was read.
  std::vector< std::vector<std::string> > inputContent;

  public :

  /// @brief Construct a new Config.
  ///
  /// @param bd The BD that describes the tapes of this Config.
  /// @param inputFilename The name of the input file.
  Config(BD & bd, const std::string inputFilename);
  /// @brief Copy constructor.
  ///
  /// @param other The model.
  Config(const Config & other);
  /// @brief Get a Tape by its name.
  ///
  /// @param name The name of the Tape.
  ///
  /// @return Reference to the Tape.
  Tape & getTape(const std::string & name);
  /// @brief Check if a Tape exists.
  ///
  /// @param name The name of the Tape.
  ///
  /// @return Whether or not the Tape exists.
  bool hasTape(const std::string & name);
  /// @brief Get a Tape by its corresponding column in the input (mcf).
  ///
  /// @param col The index of the input column.
  ///
  /// @return The corresponding Tape.
  Tape & getTapeByInputCol(int col);
  /// @brief Read a part of a formated input file (mcf) and use it to fill the tapes.
  void readInput();
  void fillTapesWithInput();
  /// @brief Print the Config for debug purposes.
  ///
  /// @param output Where to print.
  void printForDebug(FILE * output);
  /// @brief Print the tapes as the output of the program.
  ///
  /// @param output Where to print.
  /// @param dataIndex Index of line to print.
  /// @param realIndex Index of line to print.
  /// @param forceRef True to force the output to be the ref tape.
  void printAsOutput(FILE * output, int dataIndex, int realIndex, bool forceRef);
  /// @brief Print the Config without information loss.
  ///
  /// @param output Where to print.
  void printAsExample(FILE * output);
  /// @brief Move the head of this Config.
  ///
  /// @param mvt The relative increment in the position of the head.
  void moveHead(int mvt);
  /// @brief Move the rawInputHead of this Config.
  ///
  /// @param mvt The relative increment in the position of the rawInputHead.
  void moveRawInputHead(int mvt);
  /// @brief Whether or not this Config is terminal.
  ///
  /// A Config is terminal when the head is at the end of the multi-tapes buffer and the stack is empty.
  /// @return Whether or not this Config is terminal.
  bool isFinal();
  /// @brief Reset the Config as it was just after the reading of the input.
  void reset();
  /// @brief Get the name of the current state in the TransitionMachine.
  ///
  /// @return the name of the current state in the TransitionMachine.
  std::string & getCurrentStateName();
  /// @brief Set the name of the current state in the TransitionMachine.
  ///
  /// This function does not change the current state in the TransitionMachine.\n
  /// But it has to be called everytime the TransitionMachine's current state change, in order to notify the Config of this change.
  /// @param name The name of the current state in the TransitionMachine.
  void setCurrentStateName(const std::string & name);
  /// @brief Get the history of Action of the current state in the TransitionMachine.
  ///
  /// @return The history of Action of the current state in the TransitionMachine.
  LimitedStack<std::string> & getCurrentStateHistory();
  /// @brief Get the history of a state in the TransitionMachine.
  ///
  /// @param The name of the state.
  ///
  /// @return The history of Action for this particular state in the TransitionMachine.
  LimitedStack<std::string> & getStateHistory(const std::string & state);
  /// @brief Get the history of entropies of the current state in the TransitionMachine.
  ///
  /// @return The history of entropies of the current state in the TransitionMachine.
  LimitedStack<float> & getCurrentStateEntropyHistory();
  /// @brief Shuffle the Config per sequences.
  void shuffle();
  /// @brief Get element from the stack at depth index.
  ///
  /// @param index The depth of the requested element.
  ///
  /// @return  The requested element.
  int stackGetElem(int index) const;
  /// @brief Return true if the stack has an element of depth index.
  ///
  /// @param index The depth of the requested element.
  ///
  /// @return True if the stack has an element of depth index.
  bool stackHasIndex(int index) const;
  /// @brief Return true if the stack is empty.
  ///
  /// @return True if the stack is empty.
  bool stackEmpty() const;
  /// @brief Pop the stack.
  void stackPop();
  /// @brief Push elem to the stack.
  ///
  /// @param elem The element to push.
  void stackPush(int elem);
  /// @brief Get the top element of the stack.
  ///
  /// @return  The top element of the stack.
  int stackTop();
  /// @brief Return the number of elements in the stack.
  ///
  /// @return The number of elements in the stack.
  int stackSize() const;
  /// @brief Load a Config to match the one that has been written to file,
  /// formated by printAsExample.
  ///
  /// @param file The File to read from.
  void loadFromFile(File & file);
  /// @brief Add the entropy to the entropyHistory.
  ///
  /// @param entropy The entropy value.
  void addToEntropyHistory(float entropy);
  // //////////////////////////////////////////////////////////////////////////
  /// \brief Compute a hash of this Config.
  ///
  /// \return The computed hash.
  // 
  ////////////////////////////////////////////////////////////////////////////
  std::size_t computeHash();
  // //////////////////////////////////////////////////////////////////////////
  /// \brief Compute and add current hash to the history of hash.
  // 
  ////////////////////////////////////////////////////////////////////////////
  void addHashToHistory();
  /// @brief Get the Dict of a Line from its index.
  ///
  /// @param num The index of the Line.
  ///
  /// @return The Dict of the Line.
  Dict * getDictOfLine(int num);
  /// @brief Get the Dict of a Line from its name.
  ///
  /// @param name The name of the Line.
  ///
  /// @return The Dict of the Line.
  Dict * getDictOfLine(const std::string & name);
  /// @brief Get the value of the head.
  ///
  /// @return The head of the multi-tapes buffer.
  int getHead() const;
  /// @brief set eosTouched to true.
  void setEosTouched();
  /// @brief Return true if the head is at the end of the tapes.
  ///
  /// @return True if the head is at the end of the tapes.
  bool endOfTapes() const;
  /// @brief Set the output file.
  void setOutputFile(FILE * outputFile);
  /// @brief Print the cells that have not been printed.
  void printTheRest(bool forceRef);
  void setEntropy(float entropy);
  float getEntropy() const;
  void addToEntropy(float entropy);
  /// @brief Print a column content for debug purpose.
  ///
  /// @param index Index of the column to print.
  void printColumnInfos(unsigned int index);
  void addToActionsHistory(std::string & state, const std::string & action, int cost);
  std::vector< std::pair<std::string, int> > & getActionsHistory(std::string & state);
  void transformSymbol(const std::string & from, const std::string & to);
  void setLastIndexPrinted(int lastIndexPrinted);
  /// @brief Transform the tape GOV from relative indexes to UD format.
  void setGovsAsUD(bool ref);
  /// @brief Update the IDs in the last predicted sequence.
  void updateIdsInSequence();
  bool rawInputOnlySeparatorsLeft() const;
};

#endif
