/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
/// @file ActionSet.hpp
/// @author Franck Dary
/// @version 1.0
/// @date 2018-08-06

#ifndef ACTIONSET__H
#define ACTIONSET__H

#include <vector>
#include <map>
#include "Action.hpp"

/// @brief A set of Action.
///
/// Each Classifier has an ActionSet, it represents the Action that the Classifier is able to predict.
class ActionSet
{
  public :

  /// @brief The name of this ActionSet.
  std::string name;
  /// @brief The Action contaiend by this ActionSet.
  std::vector<Action> actions;
  /// @brief Map action's name to their index inside the vector actions.
  std::map<std::string, int> str2index;
  /// @brief Whether or not this ActionSet is dynamic.
  ///
  /// When an ActionSet is dynamic, new Action can be added to it at any time.\n
  /// A Classifier of type Prediction cannot have a dynamic ActionSet.
  bool isDynamic;
  /// @brief Whether or not this ActionSet have a default action.
  ///
  /// If it is the case, the default action is the first element of the vector actions.\n When getAction is called with an unknown name, the default action is returned.\n If an ActionSet is dynamic, it cannot have a default action.
  bool hasDefaultAction;
  /// @brief Index of the default action, if it has one.
  int defaultActionIndex;

  public :

  /// @brief Construct a new ActionSet from a file.
  ///
  /// @param filename The name of the file containing the ActionSet.
  /// @param isDynamic Whether or not this ActionSet is dynamic.
  ActionSet(const std::string & filename, bool isDynamic);
  /// @brief Print this ActionSet for debug purposes.
  ///
  /// @param output Where to print.
  void printForDebug(FILE * output);
  /// @brief Get the index of an action given its name.
  ///
  /// This is useful to map each action to an integer, this integer will be the index of the corresponding output neuron of the neural network of the Classifier.
  /// @param name The name of the Action.
  ///
  /// @return The index of the Action in the vector actions.
  int getActionIndex(const std::string & name);
  /// @brief Get the name of the Action given its index.
  ///
  /// This is useful to map each action to an integer, this integer will be the index of the corresponding output neuron of the neural network of the Classifier.
  /// @param actionIndex The index of the Action in the vector actions.
  ///
  /// @return The name of the Action.
  std::string getActionName(int actionIndex);
  /// @brief Get a pointer to the Action object given its name.
  ///
  /// @param name The name of the Action.
  ///
  /// @return A pointer to the Action.
  Action * getAction(const std::string & name);
  /// @brief Get a pointer to the default Action object.
  ///
  /// @return A pointer to the default Action.
  Action * getDefaultAction();
  /// @brief Get the number of actions contained in this set.
  ///
  /// @return The number of actions in this set.
  unsigned int size();
};

#endif
