/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "ActionSet.hpp"
#include "File.hpp"
#include "util.hpp"

ActionSet::ActionSet(const std::string & filename, bool isDynamic)
{
  this->isDynamic = isDynamic;
  hasDefaultAction = false;

  if(!isDynamic)
  {
    File file(filename, "r");
    FILE * fd = file.getDescriptor();

    char buffer[1024];

    while (true)
    {
      if (fscanf(fd, "Default : %[^\n]\n", buffer) == 1)
      {
        if (hasDefaultAction)
        {
          fprintf(stderr, "ERROR (%s) : \'%s\' has more than 1 default action. Aborting.\n", ERRINFO, filename.c_str());
          exit(1);
        }

        str2index[buffer] = actions.size();
        actions.emplace_back(buffer);
        hasDefaultAction = true;
        defaultActionIndex = actions.size()-1;
        continue;
      }

      if (fscanf(fd, "%[^\n]\n", buffer) == 1)
      {
        str2index[buffer] = actions.size();
        actions.emplace_back(buffer);
        continue;
      }

      break;
    }

    this->name = util::getFilenameFromPath(filename);
  }
  else
  {
    this->name = filename;
  }
}

void ActionSet::printForDebug(FILE * output)
{
  fprintf(output, "ActionSet %s :\n", name.c_str());

  for(auto & action : actions)
    action.printForDebug(output);
}

int ActionSet::getActionIndex(const std::string & name)
{
  auto it = str2index.find(name);

  if(it != str2index.end())
    return it->second;

  if(!isDynamic)
  {
    fprintf(stderr, "ERROR (%s) : unknown action \'%s\'. Aborting.\n", ERRINFO, name.c_str());
    printForDebug(stderr);

    exit(1);
  }

  str2index[name] = actions.size();
  actions.emplace_back(name);

  return str2index[name];
}

std::string ActionSet::getActionName(int actionIndex)
{
  if(actionIndex >= 0 && actionIndex < (int)actions.size())
  {
    return actions[actionIndex].name;
  }

  fprintf(stderr, "ERROR (%s) : invalid action index \'%d\'. Aborting.\n", ERRINFO, actionIndex);

  exit(1);

  return "";
}

Action * ActionSet::getAction(const std::string & name)
{
  return &actions[getActionIndex(name)];
}

Action * ActionSet::getDefaultAction()
{
  if (!hasDefaultAction)
  {
    fprintf(stderr, "ERROR (%s) : requested default action in ActionSet \'%s\'. Aborting.\n", ERRINFO, name.c_str());
    exit(1);
  }

  return &actions[defaultActionIndex];
}

unsigned int ActionSet::size()
{
  return actions.size();
}

