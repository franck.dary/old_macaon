/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "FeatureModel.hpp"
#include "FeatureBank.hpp"
#include "File.hpp"

FeatureModel::Feature::Feature(const std::string & name)
{
  this->name = name;
  this->func = FeatureBank::str2func(name);
}

FeatureModel::FeatureDescription & FeatureModel::getFeatureDescription(Config & config)
{
  static FeatureDescription featureDescription;
  featureDescription.values.clear();

  for(auto & feature : features)
    featureDescription.values.emplace_back(feature.func(config));

  return featureDescription;
}

FeatureModel::FeatureModel(const std::string & filename)
{
  this->filename = util::getFilenameFromPath(filename);

  File file(filename, "r");
  FILE * fd = file.getDescriptor();
  char buffer[1024];

  while (fscanf(fd, "%[^\n]\n", buffer) == 1)
  {
    std::string featureName(buffer);
    if(featureName.empty() || featureName[0] == '#' || featureName[0] == '/' ||
       featureName[0] == '%')
      continue;

    features.emplace_back(featureName);
  }
}

void FeatureModel::FeatureDescription::printForDebug(FILE * output)
{
  fprintf(output, "%s", toString().c_str());
}

std::string FeatureModel::FeatureValue::toString(unsigned int i)
{
  std::string result;

  if (i >= dicts.size())
  {
    fprintf(stderr, "ERROR (%s) : Requested feature index %u out of a feature aggregate of size %lu. Aborting.\n", ERRINFO, i, dicts.size());
    exit(1);
  }

  Dict * dict = dicts[i];
  unsigned int index = dict->getValue(values[i]);
  float * realVector = (*dict->getLookupParameter().values())[index].batch_ptr(0);

  unsigned int dim = dict->getDimension();

  for (unsigned int j = 0; j < dim; j++)
    result += " " + util::float2str(realVector[j], "%5.2f");

  return result;
}

std::string FeatureModel::FeatureDescription::toString()
{
  std::string res;

  std::vector<int> columnSizes = {0,0,0,0};

  for(auto featValue : values)
    for (unsigned int i = 0; i < featValue.dicts.size(); i++)
    {
      int size1 = util::lengthPrinted(featValue.names[i])+15;
      int size2 = util::lengthPrinted("  " + std::string(featValue.values[i]));
      int size3 = util::lengthPrinted("  " + featValue.toString(i));

      columnSizes[0] = std::max(columnSizes[0], size1);
      columnSizes[1] = std::max(columnSizes[1], size2);
      columnSizes[2] = std::max(columnSizes[2], size3);
    }

  int totalLength = 0;
  for (auto n : columnSizes)
    totalLength += n;

  std::string column;
  while ((int)util::lengthPrinted(column) < totalLength){column.push_back('-');}
  column += "\n";
  res += column;

  for(auto featValue : values)
    for (unsigned int i = 0; i < featValue.dicts.size(); i++)
    {
      column = "FeatureValue : " + featValue.names[i];
      while ((int)util::lengthPrinted(column) < columnSizes[0]){column.push_back(' ');}
      res += column;
      column = "  " + std::string(featValue.values[i]);
      while ((int)util::lengthPrinted(column) < columnSizes[1]){column.push_back(' ');}
      res += column;
      column = "  " + featValue.toString(i);
      res += column;
      res.push_back('\n');
    }

  column.clear();
  while ((int)util::lengthPrinted(column) < totalLength){column.push_back('-');}
  column += "\n";
  res += column;

  return res;
}

const char * FeatureModel::policy2str(Policy policy)
{
  if(policy == Policy::Final)
    return "Final";
  else if (policy == Policy::Modifiable)
    return "Modifiable";

  return "null";
}

FeatureModel::FeatureValue::FeatureValue(Dict * dict, const std::string & name, const std::string & value, Policy policy)
{
  this->func = Function::Concat;
  dicts.emplace_back(dict);
  names.emplace_back(name);
  values.emplace_back(value);
  policies.emplace_back(policy);
}

FeatureModel::FeatureValue::FeatureValue(Function func)
{
  this->func = func;
}

std::string FeatureModel::FeatureDescription::featureValues()
{
  std::string res;

  for (auto & feature : values)
    for (unsigned int i = 0; i < feature.values.size(); i++)
      res += feature.values[i] + (i == feature.values.size()-1 ? "\t" : " ");

  if (!res.empty())
    res.pop_back();

  return res;
}

