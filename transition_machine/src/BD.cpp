/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "BD.hpp"
#include "File.hpp"
#include "util.hpp"

BD::Line::Line(int num, int outputIndex, std::string name, std::string dictName,
               int inputColumn, bool mustPrint, bool isKnown)
{
  this->outputIndex = outputIndex;
  this->dict = dictName;
  this->num = num;
  this->name = name;
  this->inputColumn = inputColumn;
  this->mustPrint = mustPrint;
  this->isKnown = isKnown;
}

BD::BD(const std::string & BDfilename, const std::string & MCDfilename)
{
  char buffer[1024];
  char name[1024];
  char refHyp[1024];
  char dict[1024];
  char policy[1024];
  int  mustPrint;

  File mcd(MCDfilename, "r");
  FILE * fd = mcd.getDescriptor();
  std::map<int, std::string> mcdCol2Str;
  std::map<std::string, int> mcdStr2Col;

  while(fscanf(fd, "%[^\n]\n", buffer) == 1)
  {
    if(buffer[0] == '#')
      continue;

    int col;

    if(sscanf(buffer, "%d %s", &col, name) != 2)
    {
      fprintf(stderr, "ERROR (%s) : \'%s\' is not a valid MCD line. Aborting.\n", ERRINFO, buffer);
      exit(1);
    }

    mcdCol2Str[col] = name;
    mcdStr2Col[name] = col;
  }

  File bd(BDfilename, "r");
  fd = bd.getDescriptor();

  while(fscanf(fd, "%[^\n]\n", buffer) == 1)
  {
    if(buffer[0] == '#')
      continue;

    int outputIndex;

    if(sscanf(buffer, "%d %s %s %s %s %d", &outputIndex, name, refHyp, dict, policy, &mustPrint) != 6)
    {
      fprintf(stderr, "ERROR (%s) : \'%s\' is not a valid BD line. Aborting.\n", ERRINFO, buffer);
      exit(1);
    }

    if(util::noAccentLower(refHyp) != std::string("ref") && util::noAccentLower(refHyp) != std::string("hyp"))
    {
      fprintf(stderr, "ERROR (%s) : \'%s\' is not a valid BD line argument. Aborting.\n", ERRINFO, refHyp);
      exit(1);
    }

    bool known = util::noAccentLower(refHyp) == std::string("ref");

    int inputColumn = mcdStr2Col.find(name) == mcdStr2Col.end() ? -1 : mcdStr2Col[name];

    lines.emplace_back(new Line(lines.size(), outputIndex, name, dict, inputColumn, mustPrint == 1, known));
    Line * line = lines.back().get();
    num2line.emplace(line->num, line);
    name2line.emplace(line->name, line);
    col2line.emplace(line->inputColumn, line);
  }
}

Dict * BD::getDictOfLine(int num)
{
  auto it = num2line.find(num);

  if(it == num2line.end())
  {
    fprintf(stderr, "ERROR (%s) : requesting line number %d in BD. Aborting.\n", ERRINFO, num);
    exit(1);
  }

  return Dict::getDict(it->second->dict);
}

Dict * BD::getDictOfLine(const std::string & name)
{
  auto it = name2line.find(name);

  if(it == name2line.end())
  {
    fprintf(stderr, "ERROR (%s) : requesting line \'%s\' in BD. Aborting.\n", ERRINFO, name.c_str());
    exit(1);
  }

  return Dict::getDict(it->second->dict);
}

Dict * BD::getDictOfInputCol(int col)
{
  auto it = col2line.find(col);

  if(it == col2line.end())
  {
    fprintf(stderr, "ERROR (%s) : requesting line of input column %d in BD. Aborting.\n", ERRINFO, col);
    exit(1);
  }

  return Dict::getDict(it->second->dict);
}


bool BD::hasLineOfName(const std::string & name)
{
  return name2line.find(name) != name2line.end();
}

int BD::getLineOfName(const std::string & name)
{
  auto it = name2line.find(name);

  if(it == name2line.end())
  {
    fprintf(stderr, "ERROR (%s) : requesting line %s in BD. Aborting.\n", ERRINFO, name.c_str());
    exit(1);
  }

  return it->second->num;
}

int BD::getLineOfInputCol(int col)
{
  auto it = col2line.find(col);

  if(it == col2line.end())
  {
    fprintf(stderr, "ERROR (%s) : requesting line in BD corresponding to input col %d. Aborting.\n", ERRINFO, col);
    exit(1);
  }

  return it->second->num;
}

bool BD::hasLineOfInputCol(int col)
{
  return col2line.find(col) != col2line.end();
}

int BD::getNbLines()
{
  return lines.size();
}

bool BD::mustPrintLine(int index)
{
  auto it = num2line.find(index);

  if(it == num2line.end())
  {
    fprintf(stderr, "ERROR (%s) : requesting line number %d in BD. Aborting.\n", ERRINFO, index);
    exit(1);
  }

  return it->second->mustPrint;
}

const std::string & BD::getNameOfLine(int line)
{
  return lines[line]->name;
}

bool BD::lineIsKnown(int line)
{
  return lines[line]->isKnown;
}

int BD::getOutputIndexOfLine(int index)
{
  return lines[index]->outputIndex;
}

