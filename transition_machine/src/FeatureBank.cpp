/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "FeatureBank.hpp"
#include "util.hpp"
#include "ProgramParameters.hpp"

int getBufferIndex(Config & c, char object, int relativeIndex)
{
  if (object == 'b')
  {
    int result =  c.getHead() + relativeIndex;
    if (result < 0)
      return -1;
    return result;
  }

  if (!c.stackHasIndex(relativeIndex))
    return -1;
  return c.stackGetElem(relativeIndex);
}

FeatureModel::FeatureValue access(Config & config, int bufferIndex, const std::string & tapeName, const std::string & featName)
{
  auto & tape = config.getTape(tapeName);
  Dict * dict = config.getDictOfLine(tapeName);
  auto policy = FeatureBank::dictPolicy2FeaturePolicy(dict->policy);

  if(bufferIndex < 0 || bufferIndex >= tape.hypSize())
    return {dict, featName, Dict::nullValueStr, policy};

  if(tape[bufferIndex-config.getHead()].empty())
    return {dict, featName, Dict::nullValueStr, policy};

  return {dict, featName, tape[bufferIndex-config.getHead()], policy};
}

int getGov(Config & config, int index)
{
  auto & govs = config.getTape("GOV");

  if(index < 0 || index >= govs.hypSize())
    return -1;

  std::string value = govs[index-config.getHead()];

  try
  {
    return index + std::stoi(value);
  }
  catch (std::exception &)
  {
    return -1;
  }
}

int getLeftMostDep(Config & config, int index)
{
  auto & govs = config.getTape("GOV");
  auto & eos = config.getTape(ProgramParameters::sequenceDelimiterTape);
  int b0 = config.getHead();

  if(index < 0 || index >= govs.hypSize())
    return -1;

  int candidate = -1;
  unsigned int maxDist = 50;

  for (int i = std::max<int>(index-1, 0); index - i <= (int)maxDist && i >= 0 && eos[i-b0] != ProgramParameters::sequenceDelimiter; i--)
  {
    int dist = index - i;
    if(govs[i-b0] == std::to_string(dist))
      candidate = i;
  }

  return candidate;
}

FeatureModel::FeatureValue getNbLeftDep(Config & config, int index, const std::string & featName)
{
  auto & govs = config.getTape("GOV");
  auto & eos = config.getTape(ProgramParameters::sequenceDelimiterTape);
  int b0 = config.getHead();

  Dict * dict = Dict::getDict("int");
  auto policy = FeatureBank::dictPolicy2FeaturePolicy(dict->policy);

  if(index < 0 || index >= govs.hypSize())
    return {dict, featName, Dict::nullValueStr, policy};

  int nb = 0;
  unsigned int maxDist = 50;

  for (int i = std::max<int>(index-1, 0); index - i <= (int)maxDist && i >= 0 && eos[i-b0] != ProgramParameters::sequenceDelimiter; i--)
  {
    int dist = index - i;
    if(govs[i-b0] == std::to_string(dist))
	  nb++;
  }

  return {dict, featName, std::to_string(nb), policy};
}

FeatureModel::FeatureValue getNbRightDep(Config & config, int index, const std::string & featName)
{
  auto & govs = config.getTape("GOV");
  auto & eos = config.getTape(ProgramParameters::sequenceDelimiterTape);
  int b0 = config.getHead();

  Dict * dict = Dict::getDict("int");
  auto policy = FeatureBank::dictPolicy2FeaturePolicy(dict->policy);

  if(index < 0 || index >= govs.hypSize())
    return {dict, featName, Dict::nullValueStr, policy};

  int nb = 0;
  unsigned int maxDist = 50;

  for (int i = index+1; i - index <= (int)maxDist && i < eos.hypSize() && eos[i-1-b0] != ProgramParameters::sequenceDelimiter; i++)
  {
    int dist = index - i;
    if(govs[i-b0] == std::to_string(dist))
	  nb++;
  }

  return {dict, featName, std::to_string(nb), policy};
}

int getSecondLeftMostDep(Config & config, int index)
{
  auto & govs = config.getTape("GOV");
  auto & eos = config.getTape(ProgramParameters::sequenceDelimiterTape);
  int b0 = config.getHead();

  if(index < 0 || index >= govs.hypSize())
    return -1;

  int candidate1 = -1;
  int candidate2 = -1;
  unsigned int maxDist = 50;

  for (int i = std::max<int>(index-1, 0); index - i <= (int)maxDist && i >= 0 && eos[i-b0] != ProgramParameters::sequenceDelimiter; i--)
  {
    int dist = index - i;
    if(govs[i-b0] == std::to_string(dist))
    {
      candidate2 = candidate1;
      candidate1 = i;
    }
  }

  return candidate2;
}

int getRightMostDep(Config & config, int index)
{
  auto & govs = config.getTape("GOV");
  auto & eos = config.getTape(ProgramParameters::sequenceDelimiterTape);
  int b0 = config.getHead();

  if(index < 0 || index >= govs.hypSize())
    return -1;

  int candidate = -1;
  unsigned int maxDist = 50;

  for (int i = index+1; i - index <= (int)maxDist && i < eos.hypSize() && eos[i-1-b0] != ProgramParameters::sequenceDelimiter; i++)
  {
    int dist = index - i;
    if(govs[i-b0] == std::to_string(dist))
      candidate = i;
  }

  return candidate;
}


int getSecondRightMostDep(Config & config, int index)
{
  auto & govs = config.getTape("GOV");
  auto & eos = config.getTape(ProgramParameters::sequenceDelimiterTape);
  int b0 = config.getHead();

  if(index < 0 || index >= govs.hypSize())
    return -1;

  int candidate1 = -1;
  int candidate2 = -1;
  unsigned int maxDist = 50;

  for (int i = index+1; i - index <= (int)maxDist && i < eos.hypSize() && eos[i-1-b0] != ProgramParameters::sequenceDelimiter; i++)
  {
    int dist = index - i;
    if(govs[i-b0] == std::to_string(dist))
    {
      candidate2 = candidate1;
      candidate1 = i;
    }
  }

  return candidate2;
}

FeatureModel::FeatureValue getDistance(int index1, int index2, const std::string & featName)
{
  Dict * dict = Dict::getDict("int");
  auto policy = FeatureBank::dictPolicy2FeaturePolicy(dict->policy);

  if(index1 < 0 || index2 < 0)
    return {dict, featName, Dict::nullValueStr, policy};

  return {dict, featName, std::to_string(index1-index2), policy};
}


std::function<FeatureModel::FeatureValue(Config &)> FeatureBank::str2func(const std::string & s)
{
  if (util::split(s,'.')[0] == "raw")
  {
    int relativeIndex;
    try {relativeIndex = std::stoi(util::split(s, '.')[1]);}
    catch (std::exception &)
    {
      fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\'. Relative index must be an integer. Aborting.\n", ERRINFO, s.c_str());
      exit(1);
    }
      return [relativeIndex, s](Config & c)
      {
        int relativeCharIndex = util::getStartIndexOfNthSymbolFrom(c.rawInput.begin()+c.rawInputHeadIndex, relativeIndex >= 0 ? c.rawInput.end() : c.rawInput.begin(), relativeIndex);

        Dict * dict = Dict::getDict("letters");
        auto policy = dictPolicy2FeaturePolicy(dict->policy);

        if (relativeCharIndex >= 0 && relativeIndex < 0)
          return FeatureModel::FeatureValue({dict, s, Dict::nullValueStr, policy});
        if (relativeCharIndex < 0 && relativeIndex >= 0)
          return FeatureModel::FeatureValue({dict, s, Dict::nullValueStr, policy});

        int endIndex = util::getEndIndexOfNthSymbolFrom(c.rawInput.begin()+c.rawInputHeadIndex+relativeCharIndex, c.rawInput.end(), 0);

        auto a = c.rawInput.begin()+c.rawInputHeadIndex+relativeCharIndex;
        auto b = a + endIndex + 1;

        std::string value;

        if (a <= b)
          value = std::string(a,b);
        else
          value = std::string(b,a);

        if (value.empty())
          return FeatureModel::FeatureValue({dict, s, Dict::nullValueStr, policy});

        return FeatureModel::FeatureValue({dict, s, value, policy});
      };
  }

  auto splited = util::split(s, '#');

  if (splited.size() == 1)
  {
    splited = util::split(splited[0], '.');

    int index = -1;
    try {index = std::stoi(splited[1]);} catch(std::exception &)
    {
      fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\'. Relative index must be an integer. Aborting.\n", ERRINFO, s.c_str());
      exit(1);
    }

    if (splited[0] == "tc")
    {
      return [index](Config & c){return actionHistory(c, index, "tc."+std::to_string(index));};
    }
    else if (splited[0] == "entropy")
    {
      return [index](Config & c){return entropyHistory(c, index, "entropy."+std::to_string(index));};
    }
    else
    {
      fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\'. Aborting.\n", ERRINFO, s.c_str());
      exit(1);
    }
  }
  else if (splited.size() != 2)
  {
    fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\'. Aborting.\n", ERRINFO, s.c_str());
    exit(1);
  }

  auto target = splited[0];
  auto feature = splited[1];

  if (target.size() < 3)
  {
    fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\'. Aborting.\n", ERRINFO, s.c_str());
    exit(1);
  }

  splited = util::split(target, '.');
  if (splited.size() < 2)
  {
    fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\'. Aborting.\n", ERRINFO, s.c_str());
    exit(1);
  }

  if (splited[0] != "b" && splited[0] != "s")
  {
    fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\'. Aborting.\n", ERRINFO, s.c_str());
    exit(1);
  }

  int relativeIndex = 0;
  try {relativeIndex = std::stoi(splited[1]);} catch(std::exception &)
  {
    fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\'. Relative index must be an integer. Aborting.\n", ERRINFO, s.c_str());
    exit(1);
  }

  if (splited[0] == "s" && relativeIndex < 0)
  {
    fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\', relative index < 0 with object s. Aborting.\n", ERRINFO, s.c_str());
    exit(1);
  }

  std::vector<std::function<int(Config&, int)> > contextFuncs;
  char object = splited[0][0];
  contextFuncs.emplace_back([relativeIndex, object](Config & c, int){return getBufferIndex(c, object, relativeIndex);});
  for (unsigned int i = 2; i < splited.size(); i++)
  {
    if (splited[i] == "ldep")
      contextFuncs.emplace_back([](Config & c, int currentIndex){return getLeftMostDep(c, currentIndex);});
    else if (splited[i] == "rdep")
      contextFuncs.emplace_back([](Config & c, int currentIndex){return getRightMostDep(c, currentIndex);});
	else if (splited[i] == "l2dep")
      contextFuncs.emplace_back([](Config & c, int currentIndex){return getSecondLeftMostDep(c, currentIndex);});
    else if (splited[i] == "r2dep")
      contextFuncs.emplace_back([](Config & c, int currentIndex){return getSecondRightMostDep(c, currentIndex);});
    else if (splited[i] == "gov")
      contextFuncs.emplace_back([](Config & c, int currentIndex){return getGov(c, currentIndex);});
    else
    {
      fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\', unknown context \'%s\'. Aborting.\n", ERRINFO, s.c_str(), splited[i].c_str());
      exit(1);
    }
  }

  auto getContext = [contextFuncs](Config & c)
    {
      int context = 0;
      for (auto contextFunc : contextFuncs) 
        context = contextFunc(c, context);
      return context;
    };

  splited = util::split(feature, '.');

  feature = splited[0];

  if (feature == "DIST")
  {
    if (splited.size() != 3)
    {
      fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\', \'DIST\' must have two arguments. Aborting.\n", ERRINFO, s.c_str());
      exit(1);
    }
    char object = splited[1][0];
    int relativeIndex = std::stoi(splited[2]);

	return [getContext, relativeIndex, object, s](Config & c)
	{
	  return getDistance(getContext(c), getBufferIndex(c, object, relativeIndex), s);
	};
  }
  else if (feature == "nbl")
  {
	return [getContext, s](Config & c)
	{
	  return getNbLeftDep(c, getContext(c), s);
	};
  }
  else if (feature == "nbr")
  {
	return [getContext, s](Config & c)
	{
	  return getNbRightDep(c, getContext(c), s);
	};
  }

  auto featureValue = [getContext, feature, s](Config & c)
    {
      int bufferIndex = getContext(c);
      return access(c, bufferIndex, feature, s);
    };

  if (splited.size() == 1)
    return featureValue;

  if (splited[1] == "U")
    return [featureValue](Config & c){return getUppercase(c, featureValue(c));};
  if (splited[1] == "LEN")
    return [featureValue](Config & c){return getLength(c, featureValue(c));};
  if (splited[1] == "fasttext")
    return [featureValue](Config & c){return fasttext(c, featureValue(c));};
  if (splited[1] == "PART")
  {
    if (splited.size() != 4)
    {
      fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\', expected 2 arguments for \'PART\'. Aborting.\n", ERRINFO, s.c_str());
      exit(1);
    }

    try
    {
      int from = std::stoi(splited[2]);
      int to = std::stoi(splited[3]);
      return [featureValue, from, to](Config & c){return getLetters(c, featureValue(c), from, to);};
    } catch (std::exception &)
    {
      fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\', arguments of \'PART\' must be integers. Aborting.\n", ERRINFO, s.c_str());
      exit(1);
    }
  }

  fprintf(stderr, "ERROR (%s) : invalid feature format \'%s\'. Aborting.\n", ERRINFO, s.c_str());
  exit(1);

  return featureValue;
}

FeatureModel::FeatureValue FeatureBank::actionHistory(Config & config, int index, const std::string & featName)
{
  Dict * dict = Dict::getDict("actions");
  auto policy = dictPolicy2FeaturePolicy(dict->policy);
  auto & history = config.getCurrentStateHistory();

  if(index < 0 || index >= (int)history.size())
    return {dict, featName, Dict::nullValueStr, policy};

  return {dict, featName, history.getElem(index), policy};
}

FeatureModel::FeatureValue FeatureBank::entropyHistory(Config & config, int index, const std::string & featName)
{
  Dict * dict = Dict::getDict("entropy");
  auto policy = dictPolicy2FeaturePolicy(dict->policy);
  auto & history = config.getCurrentStateEntropyHistory();

  if(index < 0 || index >= (int)history.size())
    return {dict, featName, Dict::nullValueStr, policy};

  std::string value = std::to_string((int)history.getElem(index));

  return {dict, featName, value, policy};
}

FeatureModel::FeatureValue FeatureBank::getUppercase(Config &, const FeatureModel::FeatureValue & fv)
{
  Dict * dict = Dict::getDict("bool");
  auto policy = dictPolicy2FeaturePolicy(dict->policy);
  bool firstLetterUppercase = util::isUpper(fv.values[0][0]);

  if(fv.values[0] == Dict::nullValueStr)
    return {dict, fv.names[0], Dict::nullValueStr, policy};

  std::string str = firstLetterUppercase ? std::string("true") : std::string("false");

  return {dict, fv.names[0], str, policy};
}

FeatureModel::FeatureValue FeatureBank::getLength(Config &, const FeatureModel::FeatureValue & fv)
{
  Dict * dict = Dict::getDict("int");
  auto policy = dictPolicy2FeaturePolicy(dict->policy);
  int len = util::lengthPrinted(fv.values[0]);

  if(fv.values[0] == Dict::nullValueStr)
    return {dict, fv.names[0], Dict::nullValueStr, policy};

  int limit = 10;
  if (len > limit)
    len = limit;

  std::string str = std::to_string(len);

  return {dict, fv.names[0], str, policy};
}

FeatureModel::FeatureValue FeatureBank::getLetters(Config &, const FeatureModel::FeatureValue & fv, int from, int to)
{
  Dict * dict = Dict::getDict("letters");
  auto policy = dictPolicy2FeaturePolicy(dict->policy);

  if(fv.values[0] == Dict::nullValueStr)
    return {dict, fv.names[0], Dict::nullValueStr, policy};

  int nbSymbols = util::getNbSymbols(fv.values[0]);

  if(from < 0)
    from = nbSymbols + from;
  if(to < 0)
    to = nbSymbols + to;

  if(from < 0 || to < 0 || from >= nbSymbols || to >= nbSymbols)
    return {dict, fv.names[0], Dict::nullValueStr, policy};

  if(to < from)
  {
    fprintf(stderr, "ERROR (%s) : to(%d) < from(%d), FeatureModel probably invalid. Aborting.\n", ERRINFO, to, from);
    exit(1);
  }

  int start = util::getStartIndexOfNthSymbol(fv.values[0], from);
  int end = util::getEndIndexOfNthSymbol(fv.values[0], to);

  std::string letters;
  for(int i = start; i <= end; i++)
    letters.push_back(fv.values[0][i]);

  return {dict, fv.names[0], letters, policy};
}

FeatureModel::Policy FeatureBank::dictPolicy2FeaturePolicy(Dict::Policy policy)
{
  return policy == Dict::Policy::Final ? FeatureModel::Policy::Final : FeatureModel::Policy::Modifiable;
}

FeatureModel::FeatureValue FeatureBank::aggregateBuffer(Config & c, int from, int to, const std::vector<std::string> & exceptions)
{
  FeatureModel::FeatureValue result(FeatureModel::Function::Concat);

  for (auto & tape : c.tapes)
  {
    Dict * dict = c.getDictOfLine(tape.getName());
    auto policy = dictPolicy2FeaturePolicy(dict->policy);
    bool ignored = false;
    for (auto & except : exceptions)
      if (except == tape.getName())
      {
        ignored = true;
        break;
      }
    if (ignored)
      continue;

    for (int i = from; i <= to; i++)
    {
      int index = c.getHead() + i;
      std::string featName = "b."+std::to_string(i)+"."+tape.getName();
      if(index < 0 || index >= tape.hypSize())
      {
        result.dicts.emplace_back(dict);
        result.names.emplace_back(featName);
        result.values.emplace_back(Dict::nullValueStr);
        result.policies.emplace_back(policy);
        continue;
      }
      if(tape[i].empty())
      {
        result.dicts.emplace_back(dict);
        result.names.emplace_back(featName);
        result.values.emplace_back(Dict::nullValueStr);
        result.policies.emplace_back(policy);
        continue;
      }

      result.dicts.emplace_back(dict);
      result.names.emplace_back(featName);
      result.values.emplace_back(tape[i]);
      result.policies.emplace_back(policy);
    }
  }

  return result;
}

FeatureModel::FeatureValue FeatureBank::aggregateStack(Config & c, int from, const std::vector<std::string> & exceptions)
{
  FeatureModel::FeatureValue result(FeatureModel::Function::Concat);

  for (auto & tape : c.tapes)
  {
    Dict * dict = c.getDictOfLine(tape.getName());
    auto policy = dictPolicy2FeaturePolicy(dict->policy);
    bool ignored = false;
    for (auto & except : exceptions)
      if (except == tape.getName())
      {
        ignored = true;
        break;
      }
    if (ignored)
      continue;

    for (int i = 0; i >= from; i--)
    {
      std::string featName = "s."+std::to_string(i)+"."+tape.getName();
      if(!c.stackHasIndex(i))
      {
        result.dicts.emplace_back(dict);
        result.names.emplace_back(featName);
        result.values.emplace_back(Dict::nullValueStr);
        result.policies.emplace_back(policy);
        continue;
      }
      int index = c.stackGetElem(i);
      if( (index < 0 || index >= tape.hypSize()) || tape[index-c.getHead()].empty() )
      {
        result.dicts.emplace_back(dict);
        result.names.emplace_back(featName);
        result.values.emplace_back(Dict::nullValueStr);
        result.policies.emplace_back(policy);
        continue;
      }

      result.dicts.emplace_back(dict);
      result.names.emplace_back(featName);
      result.values.emplace_back(tape[index-c.getHead()]);
      result.policies.emplace_back(policy);
    }
  }

  return result;
}

FeatureModel::FeatureValue FeatureBank::fasttext(Config & c, const FeatureModel::FeatureValue & word)
{
  FeatureModel::FeatureValue result(FeatureModel::Function::Mean);

  Dict * lettersDict = Dict::getDict("form.f");
  auto policy = dictPolicy2FeaturePolicy(lettersDict->policy);
  
  if(word.values[0] == Dict::nullValueStr)
    return {lettersDict, word.names[0], Dict::nullValueStr, policy};

  unsigned int wordLength = util::getNbSymbols(word.values[0]);
  unsigned int gramLength = 4;

  bool slidingMode = false;

  if (wordLength < gramLength)
  {
    auto value = getLetters(c, word, 0, wordLength-1);
    result.dicts.emplace_back(lettersDict);
    result.names.emplace_back(value.names[0]);
    result.values.emplace_back(value.values[0]);
    result.policies.emplace_back(FeatureModel::Policy::Modifiable);
  }
  else
  {
    if (!slidingMode)
    {
      int nbGrams = wordLength / gramLength + (wordLength % gramLength ? 1 : 0);
      for (int i = 0; i < nbGrams; i++)
      {
        int from = i * gramLength;
        int to = i == nbGrams-1 ? wordLength-1 : (i+1)*gramLength-1;
        auto value = getLetters(c, word, from, to);
        result.dicts.emplace_back(lettersDict);
        result.names.emplace_back(value.names[0]);
        result.values.emplace_back(value.values[0]);
        result.policies.emplace_back(FeatureModel::Policy::Modifiable);
      }
    }
    else
    {
      for (unsigned int i = 0; i+gramLength-1 < wordLength; i++)
      {
        auto value = getLetters(c, word, i, i+gramLength-1);
        result.dicts.emplace_back(lettersDict);
        result.names.emplace_back(value.names[0]);
        result.values.emplace_back(value.values[0]);
        result.policies.emplace_back(FeatureModel::Policy::Modifiable);
      }
    }
  }

  return result;
}

