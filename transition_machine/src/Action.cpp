/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "Action.hpp"
#include "Config.hpp"
#include "ActionBank.hpp"
#include "util.hpp"
#include "ProgramParameters.hpp"

bool Action::operator==(const Action & other) const
{
  return name == other.name;
}

void Action::apply(Config & config)
{
  config.addHashToHistory();

  for(auto & basicAction : sequence)
    basicAction.apply(config, basicAction);

  config.getCurrentStateHistory().push(name);
  config.pastActions.push(std::pair<std::string, Action>(config.getCurrentStateName(), *this));
}

bool Action::appliable(Config & config)
{
  for(auto & basicAction : sequence)
    if (!basicAction.appliable(config, basicAction))
      return false;

  return true;
}

void Action::undo(Config & config)
{
  for(int i = sequence.size()-1; i >= 0; i--)
    sequence[i].undo(config, sequence[i]);

  if (ProgramParameters::debug)
    fprintf(stderr, "Undoing action <%s><%s>, state history size = %d past actions size = %d...", stateName.c_str(), name.c_str(), config.getStateHistory(stateName).size(), config.pastActions.size());

  if (true)
  {
    std::string undoneName = config.getStateHistory(stateName).pop();

    auto & history = config.getActionsHistory(stateName);
    if (history.size() == 0)
    {
      fprintf(stderr, "ERROR (%s) : actionsHistory of \'%s\' is empty (undoneName=\'%s\'). Aborting.\n", ERRINFO, stateName.c_str(), undoneName.c_str());
      exit(1);
    }

    for (int i = (int)history.size()-1; i >= 0; i--)
    {
      if (history[i].first == undoneName)
      {
        config.lastUndoneAction = history[i];
        break;
      }
      else if (i == 0)
      {
        fprintf(stderr, "ERROR (%s) : could not find action \'%s\' in actionsHistory of state \'%s\'. Aborting.\n", ERRINFO, undoneName.c_str(), stateName.c_str());
        exit(1);
      }
    }
  }

  if (ProgramParameters::debug)
    fprintf(stderr, "done\n");
}

void Action::undoOnlyStack(Config & config)
{
  for(int i = sequence.size()-1; i >= 0; i--)
  {
    auto type = sequence[i].type;
    if(type == BasicAction::Type::Write || type == BasicAction::Type::Back)
      continue;

    sequence[i].undo(config, sequence[i]);
  }

  if (ProgramParameters::debug)
    fprintf(stderr, "Undoing only stack action <%s><%s>, state history size = %d past actions size = %d...", stateName.c_str(), name.c_str(), config.getStateHistory(stateName).size(), config.pastActions.size());

  char buffer[1024];
  if (sscanf(stateName.c_str(), "error_%s", buffer) != 1)
    config.getStateHistory(stateName).pop();

  if (ProgramParameters::debug)
    fprintf(stderr, "done\n");
}

Action::Action(const std::string & name)
{
  this->name = name;
  for(unsigned int i = 0; i < name.size() && !util::isSeparator(name[i]); i++)
    this->namePrefix.push_back(name[i]);

  this->sequence = ActionBank::str2sequence(name);
}

Action::Action()
{
}

std::string Action::BasicAction::to_string()
{
  if(type == Type::Push)
    return "push " + data;
  else if(type == Type::Pop)
    return "pop " + data;
  else if(type == Type::Write)
    return "write " + data;
  else if(type == Type::Back)
    return "back " + data;
  else if(type == Type::MoveHead)
    return "moveHead " + data;

  return "null";
}

void Action::printForDebug(FILE * output)
{
  fprintf(output, "%s :\n\t", name.c_str());

  for(auto & basic : sequence)
    fprintf(output, "%s ", basic.to_string().c_str());
  fprintf(output, "\n");
}

void Action::setInfos(std::string stateName)
{
  this->stateName = stateName;
}

