/*Copyright (c) 2019 Alexis Nasr && Franck Dary

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:i

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/
#include "Classifier.hpp"
#include "File.hpp"
#include "util.hpp"
#include "MLP.hpp"
#include "ReversedMLP.hpp"
#include "MultiMLP.hpp"
#include "GeneticAlgorithm.hpp"

Classifier::Classifier(const std::string & filename, bool trainMode)
{
  this->trainMode = trainMode;

  auto badFormatAndAbort = [&filename](const char * errInfo)
  {
    fprintf(stderr, "ERROR (%s) : file %s bad format. Aborting.\n", errInfo, filename.c_str());

    exit(1);
  };

  File file(ProgramParameters::expPath + filename, "r");
  FILE * fd = file.getDescriptor();

  char buffer[1000000];

  if(fscanf(fd, "Name : %s\n", buffer) != 1)
    badFormatAndAbort(ERRINFO);

  name = buffer;

  if(fscanf(fd, "Type : %s\n", buffer) != 1)
    badFormatAndAbort(ERRINFO);

  type = str2type(buffer);

  if(fscanf(fd, "Oracle : %s\n", buffer) != 1)
    badFormatAndAbort(ERRINFO);

  if(type != Type::Prediction)
  {
    char buffer2[1024];

    if(type == Type::Information)
    {
      if(fscanf(fd, "Oracle Filename : %s\n", buffer2) != 1)
        badFormatAndAbort(ERRINFO);

      oracle = Oracle::getOracle(buffer, ProgramParameters::expPath + std::string("/") + buffer2);
    }
    else
      oracle = Oracle::getOracle(buffer);

    as.reset(new ActionSet(this->name + "_ActionSet", true));

    return;
  }

  oracle = Oracle::getOracle(buffer);

  if(fscanf(fd, "Feature Model : %s\n", buffer) != 1)
    badFormatAndAbort(ERRINFO);

  std::string fmFilename = ProgramParameters::expPath + buffer;

  if (ProgramParameters::featureModelByClassifier.count(this->name))
    fmFilename = ProgramParameters::featureModelByClassifier[this->name];

  fm.reset(new FeatureModel(fmFilename));

  if(fscanf(fd, "Action Set : %s\n", buffer) != 1)
    badFormatAndAbort(ERRINFO);

  as.reset(new ActionSet(ProgramParameters::expPath + buffer, false));

  if(fscanf(fd, "Topology : %[^\n]\n", buffer) != 1)
    badFormatAndAbort(ERRINFO);

  topology = buffer;

  batchSize = 0;
  int batchsizeRead = 0;
  if(fscanf(fd, "Batchsize : %d\n", &batchsizeRead) == 1)
    batchSize = batchsizeRead;

  dynamic = false;
  if(fscanf(fd, "Dynamic : %[^\n]\n", buffer) != 1)
    badFormatAndAbort(ERRINFO);

  if (!strcmp(buffer, "yes"))
    dynamic = true;
}

Classifier::Type Classifier::str2type(const std::string & s)
{
  if(s == "Prediction")
    return Type::Prediction;
  else if (s == "Information")
    return Type::Information;
  else if (s == "Forced")
    return Type::Forced;

  fprintf(stderr, "ERROR (%s) : invalid type \'%s\'. Aborting.\n", ERRINFO, s.c_str());
  exit(1);

  return Type::Prediction;
}

Classifier::WeightedActions Classifier::weightActions(Config & config)
{
  WeightedActions result;

  if(type == Type::Prediction)
  {
    if (ProgramParameters::noNeuralNetwork)
    {
      for (unsigned int i = 0; i < as->actions.size(); i++)
        result.emplace_back(as->actions[i].appliable(config), std::pair<float, std::string>(1.0, as->actions[i].name));

      std::random_shuffle(result.begin(), result.end());

      return result;
    }

    initClassifier(config);

    auto & fd = fm->getFeatureDescription(config);
    auto scores = nn->predict(fd);

    if (ProgramParameters::showFeatureRepresentation == 1)
      fd.printForDebug(stderr);

    for (unsigned int i = 0; i < scores.size(); i++)
      result.emplace_back(as->actions[i].appliable(config), std::pair<float, std::string>(scores[i], as->actions[i].name));

    std::sort(result.begin(), result.end(),
      [](const std::pair< bool, std::pair<float, std::string> > & a, const std::pair< bool, std::pair<float, std::string> > & b)
      {
        return a.second.first > b.second.first; 
      });
  }
  else
    result.emplace_back(true, std::pair<float, std::string>(1.0, getOracleAction(config)));

  return result;
}

void Classifier::initClassifier(Config & config)
{
  if(type != Type::Prediction)
    return;

  if(nn.get())
    return;

  std::string modelFilename = ProgramParameters::expPath + name + ".model";
  if (util::fileExists(modelFilename))
  {
    nn.reset(createNeuralNetwork(modelFilename));
    Dict::initDicts(nn->getModel(), name);
    return;
  }

  if (!trainMode)
  {
    fprintf(stderr, "ERROR (%s) : could not find model \'%s\'. Maybe training was stopped before any model could be saved. Aborting.\n", ERRINFO, modelFilename.c_str());
    exit(1);
  }

  nn.reset(createNeuralNetwork());

  Dict::initDicts(nn->getModel(), name);

  auto fd = fm->getFeatureDescription(config);

  int nbInputs = 0;
  int nbOutputs = as->actions.size();

  for (auto feat : fd.values)
    nbInputs += NeuralNetwork::featureSize(feat);

  nn->init(nbInputs, topology, nbOutputs);
  if (batchSize)
    nn->setBatchSize(batchSize);
}

FeatureModel::FeatureDescription Classifier::getFeatureDescription(Config & config)
{
  if(type != Type::Prediction)
  {
    fprintf(stderr, "ERROR (%s) : classifier \'%s\' has no feature description. Aborting.\n", ERRINFO, name.c_str());
    exit(1);
  }

  return fm->getFeatureDescription(config);
}

std::string Classifier::getOracleAction(Config & config)
{
  return oracle->getAction(config);
}

int Classifier::getOracleActionIndex(Config & config)
{
  return as->getActionIndex(oracle->getAction(config));
}

int Classifier::getActionIndex(const std::string & action)
{
  return as->getActionIndex(action);
}

std::string Classifier::getActionName(int actionIndex)
{
  return as->getActionName(actionIndex);
}

void Classifier::printWeightedActions(FILE * output, WeightedActions & wa, int threshhold)
{
  int nbCols = 80;
  char symbol = '-';

  for(int i = 0; i < nbCols; i++)
    fprintf(output, "%c%s", symbol, i == nbCols-1 ? "\n" : "");

  bool oneActionWasPossible = false;

  for(unsigned int i = 0; i < wa.size() && (int)i < threshhold; i++)
  {
    auto & it = wa[i];
    bool thisActionIsPossible = it.first ? true : false;
    oneActionWasPossible = oneActionWasPossible || thisActionIsPossible;
    fprintf(output, "%s %6.2f %s\n", thisActionIsPossible ? "*" : " ", it.second.first, it.second.second.c_str());
  }

  if(!oneActionWasPossible)
    for(unsigned int i = threshhold; i < wa.size() ;i++)
      if(wa[i].first)
      {
        fprintf(output, "%s %6.2f %s\n", "*", wa[i].second.first, wa[i].second.second.c_str());
        break;
      }

  for(int i = 0; i < nbCols; i++)
    fprintf(output, "%c%s", symbol, i == nbCols-1 ? "\n" : "");
}

void Classifier::save(const std::string & filename)
{
  if(type != Type::Prediction)
  {
    fprintf(stderr, "ERROR (%s) : classifier \'%s\' cannot be saved. Aborting.\n", ERRINFO, name.c_str());
    exit(1);
  }

  nn->save(filename);
}

Action * Classifier::getAction(const std::string & name)
{
  return as->getAction(name);
}

bool Classifier::needsTrain()
{
  return type == Type::Prediction;
}

void Classifier::printTopology(FILE * output)
{
  if (ProgramParameters::interactive)
    fprintf(output, "                                               \r");
  fprintf(output, "%s topology : ", name.c_str());
  nn->printTopology(output);
}

int Classifier::getActionCost(Config & config, const std::string & action)
{
  return oracle->getActionCost(config, action);
}

std::vector<std::string> Classifier::getZeroCostActions(Config & config)
{
  std::vector<std::string> result;

  for (Action & a : as->actions)
    if (a.appliable(config) && oracle->getActionCost(config, a.name) == 0)
      result.emplace_back(a.name);

  if (ProgramParameters::debug)
  {
    fprintf(stderr, "Zero cost actions : ");
    for (auto & s : result)
      fprintf(stderr, "<%s>", s.c_str());
    fprintf(stderr, "\n");
  }

  if (result.empty() && as->hasDefaultAction)
    if (as->getDefaultAction()->appliable(config))
      result.emplace_back(as->getDefaultAction()->name);

  return result;
}

std::string Classifier::getDefaultAction() const
{
  if (as->hasDefaultAction)
    return as->getDefaultAction()->name;

  return std::string();
}

float Classifier::trainOnExample(Config & config, int gold)
{
  if (ProgramParameters::noNeuralNetwork)
    return 0.0;

  auto & fd = fm->getFeatureDescription(config);
  return nn->update(fd, gold);
}

float Classifier::trainOnExample(Config & config, const std::vector<float> & gold)
{
  if (ProgramParameters::noNeuralNetwork)
    return 0.0;

  auto & fd = fm->getFeatureDescription(config);
  return nn->update(fd, gold);
}

float Classifier::trainOnExample(FeatureModel::FeatureDescription & fd, int gold)
{
  if (ProgramParameters::noNeuralNetwork)
    return 0.0;

  return nn->update(fd, gold);
}

float Classifier::trainOnExample(FeatureModel::FeatureDescription & fd, const std::vector<float> & gold)
{
  if (ProgramParameters::noNeuralNetwork)
    return 0.0;

  return nn->update(fd, gold);
}

float Classifier::getLoss(Config & config, int gold)
{
  if (ProgramParameters::noNeuralNetwork)
    return 0.0;

  auto & fd = fm->getFeatureDescription(config);
  return nn->getLoss(fd, gold);
}

float Classifier::getLoss(Config & config, const std::vector<float> & gold)
{
  if (ProgramParameters::noNeuralNetwork)
    return 0.0;

  auto & fd = fm->getFeatureDescription(config);
  return nn->getLoss(fd, gold);
}

void Classifier::explainCostOfActions(FILE * output, Config & config)
{
  for (Action & a : as->actions)
  {
    fprintf(output, "%s : ", a.name.c_str());

    if (!a.appliable(config))
    {
      fprintf(output, "not appliable\n");
      continue;
    }
      
    oracle->explainCostOfAction(output, config, a.name);
  }
}

float Classifier::computeEntropy(WeightedActions & wa)
{
  float entropy = 0.0;
 
  for (unsigned int i = 0; i < 2 && i < wa.size(); i++)
  {
    auto it = wa.begin() + i;
    entropy -= it->second.first - (it->second.first - wa[0].second.first);
  }

  return entropy;
}

NeuralNetwork * Classifier::createNeuralNetwork()
{
  auto splited = util::split(topology, ' ');

  if (splited.size() == 2)
    return new GeneticAlgorithm();

  if (topology[0] == 'R')
    return new ReversedMLP();

  if (topology[0] == 'M')
    return new MultiMLP();

  return new MLP();
}

NeuralNetwork * Classifier::createNeuralNetwork(const std::string & modelFilename)
{
  auto splited = util::split(topology, ' ');

  if (splited.size() == 2)
    return new GeneticAlgorithm(modelFilename);

  if (topology[0] == 'R')
    return new ReversedMLP(modelFilename);

  if (topology[0] == 'M')
    return new MultiMLP(modelFilename);

  return new MLP(modelFilename);
}

unsigned int Classifier::getNbActions()
{
  return as->size();
}

FeatureModel * Classifier::getFeatureModel()
{
  return fm.get();
}

void Classifier::endOfIteration()
{
  nn->endOfIteration();
}

bool Classifier::isDynamic()
{
  return dynamic;
}

